//
//  PositionBundle.swift
//  PositionKit
//
//  Created by Carl-Johan Dahlman on 2019-10-30.
//

import Foundation
import qps
import UIKit
import CoreGraphics

public struct PositionBundle {
    public var position: CGPoint?
    public var debugPositions: [DebugPosition]?
    public var radius: Double?
    public var confidence: Double?
    public var certaintyState: CertaintyState = CertaintyState.CERTAIN
    
    public init(
        x: Float,
        y: Float,
        debugPositions: [DebugPosition],
        radius: Double,
        confidence: Double,
        certaintyState: CertaintyState = CertaintyState.CERTAIN
    ){
        self.position = CGPoint(x: CGFloat(x), y: CGFloat(y))
        self.debugPositions = debugPositions
        self.radius = radius
        self.confidence = confidence
        self.certaintyState = certaintyState
    }
    
    public init() {
        self.position = nil
        self.debugPositions = nil
        self.radius = nil
    }
    
    public enum CertaintyState {
        case CERTAIN
        case UNCERTAIN
        case RECOVERY
    }
}
