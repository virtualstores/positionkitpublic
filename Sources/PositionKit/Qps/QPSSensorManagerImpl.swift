//
//  QPSSensorManagerImpl.swift
//  PositionKit
//
//  Created by Carl-Johan Dahlman on 2019-10-30.
//

import Foundation
import qps

public class QPSSensorManagerImpl: IQPSRawSensorManager {
    public var accelerationSensor: RawSensor = QPSAccelerationSensor()
    public var gravitySensor: RawSensor = QPSGravitySensor()
    public var rotationSensor: RawSensor = QPSRotationSensor()
    public var orientationSensor: RawSensor? = QPSOrientationSensor()
    public var systemType: IQPSSystemType = IQPSSystemType.ios
    public var accelerationSensorUncalibrated: RawSensor? = nil
    public var gyroscopeSensorUncalibrated: RawSensor? = nil
}
