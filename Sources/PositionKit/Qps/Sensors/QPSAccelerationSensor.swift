//
//  QPSAccelerationSensor.swift
//  PositionKit
//
//  Created by Emil Bond on 2019-10-30.
//

import Foundation
import qps
import CoreMotion
import QuartzCore

public class QPSAccelerationSensor: RawSensor {
    let lsEhHq2je2y0kEWI1PFfD8yz8ekcWfkx = CMMotionManager()
    var D9xfmyuzbSllE7iMavPLzKtL7AD2xlbN: Timer?
    
    override public var sensorDataType: RawSensorData.SensorDataType{
        get{
            return RawSensorData.SensorDataType.acceleration
        }
    }
    
    override public var currentTime: Int64{
        get{
            Int64(CACurrentMediaTime() * 1000)
        }
    }
    
    override public func start() {
        // Make sure the accelerometer hardware is available.
        if self.lsEhHq2je2y0kEWI1PFfD8yz8ekcWfkx.isDeviceMotionAvailable {
            self.lsEhHq2je2y0kEWI1PFfD8yz8ekcWfkx.deviceMotionUpdateInterval = 1.0 / 100.0
            self.lsEhHq2je2y0kEWI1PFfD8yz8ekcWfkx.startDeviceMotionUpdates(to: OperationQueue.main, withHandler: { (data, error) in
                if let validData = data {
                    let arr = KotlinFloatArray(size: 3)
                    arr.set(index: 0, value: Float(validData.userAcceleration.x * 9.81))
                    arr.set(index: 1, value: Float(validData.userAcceleration.y * 9.81))
                    arr.set(index: 2, value: Float(validData.userAcceleration.z * 9.81))
                    
                    self.notifyChanged(values: arr)
                }
            })
        }
    }
    
    override public func stop() {
        if self.lsEhHq2je2y0kEWI1PFfD8yz8ekcWfkx.isDeviceMotionAvailable {
            self.lsEhHq2je2y0kEWI1PFfD8yz8ekcWfkx.stopDeviceMotionUpdates()
        }
    }
    
    public override func notifyChanged(values: KotlinFloatArray) {
        super.setChanged()
        super.notifyChanged(values:values)
    }
    
    public override func getVendorName() -> String {
        return "Apple"
    }
}
