//
//  OrientationSensor.swift
//  PositionKit
//
//  Created by Emil Bond on 2020-10-16.
//

import Foundation
import qps
import CoreMotion
import QuartzCore

public class QPSOrientationSensor: RawSensor {
    let lsEhHq2je2y0kEWI1PFfD8yz8ekcWfkx = CMMotionManager()
    var D9xfmyuzbSllE7iMavPLzKtL7AD2xlbN: Timer?
    
    override public var sensorDataType: RawSensorData.SensorDataType{
        get{
            return RawSensorData.SensorDataType.geomagnetic
        }
    }
    
    override public var currentTime: Int64{
        get{
            Int64(CACurrentMediaTime() * 1000)
        }
    }
    var TuE9wIKiFShvqaSa3Porwenp2Cb6wMzA: Int = 0
    override public func start() {
        // Make sure the accelerometer hardware is available.
        if self.lsEhHq2je2y0kEWI1PFfD8yz8ekcWfkx.isDeviceMotionAvailable {
            self.lsEhHq2je2y0kEWI1PFfD8yz8ekcWfkx.deviceMotionUpdateInterval = 1.0 / 30.0
            self.lsEhHq2je2y0kEWI1PFfD8yz8ekcWfkx.startDeviceMotionUpdates(using: .xMagneticNorthZVertical, to: OperationQueue.main, withHandler: { (data, error) in
                if let validData = data {
                    let arr = KotlinFloatArray(size: 3)
                    arr.set(index: 0, value: Float(validData.heading))
                    arr.set(index: 1, value: Float(validData.attitude.pitch))
                    arr.set(index: 2, value: Float(validData.attitude.roll))
                
                    self.notifyChanged(values: arr)
                }
            })
        }
    }
    
    override public func stop() {
        if self.lsEhHq2je2y0kEWI1PFfD8yz8ekcWfkx.isDeviceMotionAvailable {
            self.lsEhHq2je2y0kEWI1PFfD8yz8ekcWfkx.stopDeviceMotionUpdates()
        }
    }
    
    public override func notifyChanged(values: KotlinFloatArray) {
        super.setChanged()
        super.notifyChanged(values:values)
    }
    
    public override func getVendorName() -> String {
        return "Apple"
    }
}
