//
//  QPSExtensions.swift
//  PositionKit
//
//  Created by Carl-Johan Dahlman on 2019-10-30.
//

import Foundation
import qps

extension PositionBundle {
    static func uqQYIWpCwgzBub2rfj0HXlDmBariXGrJ(_ bundle: QPSNavBundle) -> PositionBundle{
        return PositionBundle(
            x: bundle.position!.x,
            y: bundle.position!.y,
            debugPositions: bundle.debugPositions,
            radius: bundle.precisionCircle.radius,
            confidence: bundle.confidence)
    }
}
