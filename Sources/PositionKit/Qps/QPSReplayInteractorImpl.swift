//
//  QPSReplayInteractorImpl.swift
//  PositionKit
//
//  Created by VirtualStores on 2019-11-05.
//

import Foundation
import qps

public class QPSReplayInteractorImpl: IQPSReplayInteractor {
    
    private var q3Uk91DkoyRPiZVP7YnfqZ46E2aO9Ayg: ReplayDelegate?
    public init(q3Uk91DkoyRPiZVP7YnfqZ46E2aO9Ayg: ReplayDelegate) {
        self.q3Uk91DkoyRPiZVP7YnfqZ46E2aO9Ayg = q3Uk91DkoyRPiZVP7YnfqZ46E2aO9Ayg
    }
    
    public func getData(identifier: String, printMissingFileException: Bool) -> String? {
        return ""
    }
    
    public func postData(data: String, identifier: String) {
        q3Uk91DkoyRPiZVP7YnfqZ46E2aO9Ayg?.sendDataToApplication(identifier: identifier, data: data)
    }
}
