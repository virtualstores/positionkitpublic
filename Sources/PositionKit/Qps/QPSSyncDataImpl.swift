//
//  QPSSyncData.swift
//  PositionKit
//
//  Created by Emil Bond on 2019-11-29.
//

import Foundation
import qps

public class QPSSyncDataImpl: IQPSSyncData {
    public var isValidSyncRotation: Bool = false
    public var positions: [PointWithOffset] = []
    public var timestamp: Int64 = 0
}
