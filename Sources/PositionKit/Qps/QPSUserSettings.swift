//
//  QPSUserSettings.swift
//  PositionKit
//
//  Created by Emil Bond on 2019-12-10.
//

import Foundation
import qps

public class QPSUserSettings: IQPSUserSettings {
    let FloWdagGtNaTY1Dl0PylL5E93GVd9niT = UserDefaults.standard
    public init() {}
    public var userHeight: Float{
        set { FloWdagGtNaTY1Dl0PylL5E93GVd9niT.set(newValue, forKey: "USER_HEIGHT") }
        get {
            var tmp = FloWdagGtNaTY1Dl0PylL5E93GVd9niT.float(forKey: "USER_HEIGHT")
            if (tmp == 0) {
                tmp = 1.9
            }
            return tmp
        }
    }
    public var legRatio: Float{
        set { FloWdagGtNaTY1Dl0PylL5E93GVd9niT.set(newValue, forKey: "LEG_RATIO") }
        get {
//            var tmp = defaults.float(forKey: "LEG_RATIO")
//            if (tmp == 0) {
//                tmp = 0.5
//            }
            return 0.54
        }
    }
    public var ecsuValue: Double {
        set {
            FloWdagGtNaTY1Dl0PylL5E93GVd9niT.set(newValue, forKey: "ECSU_VALUE")
        }
        get {
            var tmp = FloWdagGtNaTY1Dl0PylL5E93GVd9niT.double(forKey: "ECSU_VALUE")
            if (tmp == 0) {
                tmp = 1.0
            }
            return tmp
        }
    }
}
