//
//  File.swift
//  
//
//  Created by Carl-Johan Dahlman on 2019-10-29.
//

import Foundation
import qps
import MapKit

public protocol ReplayDelegate {
    func sendDataToApplication(identifier: String, data: String)
}

public class PositionKitImpl: PositionKit {
    let e083RhVnaVP4xM3MWr8kBsjKdj3FKlRu = QPSSensorManagerImpl()
    var Mkg6uTsSWq5syFji9AViwsMOKnJYYPE1: QPSHandler!
    var TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0: IQPSInteractorOutput?
    var AHpcnNgAFxUHvzM9Kk14i34fbxyENRUZ: QPSReplayInteractorImpl?
    var M5BINffYADigFohvlFHUvaZoLmAAR4ZE: Bool = false
    public var shouldRecord: Bool = true
    var ryGRqu02Ddm8I3bna4GqcBTaGlyvA5e4: Bool = false
    var AzHjMdbo7CHx173mUFH40DfBVC9aVbPA: B8Btq4wEj1m6oFVvAYjFQtUbLpCHoM4C?
    var mRAatnD6Ez0Zs1nuxfbHA1H4KRLV0y63: yA4lnpFYgqftdnxD76y7Wyf8UFk15WOU?
    var CfhZj5c6cPULrFR67hYY9pNtqWU8uuXY: OffsetZoneData?
    // Deafault value, HEMKOP is 59,59 must be set from app then
    public var scale: CGPoint = CGPoint(x: 50, y: 50)
    private var CTRqcgKxrwzPnwWrrMues0cAxglCGpd2 = 0.0
    
    public init(){
        self.AHpcnNgAFxUHvzM9Kk14i34fbxyENRUZ = QPSReplayInteractorImpl(q3Uk91DkoyRPiZVP7YnfqZ46E2aO9Ayg: self)
    }
    
    public func start() {
        if !M5BINffYADigFohvlFHUvaZoLmAAR4ZE {
            self.M5BINffYADigFohvlFHUvaZoLmAAR4ZE = true
            self.Mkg6uTsSWq5syFji9AViwsMOKnJYYPE1 = QPSHandler(rawSensorManager: e083RhVnaVP4xM3MWr8kBsjKdj3FKlRu, interactor: self, replayInteractor: AHpcnNgAFxUHvzM9Kk14i34fbxyENRUZ!, mapInformation: xGzjCgEoNgyMVw6qZEMTygGydZ6f5Kwv(), userSettings: QPSUserSettings(), playback: false)
        }
    }
    
    private func xGzjCgEoNgyMVw6qZEMTygGydZ6f5Kwv() -> IQPSMapInformation {
        let mapInformation = MapInformation.init(width: Int32(self.mRAatnD6Ez0Zs1nuxfbHA1H4KRLV0y63!.MqvQOAD3ItmWtzXvrqKIqRRHGLl37tmu), height: Int32(self.mRAatnD6Ez0Zs1nuxfbHA1H4KRLV0y63!.puOFESKJSADArOayhXi1V7HEiiPheJOC), mapFencePolygons: self.mRAatnD6Ez0Zs1nuxfbHA1H4KRLV0y63!.hEAR3gzHADuczTFbhUNRItlVdWWQim81, mapFenceScale: Double(scale.x), offsetZones: self.CfhZj5c6cPULrFR67hYY9pNtqWU8uuXY?.EXYRdIwi3msDXR81xRen1U4cCaaTc9R7 ??  [OffsetZone].init(), realWorldOffset: self.CTRqcgKxrwzPnwWrrMues0cAxglCGpd2)
        return mapInformation
    }
    
    public func stop() {
        if M5BINffYADigFohvlFHUvaZoLmAAR4ZE {
            M5BINffYADigFohvlFHUvaZoLmAAR4ZE = false
            Mkg6uTsSWq5syFji9AViwsMOKnJYYPE1.stopNavigation()
            Mkg6uTsSWq5syFji9AViwsMOKnJYYPE1 = nil
        }
    }
    
    public func startRecording() {
        if M5BINffYADigFohvlFHUvaZoLmAAR4ZE && shouldRecord {
            Mkg6uTsSWq5syFji9AViwsMOKnJYYPE1.startRecording()
            ryGRqu02Ddm8I3bna4GqcBTaGlyvA5e4 = true
        }
    }
    
    public func stopRecording() {
        if M5BINffYADigFohvlFHUvaZoLmAAR4ZE && ryGRqu02Ddm8I3bna4GqcBTaGlyvA5e4{
            Mkg6uTsSWq5syFji9AViwsMOKnJYYPE1.stopRecording()
            ryGRqu02Ddm8I3bna4GqcBTaGlyvA5e4 = false
        }
    }
    
    // startposition should be in meter
    public func startNavigation(startPosition: PointF, startAngle: Double) {
        if M5BINffYADigFohvlFHUvaZoLmAAR4ZE {
            TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0?.startNavigation(startPos: startPosition, startAngle: startAngle)
        } else {
            self.start()
            TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0?.startNavigation(startPos: startPosition, startAngle: startAngle)
        }
    }
    
    public func initPositionSync() {
        if M5BINffYADigFohvlFHUvaZoLmAAR4ZE {
            Mkg6uTsSWq5syFji9AViwsMOKnJYYPE1.doInitPositionSyncEvent()
        }
    }
    
    // position should be in meter
    public func setPosition(point: PointF, direction: PointF, syncDirection: Bool = false) {
        if(self.M5BINffYADigFohvlFHUvaZoLmAAR4ZE) {
            let data = QPSSyncDataImpl()
            data.timestamp = Int64(NSDate().timeIntervalSince1970 * 1000)
            data.positions = [PointWithOffset(position: point, offset: direction)]
            data.isValidSyncRotation = syncDirection
            TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0?.onPositionSyncEvent(data: data)
        } else {
            self.start()
            if syncDirection {
                let offset = direction
                let angle = Double((atan2(offset.y, offset.x)) + 180.0) * -1.0
                self.startNavigation(startPosition: point, startAngle: angle)
            } else {
                self.startNavigation(startPosition: point, startAngle: Double.nan)
            }
        }
    }
    // notifies in meter
    private func cMA7wZ7fMlsqgOfAqQGGSUw9Hk7iUVO8(positionBundle: PositionBundle) {
        jcEmBEvszfhPTWh17nv7uqmW0WweIZlv.forEach { (id, observer) in
            observer.notifyNewPosition(positionBundle)
        }
    }
    // Notifys in radians
    private func levVGtl2V836rg1upbwZkGagGaDy9KgS(directionBundle: DirectionBundle) {
        jcEmBEvszfhPTWh17nv7uqmW0WweIZlv.forEach { (id, observer) in
            observer.notifyNewDirection(directionBundle)
        }
    }
    
    private func R5H0IWqmn2h2YzcxwojGeIsNXcnhbbwn() {
        jcEmBEvszfhPTWh17nv7uqmW0WweIZlv.forEach { (id, observer) in
            observer.notifyIllegalBehaviour()
        }
    }
    
    private func ZBLCTrENar2jsykEXgMlxelV8Uq5wqGv(){
        jcEmBEvszfhPTWh17nv7uqmW0WweIZlv.forEach { (id, observer) in
            observer.notifyBadStepLength()
        }
    }
    
    private func XXXdJOpzCmFRKM6HZKpYgooSkmkVboT1(orientation: IQPSDeviceOrientation) {
        jcEmBEvszfhPTWh17nv7uqmW0WweIZlv.forEach { (id, observer) in
            observer.notifyNewDeviceOrientation(orientation)
        }
    }
    
    private func QgBDpIgofFhHP6jwtQofUPs4NU197j7F(){
        jcEmBEvszfhPTWh17nv7uqmW0WweIZlv.forEach { (id, observer) in
            observer.notifySensorsInitiated()
        }
    }
    
    private func eMf86z34I6qdaixtP9HVCGgBXV82GFhD(_ error: Error?) {
        jcEmBEvszfhPTWh17nv7uqmW0WweIZlv.forEach { (id, observer) in
            observer.notifyFileUploaded(error)
        }
    }
    
    var jcEmBEvszfhPTWh17nv7uqmW0WweIZlv = Dictionary<String, PositionKitObserver>()
    public func addObserver(observer: PositionKitObserver) {
        if(!jcEmBEvszfhPTWh17nv7uqmW0WweIZlv.keys.contains(observer.id)){
            jcEmBEvszfhPTWh17nv7uqmW0WweIZlv[observer.id] = observer
        }
    }
    
    public func removeObserver(observer: PositionKitObserver) {
        if(jcEmBEvszfhPTWh17nv7uqmW0WweIZlv.keys.contains(observer.id)){
            jcEmBEvszfhPTWh17nv7uqmW0WweIZlv.removeValue(forKey: observer.id)
        }
    }
    
    public func setMapFence(filePath: String) {
        self.AzHjMdbo7CHx173mUFH40DfBVC9aVbPA = tzoAkGtI2IZSHxs9P9XbYGP5sjPFrJ2X.aFaKYyWVbPAOotWLqIeIcs9tRA2jX6xp(fromJsonFile: filePath)
        self.mRAatnD6Ez0Zs1nuxfbHA1H4KRLV0y63 = tzoAkGtI2IZSHxs9P9XbYGP5sjPFrJ2X.Q19tItr4JbJ5OKavcPLqBv4NTW4UUPvM(fromJsonFile: filePath)
    }
    
    public func setMapFence(mapData: Data) {
        self.AzHjMdbo7CHx173mUFH40DfBVC9aVbPA = tzoAkGtI2IZSHxs9P9XbYGP5sjPFrJ2X.aFaKYyWVbPAOotWLqIeIcs9tRA2jX6xp(fromJson: mapData)
        self.mRAatnD6Ez0Zs1nuxfbHA1H4KRLV0y63 = tzoAkGtI2IZSHxs9P9XbYGP5sjPFrJ2X.Q19tItr4JbJ5OKavcPLqBv4NTW4UUPvM(fromJson: mapData)
    }
    
    public func setMapOffsetZone(filePath: String) {
        self.CfhZj5c6cPULrFR67hYY9pNtqWU8uuXY = XADvinyffWd1yk9cDwg7zCEgF5PDHmFf.GoLek02vWWUfBzQ0uXmxUUU0kQNQRBrX(fromJsonFile: filePath)
    }
    
    public func setMapOffsetZone(mapData: Data) {
        self.CfhZj5c6cPULrFR67hYY9pNtqWU8uuXY = XADvinyffWd1yk9cDwg7zCEgF5PDHmFf.GoLek02vWWUfBzQ0uXmxUUU0kQNQRBrX(fromJsonData: mapData)
    }
    
    public func setScale(scale: CGPoint) {
        self.scale = scale
    }
    
    // Stores map north offset from true north according to a compass
    public func setTrueNorthOffset(offset: Double) {
        self.CTRqcgKxrwzPnwWrrMues0cAxglCGpd2 = offset
    }
}


extension PositionKitImpl: IQPSInteractor {
    public var versionOfVPS: String {
        return "0.1.35" //TODO: remove this!!
    }
    
    public func onIllegalBehaviour() {
        R5H0IWqmn2h2YzcxwojGeIsNXcnhbbwn()
    }
    
    public func onBadStepLength() {
        ZBLCTrENar2jsykEXgMlxelV8Uq5wqGv()
    }
    
    public func onNewDeviceOrientation(orientation: IQPSDeviceOrientation) {
        XXXdJOpzCmFRKM6HZKpYgooSkmkVboT1(orientation: orientation)
    }
    
    public func validWorldPosition(point: PointF) -> Bool {
        guard let fence = self.AzHjMdbo7CHx173mUFH40DfBVC9aVbPA else {
            return false
        }
        
        return fence.HeobE0ZNAs1ndr6ivnjnAsNFpLMa3nWj (
            // mapfence is in pixels so uses the factor for meter to pixel
            x: CGFloat(point.x * Float(self.scale.x)),
            y: CGFloat(point.y * Float(self.scale.y)
        ))
    }
    
    public func onSensorsInitiated() {
        QgBDpIgofFhHP6jwtQofUPs4NU197j7F()
    }
    
    public func setOutput(output TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0: IQPSInteractorOutput) {
        self.TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0 = TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0
    }
    
    public func onDestroy() {
        
    }
    
    public func onNewNavigationBundle(navigationBundle: QPSNavBundle) {
        guard navigationBundle.position != nil else {
            return cMA7wZ7fMlsqgOfAqQGGSUw9Hk7iUVO8(positionBundle: PositionBundle())
        }
        
        cMA7wZ7fMlsqgOfAqQGGSUw9Hk7iUVO8(
            positionBundle: PositionBundle.uqQYIWpCwgzBub2rfj0HXlDmBariXGrJ(navigationBundle)
        )
    }
    
    public func onNewDirectionBundle(directionBundle: DirectionBundle) {
        levVGtl2V836rg1upbwZkGagGaDy9KgS(directionBundle: directionBundle)
    }
}

extension PositionKitImpl: ReplayDelegate {
    public func sendDataToApplication(identifier: String, data: String) {
        jcEmBEvszfhPTWh17nv7uqmW0WweIZlv.forEach { (id, observer) in
            observer.sendDataToApplication(identifier: identifier, data: data)
        }
    }
}
