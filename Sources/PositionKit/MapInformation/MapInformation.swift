//
//  MapInformation.swift
//  PositionKit
//
//  Created by Emil Bond on 2020-08-18.
//

import Foundation
import qps

public class MapInformation: IQPSMapInformation {
    public var debug: Bool = false
    public var scaleToPrint: Int32 = 4
    public var width: Int32
    public var height: Int32
    public var mapFencePolygons: [[PointF]]
    public var mapFenceScale: Double
    public var offsetZones: [IQPSOffsetZone]
    public var offsetZoneScale: KotlinDouble?
    public var realWorldOffset: Double
    
    public init(
        width: Int32,
        height: Int32,
        mapFencePolygons: [[PointF]],
        mapFenceScale: Double = 50.0,
        offsetZones: [IQPSOffsetZone],
        offsetZoneScale: Double? = 1000.0,
        realWorldOffset: Double = 0.0
    ){
        self.width = width
        self.height = height
        self.mapFencePolygons = mapFencePolygons
        self.mapFenceScale = mapFenceScale
        self.offsetZones = offsetZones
        self.offsetZoneScale = KotlinDouble.init(double: offsetZoneScale!)
        self.realWorldOffset = realWorldOffset
    }
}
