//
//  File.swift
//  
//
//  Created by Felix Andersson on 2019-10-29.
//

import UIKit
import qps

private typealias Polygon = [[CGPoint]]
private typealias PolygonJson = Dictionary<String, AnyObject>

internal class BTS2D43SbdF69enJDJxe602hILWIqCYx: NSObject {
    internal static func aFaKYyWVbPAOotWLqIeIcs9tRA2jX6xp(fromJsonFile: String) -> B8Btq4wEj1m6oFVvAYjFQtUbLpCHoM4C {
        let filePath = Bundle.main.path(forResource: fromJsonFile, ofType: "json")!
        let data = try! Data(referencing: NSData(contentsOfFile: filePath))
        return aFaKYyWVbPAOotWLqIeIcs9tRA2jX6xp(fromJsonData: data)
    }
    
    internal static func aFaKYyWVbPAOotWLqIeIcs9tRA2jX6xp(fromJsonData data: Data) -> B8Btq4wEj1m6oFVvAYjFQtUbLpCHoM4C {
        let json = try! JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! PolygonJson
        var TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0: Polygon = []
        let polygons = json["coordinates"] as! [[AnyObject]]
        let properties = json["properties"] as! NSDictionary
        let width = Int(properties["width"] as! CGFloat)
        let height = Int(properties["height"] as! CGFloat)
        
        for object in polygons{
            let polygonPoints = object[0] as! [[CGFloat]]
            let points: [CGPoint] = polygonPoints.map { (coords) -> CGPoint in
                return CGPoint(x: coords[0], y: coords[1])
            }
            TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0.append(points)
        }
        return IS8YVnCQYykCEXhastN1geK86W1iJbu1(fromPolygons: TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0, width: width, height: height)
    }
    
    internal static func Q19tItr4JbJ5OKavcPLqBv4NTW4UUPvM(fromJsonFile: String) -> yA4lnpFYgqftdnxD76y7Wyf8UFk15WOU {
        let filePath = Bundle.main.path(forResource: fromJsonFile, ofType: "json")!
        let data = try! Data(referencing: NSData(contentsOfFile: filePath))
        return Q19tItr4JbJ5OKavcPLqBv4NTW4UUPvM(fromJsonData: data)
    }
    
    internal static func Q19tItr4JbJ5OKavcPLqBv4NTW4UUPvM(fromJsonData data: Data) -> yA4lnpFYgqftdnxD76y7Wyf8UFk15WOU {
        let json = try! JSONSerialization.jsonObject(with: data,
                                                     options: JSONSerialization.ReadingOptions.allowFragments) as! PolygonJson
        var TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0: [[PointF]] = []
        let polygons = json["coordinates"] as! [[AnyObject]]
        let properties = json["properties"] as! NSDictionary
        let width = Int(properties["width"] as! CGFloat)
        let height = Int(properties["height"] as! CGFloat)
        
        for object in polygons{
            let polygonPoints = object[0] as! [[CGFloat]]
            let points: [PointF] = polygonPoints.map { (coords) -> PointF in
                return PointF(x: Double(coords[0]), y: Double(coords[1]))
            }
            TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0.append(points)
        }
        return yA4lnpFYgqftdnxD76y7Wyf8UFk15WOU(width: width, height: height, polygons: TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0)
    }
}
