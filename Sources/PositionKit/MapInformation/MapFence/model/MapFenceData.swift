//
//  MapFenceData.swift
//  PositionKit
//
//  Created by Emil Bond on 2020-08-18.
//

import Foundation
import UIKit
import qps

internal struct yA4lnpFYgqftdnxD76y7Wyf8UFk15WOU {
    let MqvQOAD3ItmWtzXvrqKIqRRHGLl37tmu: Int
    let puOFESKJSADArOayhXi1V7HEiiPheJOC: Int
    let hEAR3gzHADuczTFbhUNRItlVdWWQim81: [[PointF]]
    
    public init (
        width: Int,
        height: Int,
        polygons: [[PointF]]
    ){
        self.MqvQOAD3ItmWtzXvrqKIqRRHGLl37tmu = width
        self.puOFESKJSADArOayhXi1V7HEiiPheJOC = height
        self.hEAR3gzHADuczTFbhUNRItlVdWWQim81 = polygons
    }
}
