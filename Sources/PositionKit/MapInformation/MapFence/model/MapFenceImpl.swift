//
//  File.swift
//
//
//  Created by Felix Andersson on 2019-10-29.
//

import UIKit

internal class IS8YVnCQYykCEXhastN1geK86W1iJbu1: B8Btq4wEj1m6oFVvAYjFQtUbLpCHoM4C{
    let MqvQOAD3ItmWtzXvrqKIqRRHGLl37tmu: Int
    let puOFESKJSADArOayhXi1V7HEiiPheJOC: Int
    let v6s48ohlbygwgcJyQguFN54OYYSytD4F: UIImage
    
    public func HeobE0ZNAs1ndr6ivnjnAsNFpLMa3nWj(point: CGPoint) -> Bool {
        if point.x.isNaN || point.y.isNaN {
            return false
        }
        if(point.x < 0 ||
            point.y < 0 ||
            Int(point.x) > MqvQOAD3ItmWtzXvrqKIqRRHGLl37tmu ||
            Int(point.y) > puOFESKJSADArOayhXi1V7HEiiPheJOC) {
            return false
        }
        
        let c = gPDxrvhj21pDnFvh2Bb5TjE3rrJsVtJR(v6s48ohlbygwgcJyQguFN54OYYSytD4F, point)
        return c != .blue
    }
    
    required public init(fromPolygons polygons: [[CGPoint]], width: Int, height: Int) {
        let optimalWidth = width - (width.remainderReportingOverflow(dividingBy: 16)).partialValue
        UIGraphicsBeginImageContextWithOptions(CGSize(width: optimalWidth, height: height), false, 1)
        defer { UIGraphicsEndImageContext() }
        
        self.MqvQOAD3ItmWtzXvrqKIqRRHGLl37tmu = optimalWidth
        self.puOFESKJSADArOayhXi1V7HEiiPheJOC = height
        if polygons.isEmpty{
            fatalError()
        }
        
        var fencePath: [UIBezierPath] = []
        for polygon in polygons{
            let path = UIBezierPath()
            path.move(to: polygon.first!)
            for vertex in polygon{
                path.addLine(to: vertex)
            }
            path.close()
            fencePath.append(path)
        }
        
        UIColor.red.setFill()
        for path in fencePath {
            path.fill()
        }
        self.v6s48ohlbygwgcJyQguFN54OYYSytD4F = UIGraphicsGetImageFromCurrentImageContext()!
    }
    
    func gPDxrvhj21pDnFvh2Bb5TjE3rrJsVtJR(_ v6s48ohlbygwgcJyQguFN54OYYSytD4F:UIImage, _ point: CGPoint) -> UIColor {
        let cgImage : CGImage = v6s48ohlbygwgcJyQguFN54OYYSytD4F.cgImage!
        guard let pixelData = CGDataProvider(data: (cgImage.dataProvider?.data)!)?.data else {
            return UIColor.clear
        }
        if point.x.isNaN || point.y.isNaN {
            return UIColor.clear
        }
        let data = CFDataGetBytePtr(pixelData)!
        let x = Int(point.x)
        let y = Int(point.y)
        let index = cgImage.width * y + x
        let expectedLengthA = cgImage.width * cgImage.height
        let expectedLengthGrayScale = 2 * expectedLengthA
        let expectedLengthRGB = 3 * expectedLengthA
        let expectedLengthRGBA = 4 * expectedLengthA
        let numBytes = CFDataGetLength(pixelData)
        switch numBytes {
        case expectedLengthA:
            return UIColor(red: 0, green: 0, blue: 0, alpha: CGFloat(data[index])/255.0)
        case expectedLengthGrayScale:
            return UIColor(white: CGFloat(data[2 * index]) / 255.0, alpha: CGFloat(data[2 * index + 1]) / 255.0)
        case expectedLengthRGB:
            return UIColor(red: CGFloat(data[3*index])/255.0, green: CGFloat(data[3*index+1])/255.0, blue: CGFloat(data[3*index+2])/255.0, alpha: 1.0)
        case expectedLengthRGBA:
            return UIColor(red: CGFloat(data[4*index])/255.0, green: CGFloat(data[4*index+1])/255.0, blue: CGFloat(data[4*index+2])/255.0, alpha: CGFloat(data[4*index+3])/255.0)
        default:
            // unsupported format
            return UIColor.clear
        }
    }
}
