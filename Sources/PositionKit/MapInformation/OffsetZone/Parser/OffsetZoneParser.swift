//
//  OffsetZoneParser.swift
//  PositionKit
//
//  Created by Emil Bond on 2020-08-18.
//

import UIKit
import qps

private typealias Polygon = [[CGPoint]]
private typealias PolygonJson = Dictionary<String, AnyObject>

internal class XADvinyffWd1yk9cDwg7zCEgF5PDHmFf: NSObject {
    
    internal static func GoLek02vWWUfBzQ0uXmxUUU0kQNQRBrX(fromJsonFile: String) -> OffsetZoneData {
        let filePath = Bundle.main.path(forResource: fromJsonFile, ofType: "json")!
        let data = try! Data(referencing: NSData(contentsOfFile: filePath))
        return GoLek02vWWUfBzQ0uXmxUUU0kQNQRBrX(fromJsonData: data)
    }
    
    internal static func GoLek02vWWUfBzQ0uXmxUUU0kQNQRBrX(fromJsonData data: Data) -> OffsetZoneData {
        let json = try! JSONSerialization.jsonObject(with: data,
                                                     options: JSONSerialization.ReadingOptions.allowFragments) as! PolygonJson
        var TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0: [OffsetZone] = []
        let features = json["features"] as! [AnyObject]
        for feature in features {
            if feature != nil {
                let properties = feature["properties"] as! NSDictionary
                let geometry = feature["geometry"] as! NSDictionary
                let cordinates = geometry["coordinates"] as! [[[Double]]]
                let points: [PointF] = cordinates[0].map { (coords) -> PointF in
                    return PointF(x: coords[0], y: coords[1])
                }
                TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0.append(OffsetZone(offsetRadians: properties["radianer"] as! Double, polygons: points))
            }
        }
        
        return OffsetZoneData(width: 0, height: 0, offsetZones: TvTBAUXfskyLRvIZheYPYnXyoWCOSGi0)
    }
}
