//
//  OffsetZone.swift
//  PositionKit
//
//  Created by Emil Bond on 2020-08-18.
//

import Foundation
import UIKit
import qps

public class OffsetZone: IQPSOffsetZone {
    public var offsetRadians: Double
    public var polygons: [PointF]
    
    public init(offsetRadians: Double, polygons: [PointF]) {
        self.offsetRadians = offsetRadians
        self.polygons = polygons
    }
}
