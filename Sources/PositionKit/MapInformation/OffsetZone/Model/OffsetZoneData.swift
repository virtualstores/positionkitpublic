//
//  OffsetZoneData.swift
//  PositionKit
//
//  Created by Emil Bond on 2020-08-18.
//

import Foundation
import UIKit
import qps

public struct OffsetZoneData {
    let MqvQOAD3ItmWtzXvrqKIqRRHGLl37tmu: Int
    let puOFESKJSADArOayhXi1V7HEiiPheJOC: Int
    let EXYRdIwi3msDXR81xRen1U4cCaaTc9R7: [OffsetZone]
    
    public init (
        width: Int,
        height: Int,
        offsetZones: [OffsetZone]
    ){
        self.MqvQOAD3ItmWtzXvrqKIqRRHGLl37tmu = width
        self.puOFESKJSADArOayhXi1V7HEiiPheJOC = height
        self.EXYRdIwi3msDXR81xRen1U4cCaaTc9R7 = offsetZones
    }
}

