import Foundation
import qps
import UIKit

public protocol PositionKit {
    func start()
    func stop()
        
    func startNavigation(startPosition: PointF, startAngle: Double)
    func initPositionSync()
    func setPosition(point: PointF, direction: PointF, syncDirection: Bool)
    
    func startRecording()
    func stopRecording()
    
    func addObserver(observer: PositionKitObserver)
    func removeObserver(observer: PositionKitObserver)
    
    func setMapFence(filePath: String)
    func setMapFence(mapData: Data)
    func setMapOffsetZone(filePath: String)
    func setMapOffsetZone(mapData: Data)
    func setScale(scale: CGPoint)
}
