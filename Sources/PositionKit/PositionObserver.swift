//
//  File.swift
//  
//
//  Created by Carl-Johan Dahlman on 2019-10-30.
//

import Foundation
import qps

public protocol PositionKitObserver {
    var id: String {get}
    
    func notifyNewPosition(_ positionBundle: PositionBundle)
    func notifyNewDirection(_ direcitonBundle: DirectionBundle)
    func notifyNewDeviceOrientation(_ orientation: IQPSDeviceOrientation)
    func notifyIllegalBehaviour()
    func notifyBadStepLength()
    func notifySensorsInitiated()
    func notifyFileUploaded(_ error: Error?)
    
    func sendDataToApplication(identifier: String, data: String)
}
