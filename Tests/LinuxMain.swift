import XCTest

import PositionKitTests

var tests = [XCTestCaseEntry]()
tests += PositionKitTests.allTests()
XCTMain(tests)
