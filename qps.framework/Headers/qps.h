#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class QpsKotlinEnum, QpsIQPSDeviceOrientation, QpsDirectionBundle, QpsQPSNavBundle, QpsPointF, QpsRawSensor, QpsIQPSSystemType, QpsPointWithOffset, QpsMoveVector, QpsStepData, QpsStepStateStepStateType, QpsStepState, QpsStepFillEvaluatorSET_LENIENCY, QpsSpackStepData, QpsObservable, QpsStepDetectorManager, QpsKeyValueData, QpsStepDetector, QpsSpeedData, QpsRegularKeyValueData, QpsSpack, QpsFrequencySwingStepDetector, QpsSVM, QpsEllipseEvaluator, QpsPolynomial, QpsKotlinDoubleArray, QpsComplex, QpsKotlinFloatArray, QpsRational, QpsAnalogPrototype, QpsPassbandType, QpsSecondOrderSection, QpsIIRFilter, QpsButterworth, QpsMedFilter, QpsKotlinIntArray, QpsAllpass, QpsMapCoordinate, QpsTimedDebugPositions, QpsTimedDebugRadius, QpsDebugCluster, QpsDebugClusterAngle, QpsDebugPosition, QpsDebugState, QpsTimedDebugClusterAngles, QpsTimedDebugClusters, QpsTimedDebugMessages, QpsTimedDebugPosition, QpsTimedDebugStates, QpsRawSensorDataSensorDataType, QpsReplaySensor, QpsReplayDataCoordinator, QpsRawSensorData, QpsQPosition, QpsIReplayDataHandlerIReplayConfig, QpsQPSValues, QpsMovementStateTick, QpsReferenceAngleState, QpsReferenceAngle, QpsReplayData, QpsReplayDataInteractorActionType, QpsReplayDataInteractorAction, QpsKotlinPair, QpsReplayDataTick, QpsReplayPosition, QpsTimedMoveVector, QpsTimedReplayPositions, QpsSensorBundleBundleType, QpsSensorBundle, QpsRotationBundleTurn, QpsPrecisionCircle, QpsVectorTransformer, QpsKotlinArray, QpsMLModel, QpsStandardScaler, QpsPCATransformer, QpsHistoryHandlerTimedData, QpsHistoryHandler, QpsKotlinDoubleIterator, QpsKotlinFloatIterator, QpsKotlinIntIterator, QpsKotlinx_serialization_runtimeSerialKind, QpsKotlinNothing, QpsKotlinx_serialization_runtimeUpdateMode;

@protocol QpsKotlinComparable, QpsIQPSInteractorOutput, QpsIQPSSyncData, QpsIQPSOffsetZone, QpsIQPSRawSensorManager, QpsIQPSInteractor, QpsIQPSReplayInteractor, QpsIQPSMapInformation, QpsIQPSUserSettings, QpsIStepListener, QpsObserver, QpsKotlinx_serialization_runtimeKSerializer, QpsIReplayDataHandler, QpsKotlinx_serialization_runtimeEncoder, QpsKotlinx_serialization_runtimeSerialDescriptor, QpsKotlinx_serialization_runtimeSerializationStrategy, QpsKotlinx_serialization_runtimeDecoder, QpsKotlinx_serialization_runtimeDeserializationStrategy, QpsKotlinIterator, QpsKotlinx_serialization_runtimeCompositeEncoder, QpsKotlinx_serialization_runtimeSerialModule, QpsKotlinAnnotation, QpsKotlinx_serialization_runtimeCompositeDecoder, QpsKotlinx_serialization_runtimeSerialModuleCollector, QpsKotlinKClass, QpsKotlinKDeclarationContainer, QpsKotlinKAnnotatedElement, QpsKotlinKClassifier;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wnullability"

__attribute__((swift_name("KotlinBase")))
@interface QpsBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end;

@interface QpsBase (QpsBaseCopying) <NSCopying>
@end;

__attribute__((swift_name("KotlinMutableSet")))
@interface QpsMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end;

__attribute__((swift_name("KotlinMutableDictionary")))
@interface QpsMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end;

@interface NSError (NSErrorQpsKotlinException)
@property (readonly) id _Nullable kotlinException;
@end;

__attribute__((swift_name("KotlinNumber")))
@interface QpsNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end;

__attribute__((swift_name("KotlinByte")))
@interface QpsByte : QpsNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end;

__attribute__((swift_name("KotlinUByte")))
@interface QpsUByte : QpsNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end;

__attribute__((swift_name("KotlinShort")))
@interface QpsShort : QpsNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end;

__attribute__((swift_name("KotlinUShort")))
@interface QpsUShort : QpsNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end;

__attribute__((swift_name("KotlinInt")))
@interface QpsInt : QpsNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end;

__attribute__((swift_name("KotlinUInt")))
@interface QpsUInt : QpsNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end;

__attribute__((swift_name("KotlinLong")))
@interface QpsLong : QpsNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end;

__attribute__((swift_name("KotlinULong")))
@interface QpsULong : QpsNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end;

__attribute__((swift_name("KotlinFloat")))
@interface QpsFloat : QpsNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end;

__attribute__((swift_name("KotlinDouble")))
@interface QpsDouble : QpsNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end;

__attribute__((swift_name("KotlinBoolean")))
@interface QpsBoolean : QpsNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end;

__attribute__((swift_name("IQPS")))
@protocol QpsIQPS
@required
@end;

__attribute__((swift_name("KotlinComparable")))
@protocol QpsKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("KotlinEnum")))
@interface QpsKotlinEnum : QpsBase <QpsKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
- (int32_t)compareToOther:(QpsKotlinEnum *)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IQPSDeviceOrientation")))
@interface QpsIQPSDeviceOrientation : QpsKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) QpsIQPSDeviceOrientation *regular __attribute__((swift_name("regular")));
@property (class, readonly) QpsIQPSDeviceOrientation *swing __attribute__((swift_name("swing")));
@property (class, readonly) QpsIQPSDeviceOrientation *trolley __attribute__((swift_name("trolley")));
- (int32_t)compareToOther:(QpsIQPSDeviceOrientation *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("IQPSInteractor")))
@protocol QpsIQPSInteractor
@required
- (void)onBadStepLength __attribute__((swift_name("onBadStepLength()")));
- (void)onDestroy __attribute__((swift_name("onDestroy()")));
- (void)onIllegalBehaviour __attribute__((swift_name("onIllegalBehaviour()")));
- (void)onNewDeviceOrientationOrientation:(QpsIQPSDeviceOrientation *)orientation __attribute__((swift_name("onNewDeviceOrientation(orientation:)")));
- (void)onNewDirectionBundleDirectionBundle:(QpsDirectionBundle *)directionBundle __attribute__((swift_name("onNewDirectionBundle(directionBundle:)")));
- (void)onNewNavigationBundleNavigationBundle:(QpsQPSNavBundle *)navigationBundle __attribute__((swift_name("onNewNavigationBundle(navigationBundle:)")));
- (void)onSensorsInitiated __attribute__((swift_name("onSensorsInitiated()")));
- (void)setOutputOutput:(id<QpsIQPSInteractorOutput>)output __attribute__((swift_name("setOutput(output:)")));
@end;

__attribute__((swift_name("IQPSInteractorOutput")))
@protocol QpsIQPSInteractorOutput
@required
- (void)doInitPositionSyncEvent __attribute__((swift_name("doInitPositionSyncEvent()")));
- (void)onPositionSyncEventData:(id<QpsIQPSSyncData>)data __attribute__((swift_name("onPositionSyncEvent(data:)")));
- (void)startNavigationStartPos:(QpsPointF *)startPos startAngle:(double)startAngle __attribute__((swift_name("startNavigation(startPos:startAngle:)")));
- (void)startRecording __attribute__((swift_name("startRecording()")));
- (void)stopNavigation __attribute__((swift_name("stopNavigation()")));
- (void)stopRecording __attribute__((swift_name("stopRecording()")));
@end;

__attribute__((swift_name("IQPSMapInformation")))
@protocol QpsIQPSMapInformation
@required
@property (readonly) int32_t height __attribute__((swift_name("height")));
@property (readonly) NSArray<NSArray<QpsPointF *> *> *mapFencePolygons __attribute__((swift_name("mapFencePolygons")));
@property (readonly) double mapFenceScale __attribute__((swift_name("mapFenceScale")));
@property (readonly) QpsDouble * _Nullable offsetZoneScale __attribute__((swift_name("offsetZoneScale")));
@property (readonly) NSArray<id<QpsIQPSOffsetZone>> *offsetZones __attribute__((swift_name("offsetZones")));
@property (readonly) double realWorldOffset __attribute__((swift_name("realWorldOffset")));
@property (readonly) int32_t width __attribute__((swift_name("width")));
@end;

__attribute__((swift_name("IQPSOffsetZone")))
@protocol QpsIQPSOffsetZone
@required
@property (readonly) double offsetRadians __attribute__((swift_name("offsetRadians")));
@property (readonly) NSArray<QpsPointF *> *polygons __attribute__((swift_name("polygons")));
@end;

__attribute__((swift_name("IQPSRawSensorManager")))
@protocol QpsIQPSRawSensorManager
@required
@property (readonly) QpsRawSensor *accelerationSensor __attribute__((swift_name("accelerationSensor")));
@property (readonly) QpsRawSensor * _Nullable accelerationSensorUncalibrated __attribute__((swift_name("accelerationSensorUncalibrated")));
@property (readonly) QpsRawSensor *gravitySensor __attribute__((swift_name("gravitySensor")));
@property (readonly) QpsRawSensor * _Nullable gyroscopeSensorUncalibrated __attribute__((swift_name("gyroscopeSensorUncalibrated")));
@property (readonly) QpsRawSensor * _Nullable orientationSensor __attribute__((swift_name("orientationSensor")));
@property (readonly) QpsRawSensor *rotationSensor __attribute__((swift_name("rotationSensor")));
@property (readonly) QpsIQPSSystemType *systemType __attribute__((swift_name("systemType")));
@end;

__attribute__((swift_name("IQPSReplayInteractor")))
@protocol QpsIQPSReplayInteractor
@required
- (NSString * _Nullable)getDataIdentifier:(NSString *)identifier printMissingFileException:(BOOL)printMissingFileException __attribute__((swift_name("getData(identifier:printMissingFileException:)")));
- (void)postDataData:(NSString *)data identifier:(NSString *)identifier __attribute__((swift_name("postData(data:identifier:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IQPSReplayInteractorCompanion")))
@interface QpsIQPSReplayInteractorCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((swift_name("IQPSSyncData")))
@protocol QpsIQPSSyncData
@required
@property (readonly) BOOL isValidSyncRotation __attribute__((swift_name("isValidSyncRotation")));
@property (readonly) NSArray<QpsPointWithOffset *> *positions __attribute__((swift_name("positions")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IQPSSystemType")))
@interface QpsIQPSSystemType : QpsKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) QpsIQPSSystemType *android __attribute__((swift_name("android")));
@property (class, readonly) QpsIQPSSystemType *ios __attribute__((swift_name("ios")));
- (int32_t)compareToOther:(QpsIQPSSystemType *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("IQPSUserSettings")))
@protocol QpsIQPSUserSettings
@required
@property double ecsuValue __attribute__((swift_name("ecsuValue")));
@property float legRatio __attribute__((swift_name("legRatio")));
@property float userHeight __attribute__((swift_name("userHeight")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QPSHandler")))
@interface QpsQPSHandler : QpsBase <QpsIQPSInteractorOutput>
- (instancetype)initWithRawSensorManager:(id<QpsIQPSRawSensorManager>)rawSensorManager interactor:(id<QpsIQPSInteractor>)interactor replayInteractor:(id<QpsIQPSReplayInteractor>)replayInteractor mapInformation:(id<QpsIQPSMapInformation>)mapInformation userSettings:(id<QpsIQPSUserSettings>)userSettings playback:(BOOL)playback __attribute__((swift_name("init(rawSensorManager:interactor:replayInteractor:mapInformation:userSettings:playback:)"))) __attribute__((objc_designated_initializer));
- (void)doInitPositionSyncEvent __attribute__((swift_name("doInitPositionSyncEvent()")));
- (void)onDeviceOrientationChangedOrientation:(QpsIQPSDeviceOrientation *)orientation __attribute__((swift_name("onDeviceOrientationChanged(orientation:)")));
- (void)onIllegalBehaviour __attribute__((swift_name("onIllegalBehaviour()")));
- (void)onPositionSyncEventData:(id<QpsIQPSSyncData>)data __attribute__((swift_name("onPositionSyncEvent(data:)")));
- (void)onSensorsInitiated __attribute__((swift_name("onSensorsInitiated()")));
- (void)startDebugStartPos:(QpsPointF *)startPos startAngle:(double)startAngle __attribute__((swift_name("startDebug(startPos:startAngle:)")));
- (void)startNavigationStartPos:(QpsPointF *)startPos startAngle:(double)startAngle __attribute__((swift_name("startNavigation(startPos:startAngle:)")));
- (void)startRecording __attribute__((swift_name("startRecording()")));
- (void)stopNavigation __attribute__((swift_name("stopNavigation()")));
- (void)stopRecording __attribute__((swift_name("stopRecording()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QPSHandler.Companion")))
@interface QpsQPSHandlerCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QPSValues")))
@interface QpsQPSValues : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)qPSValues __attribute__((swift_name("init()")));
@property (readonly) double ACCEPTANCE_SLIDING_RATIO __attribute__((swift_name("ACCEPTANCE_SLIDING_RATIO")));
@property (readonly) double ANGLE_OFFSET_MAGNITUDE __attribute__((swift_name("ANGLE_OFFSET_MAGNITUDE")));
@property (readonly) double ANGLE_PID_DERIVATIVE_CONSTANT __attribute__((swift_name("ANGLE_PID_DERIVATIVE_CONSTANT")));
@property (readonly) double ANGLE_PID_INTEGRATION_CONSTANT __attribute__((swift_name("ANGLE_PID_INTEGRATION_CONSTANT")));
@property double ANGLE_PID_MAX_OFFSET __attribute__((swift_name("ANGLE_PID_MAX_OFFSET")));
@property double ANGLE_PID_PROPORTIONAL_CONSTANT __attribute__((swift_name("ANGLE_PID_PROPORTIONAL_CONSTANT")));
@property (readonly) int64_t ANGLE_RESOLUTION __attribute__((swift_name("ANGLE_RESOLUTION")));
@property double CLOSE_POSITION_THRESHOLD __attribute__((swift_name("CLOSE_POSITION_THRESHOLD")));
@property (readonly) double CLUSTERING_RAYTRACE_RESOLUTION __attribute__((swift_name("CLUSTERING_RAYTRACE_RESOLUTION")));
@property (readonly) int32_t CLUSTER_DEATH_BOUND __attribute__((swift_name("CLUSTER_DEATH_BOUND")));
@property (readonly) double CLUSTER_DISJOINT_TOLERANCE __attribute__((swift_name("CLUSTER_DISJOINT_TOLERANCE")));
@property (readonly) int32_t CLUSTER_LOWER_BOUND __attribute__((swift_name("CLUSTER_LOWER_BOUND")));
@property (readonly) int32_t COLLISION_DEAD_SENSITIVITY __attribute__((swift_name("COLLISION_DEAD_SENSITIVITY")));
@property (readonly) int32_t COLLISION_TIMER __attribute__((swift_name("COLLISION_TIMER")));
@property (readonly) int32_t CONVEX_TIMER __attribute__((swift_name("CONVEX_TIMER")));
@property float DEFAULT_LEG_RATIO_AH __attribute__((swift_name("DEFAULT_LEG_RATIO_AH")));
@property BOOL DEVICE_IS_ANDROID __attribute__((swift_name("DEVICE_IS_ANDROID")));
@property (readonly) double DISTANCE_FOR_MAX_SPEED __attribute__((swift_name("DISTANCE_FOR_MAX_SPEED")));
@property (readonly) int32_t FORCE_SPAWN_POSITION_AMOUNT_TRESHOLD __attribute__((swift_name("FORCE_SPAWN_POSITION_AMOUNT_TRESHOLD")));
@property (readonly) int32_t GENERIC_EXIT_TOLERANCE __attribute__((swift_name("GENERIC_EXIT_TOLERANCE")));
@property double GENERIC_RAYTRACE_RESOLUTION __attribute__((swift_name("GENERIC_RAYTRACE_RESOLUTION")));
@property (readonly) float GRAVITY __attribute__((swift_name("GRAVITY")));
@property (readonly) double HEAD_ON_CHECK_DISTANCE_MODIFIER __attribute__((swift_name("HEAD_ON_CHECK_DISTANCE_MODIFIER")));
@property (readonly) int32_t HEAD_ON_COLLISION_STABILITY_TOLERANCE __attribute__((swift_name("HEAD_ON_COLLISION_STABILITY_TOLERANCE")));
@property int32_t HEALTHY_POSITION_AMOUNT __attribute__((swift_name("HEALTHY_POSITION_AMOUNT")));
@property double HYPERBOLA_ANGLE __attribute__((swift_name("HYPERBOLA_ANGLE")));
@property double HYPERBOLA_INNER_CONSTANT __attribute__((swift_name("HYPERBOLA_INNER_CONSTANT")));
@property double HYPERBOLA_X_SHIFT __attribute__((swift_name("HYPERBOLA_X_SHIFT")));
@property double HYPERBOLA_Y_SHIFT __attribute__((swift_name("HYPERBOLA_Y_SHIFT")));
@property float MAGIC_CONSTANT __attribute__((swift_name("MAGIC_CONSTANT")));
@property int32_t MAX_NUMBER_OF_POINTS __attribute__((swift_name("MAX_NUMBER_OF_POINTS")));
@property double MAX_SPEED __attribute__((swift_name("MAX_SPEED")));
@property (readonly) int32_t MAX_STEP_TIME __attribute__((swift_name("MAX_STEP_TIME")));
@property (readonly) int32_t MAX_TRIES_BEFORE_FORCED_SPAWN __attribute__((swift_name("MAX_TRIES_BEFORE_FORCED_SPAWN")));
@property (readonly) int32_t MINIMUM_CLUSTER_SIZE __attribute__((swift_name("MINIMUM_CLUSTER_SIZE")));
@property (readonly) int32_t MIN_STEP_TIME __attribute__((swift_name("MIN_STEP_TIME")));
@property (readonly) int32_t NUMBER_OF_DIRECTIONS __attribute__((swift_name("NUMBER_OF_DIRECTIONS")));
@property (readonly) int32_t NUMBER_OF_FORCED_SPAWNS __attribute__((swift_name("NUMBER_OF_FORCED_SPAWNS")));
@property (readonly) double OPTIMAL_CLUSTER_RADIUS __attribute__((swift_name("OPTIMAL_CLUSTER_RADIUS")));
@property (readonly) double PID_DISTANCE_RESOLUTION __attribute__((swift_name("PID_DISTANCE_RESOLUTION")));
@property (readonly) double PID_DISTANCE_THRESHOLD __attribute__((swift_name("PID_DISTANCE_THRESHOLD")));
@property double PRIO_CLUSTER_SIZE_PROP_THRESHOLD __attribute__((swift_name("PRIO_CLUSTER_SIZE_PROP_THRESHOLD")));
@property (readonly) int32_t RAYTRACE_MAX_DISTANCE __attribute__((swift_name("RAYTRACE_MAX_DISTANCE")));
@property double RECOVERY_SPREAD_RADIUS __attribute__((swift_name("RECOVERY_SPREAD_RADIUS")));
@property (readonly) double RECOVERY_SPREAD_RADIUS_DEFAULT_VALUE __attribute__((swift_name("RECOVERY_SPREAD_RADIUS_DEFAULT_VALUE")));
@property (readonly) int32_t RESCUE_MODE_MAX_ALL_COLLIDED_TRIES __attribute__((swift_name("RESCUE_MODE_MAX_ALL_COLLIDED_TRIES")));
@property (readonly) double RESCUE_SPREAD_DISTANCE __attribute__((swift_name("RESCUE_SPREAD_DISTANCE")));
@property (readonly) double RESCUE_SPREAD_RADIUS __attribute__((swift_name("RESCUE_SPREAD_RADIUS")));
@property (readonly) int32_t RETURN_FROM_COLLISION_SENSITIVITY __attribute__((swift_name("RETURN_FROM_COLLISION_SENSITIVITY")));
@property (readonly) int64_t SECOND __attribute__((swift_name("SECOND")));
@property (readonly) int32_t SIDE_COLLISION_MAX_AGE __attribute__((swift_name("SIDE_COLLISION_MAX_AGE")));
@property (readonly) int32_t SIDE_COLLISION_STABILITY_TOLERANCE __attribute__((swift_name("SIDE_COLLISION_STABILITY_TOLERANCE")));
@property double SLIDING_ANGLE __attribute__((swift_name("SLIDING_ANGLE")));
@property (readonly) int32_t SMALL_CLUSTER_SIZE __attribute__((swift_name("SMALL_CLUSTER_SIZE")));
@property (readonly) double SPEED_PID_MAX_OFFSET __attribute__((swift_name("SPEED_PID_MAX_OFFSET")));
@property double SPEED_PID_PROPORTIONAL_CONSTANT __attribute__((swift_name("SPEED_PID_PROPORTIONAL_CONSTANT")));
@property (readonly) double SPLIT_ANGLE __attribute__((swift_name("SPLIT_ANGLE")));
@property float SPLIT_METERS __attribute__((swift_name("SPLIT_METERS")));
@property int32_t STABLE_DIRECTION_TICK_THRESHOLD __attribute__((swift_name("STABLE_DIRECTION_TICK_THRESHOLD")));
@property (readonly) double START_SPREAD_DISTANCE __attribute__((swift_name("START_SPREAD_DISTANCE")));
@property (readonly) double START_SPREAD_RADIUS __attribute__((swift_name("START_SPREAD_RADIUS")));
@property (readonly) double SYNC_POSITION_THRESHOLD __attribute__((swift_name("SYNC_POSITION_THRESHOLD")));
@property (readonly) double TAU __attribute__((swift_name("TAU")));
@property (readonly) NSString *TEST_NUMBER __attribute__((swift_name("TEST_NUMBER")));
@property int64_t TIMED_DIRECTION_HISTORY_HANDLER_DEFAULT_TIMESPAN __attribute__((swift_name("TIMED_DIRECTION_HISTORY_HANDLER_DEFAULT_TIMESPAN")));
@property int64_t TROLLEY_FILL_TIME __attribute__((swift_name("TROLLEY_FILL_TIME")));
@property float TROLLEY_MAX_SPEED __attribute__((swift_name("TROLLEY_MAX_SPEED")));
@property float TROLLEY_MIN_SPEED __attribute__((swift_name("TROLLEY_MIN_SPEED")));
@property (readonly) float TROLLEY_SPEED __attribute__((swift_name("TROLLEY_SPEED")));
@property int32_t TROLLEY_SPEED_CALIBRATION __attribute__((swift_name("TROLLEY_SPEED_CALIBRATION")));
@property float TROLLEY_SPEED_COEF_DISPLACEMENT __attribute__((swift_name("TROLLEY_SPEED_COEF_DISPLACEMENT")));
@property float TROLLEY_SPEED_COEF_SPEED __attribute__((swift_name("TROLLEY_SPEED_COEF_SPEED")));
@property float TROLLEY_SPEED_COEF_VALLEY __attribute__((swift_name("TROLLEY_SPEED_COEF_VALLEY")));
@property float TROLLEY_SPEED_INTERCEPT __attribute__((swift_name("TROLLEY_SPEED_INTERCEPT")));
@property float TROLLEY_SPEED_MULTIPLIER __attribute__((swift_name("TROLLEY_SPEED_MULTIPLIER")));
@property (readonly) float TROLLEY_USER_HEIGHT_RECIPROCAL_CONSTANT __attribute__((swift_name("TROLLEY_USER_HEIGHT_RECIPROCAL_CONSTANT")));
@property double TURN_CONSTANT __attribute__((swift_name("TURN_CONSTANT")));
@property double TURN_DEAD_ZONE __attribute__((swift_name("TURN_DEAD_ZONE")));
@property double TURN_MINIMUM __attribute__((swift_name("TURN_MINIMUM")));
@property (readonly) int32_t USER_SETTINGS_UPDATE_TICKS __attribute__((swift_name("USER_SETTINGS_UPDATE_TICKS")));
@property (readonly) int32_t WALKING_STABILITY_TOLERANCE __attribute__((swift_name("WALKING_STABILITY_TOLERANCE")));
@property float WEAK_X __attribute__((swift_name("WEAK_X")));
@property (readonly) QpsMoveVector *ZERO_VECTOR __attribute__((swift_name("ZERO_VECTOR")));
@end;

__attribute__((swift_name("IStepListener")))
@protocol QpsIStepListener
@required
- (void)onSensorsInitiated __attribute__((swift_name("onSensorsInitiated()")));
- (void)onStepDetectedStep:(QpsStepData *)step __attribute__((swift_name("onStepDetected(step:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StepDetectorManager")))
@interface QpsStepDetectorManager : QpsBase <QpsIStepListener>
- (instancetype)initWithRawSensorList:(NSArray<QpsRawSensor *> *)rawSensorList stepListener:(id<QpsIStepListener>)stepListener onIllegalBehaviour:(void (^)(void))onIllegalBehaviour onSensorsInitiated:(void (^)(void))onSensorsInitiated __attribute__((swift_name("init(rawSensorList:stepListener:onIllegalBehaviour:onSensorsInitiated:)"))) __attribute__((objc_designated_initializer));
- (void)changeStateStateType:(QpsStepStateStepStateType *)stateType __attribute__((swift_name("changeState(stateType:)")));
- (void)onSensorsInitiated __attribute__((swift_name("onSensorsInitiated()")));
- (void)onStepDetectedStep:(QpsStepData *)step __attribute__((swift_name("onStepDetected(step:)")));
- (void)stop __attribute__((swift_name("stop()")));
@property (readonly) id<QpsIStepListener> stepListener __attribute__((swift_name("stepListener")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StepDetectorManager.Companion")))
@interface QpsStepDetectorManagerCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StepFillEvaluator")))
@interface QpsStepFillEvaluator : QpsBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)handleFillAlgoritmCurrentState:(QpsStepState *)currentState step:(QpsStepData *)step __attribute__((swift_name("handleFillAlgoritm(currentState:step:)")));
- (void)handleLeniencyStep:(QpsStepData *)step callback:(void (^)(QpsStepFillEvaluatorSET_LENIENCY *))callback __attribute__((swift_name("handleLeniency(step:callback:)")));
- (void)handleTrolleyFillAlgorithmCurrentState:(QpsStepState *)currentState step:(QpsSpackStepData *)step __attribute__((swift_name("handleTrolleyFillAlgorithm(currentState:step:)")));
- (BOOL)isTrolleyStepStep:(QpsStepData *)step __attribute__((swift_name("isTrolleyStep(step:)")));
- (BOOL)validTimeStampStep:(QpsStepData *)step __attribute__((swift_name("validTimeStamp(step:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StepFillEvaluator.Companion")))
@interface QpsStepFillEvaluatorCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StepFillEvaluator.SET_LENIENCY")))
@interface QpsStepFillEvaluatorSET_LENIENCY : QpsKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) QpsStepFillEvaluatorSET_LENIENCY *setLenient __attribute__((swift_name("setLenient")));
@property (class, readonly) QpsStepFillEvaluatorSET_LENIENCY *setStrict __attribute__((swift_name("setStrict")));
- (int32_t)compareToOther:(QpsStepFillEvaluatorSET_LENIENCY *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("Observer")))
@protocol QpsObserver
@required
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StepStateEvaluator")))
@interface QpsStepStateEvaluator : QpsBase <QpsObserver>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)evaluateStateStepDetectorManager:(QpsStepDetectorManager *)stepDetectorManager __attribute__((swift_name("evaluateState(stepDetectorManager:)")));
- (void)initiateRawSensorList:(NSArray<QpsRawSensor *> *)rawSensorList __attribute__((swift_name("initiate(rawSensorList:)")));
- (void)stopRawSensorList:(NSArray<QpsRawSensor *> *)rawSensorList __attribute__((swift_name("stop(rawSensorList:)")));
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
@property int64_t currentTime __attribute__((swift_name("currentTime")));
@end;

__attribute__((swift_name("StepData")))
@interface QpsStepData : QpsBase
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type confidence:(double)confidence timestamp:(int64_t)timestamp keyValues:(QpsKeyValueData * _Nullable)keyValues success:(BOOL)success stepDetector:(QpsStepDetector *)stepDetector duration:(int64_t)duration speedData:(QpsSpeedData *)speedData __attribute__((swift_name("init(type:confidence:timestamp:keyValues:success:stepDetector:duration:speedData:)"))) __attribute__((objc_designated_initializer));
- (void)reEvaluate __attribute__((swift_name("reEvaluate()")));
- (void)setIntegratedAccIntegratedAccelerationIn:(QpsSpeedData *)integratedAccelerationIn __attribute__((swift_name("setIntegratedAcc(integratedAccelerationIn:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) double confidence __attribute__((swift_name("confidence")));
@property int64_t duration __attribute__((swift_name("duration")));
@property (readonly) QpsKeyValueData * _Nullable keyValues __attribute__((swift_name("keyValues")));
@property (readonly) BOOL reEvaluateable __attribute__((swift_name("reEvaluateable")));
@property QpsSpeedData *speedData __attribute__((swift_name("speedData")));
@property QpsStepDetector *stepDetector __attribute__((swift_name("stepDetector")));
@property NSString *stringTag __attribute__((swift_name("stringTag")));
@property BOOL success __attribute__((swift_name("success")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@property (readonly) QpsIQPSDeviceOrientation *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FrequencyStepData")))
@interface QpsFrequencyStepData : QpsStepData
- (instancetype)initWithConfidence:(double)confidence timestamp:(int64_t)timestamp success:(BOOL)success stepDetector:(QpsStepDetector *)stepDetector duration:(int64_t)duration __attribute__((swift_name("init(confidence:timestamp:success:stepDetector:duration:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type confidence:(double)confidence timestamp:(int64_t)timestamp keyValues:(QpsKeyValueData * _Nullable)keyValues success:(BOOL)success stepDetector:(QpsStepDetector *)stepDetector duration:(int64_t)duration speedData:(QpsSpeedData *)speedData __attribute__((swift_name("init(type:confidence:timestamp:keyValues:success:stepDetector:duration:speedData:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL reEvaluatable __attribute__((swift_name("reEvaluatable")));
@end;

__attribute__((swift_name("KeyValueData")))
@interface QpsKeyValueData : QpsBase
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type __attribute__((swift_name("init(type:)"))) __attribute__((objc_designated_initializer));
@property (readonly) QpsIQPSDeviceOrientation *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LinearSVMSwingStepData")))
@interface QpsLinearSVMSwingStepData : QpsStepData
- (instancetype)initWithKeyValues:(QpsKeyValueData * _Nullable)keyValues confidence:(double)confidence timestamp:(int64_t)timestamp success:(BOOL)success stepDetector:(QpsStepDetector *)stepDetector duration:(int64_t)duration __attribute__((swift_name("init(keyValues:confidence:timestamp:success:stepDetector:duration:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type confidence:(double)confidence timestamp:(int64_t)timestamp keyValues:(QpsKeyValueData * _Nullable)keyValues success:(BOOL)success stepDetector:(QpsStepDetector *)stepDetector duration:(int64_t)duration speedData:(QpsSpeedData *)speedData __attribute__((swift_name("init(type:confidence:timestamp:keyValues:success:stepDetector:duration:speedData:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL reEvaluatable __attribute__((swift_name("reEvaluatable")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RegularKeyValueData")))
@interface QpsRegularKeyValueData : QpsKeyValueData
- (instancetype)initWithPeak:(float)peak steepness:(float)steepness valley:(float)valley duration:(int64_t)duration gravityAlignment:(float)gravityAlignment __attribute__((swift_name("init(peak:steepness:valley:duration:gravityAlignment:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type __attribute__((swift_name("init(type:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (NSString *)description __attribute__((swift_name("description()")));
@property int64_t duration __attribute__((swift_name("duration")));
@property float gravityAlignment __attribute__((swift_name("gravityAlignment")));
@property float peak __attribute__((swift_name("peak")));
@property float steepness __attribute__((swift_name("steepness")));
@property float valley __attribute__((swift_name("valley")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RegularStepData")))
@interface QpsRegularStepData : QpsStepData
- (instancetype)initWithKeyValues:(QpsRegularKeyValueData *)keyValues confidence:(double)confidence timestamp:(int64_t)timestamp success:(BOOL)success stepDetector:(QpsStepDetector *)stepDetector duration:(int64_t)duration __attribute__((swift_name("init(keyValues:confidence:timestamp:success:stepDetector:duration:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type confidence:(double)confidence timestamp:(int64_t)timestamp keyValues:(QpsKeyValueData * _Nullable)keyValues success:(BOOL)success stepDetector:(QpsStepDetector *)stepDetector duration:(int64_t)duration speedData:(QpsSpeedData *)speedData __attribute__((swift_name("init(type:confidence:timestamp:keyValues:success:stepDetector:duration:speedData:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SpackStepData")))
@interface QpsSpackStepData : QpsStepData
- (instancetype)initWithSpack:(QpsSpack *)spack confidence:(double)confidence timestamp:(int64_t)timestamp success:(BOOL)success stepDetector:(QpsStepDetector *)stepDetector duration:(int64_t)duration type:(QpsIQPSDeviceOrientation *)type speedData:(QpsSpeedData *)speedData __attribute__((swift_name("init(spack:confidence:timestamp:success:stepDetector:duration:type:speedData:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type confidence:(double)confidence timestamp:(int64_t)timestamp keyValues:(QpsKeyValueData * _Nullable)keyValues success:(BOOL)success stepDetector:(QpsStepDetector *)stepDetector duration:(int64_t)duration speedData:(QpsSpeedData *)speedData __attribute__((swift_name("init(type:confidence:timestamp:keyValues:success:stepDetector:duration:speedData:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (NSString *)description __attribute__((swift_name("description()")));
@property BOOL first __attribute__((swift_name("first")));
@property (readonly) QpsSpack *spack __attribute__((swift_name("spack")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SwingKeyValueData")))
@interface QpsSwingKeyValueData : QpsKeyValueData
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type __attribute__((swift_name("init(type:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SwingStepData")))
@interface QpsSwingStepData : QpsStepData
- (instancetype)initWithKeyValues:(QpsKeyValueData * _Nullable)keyValues confidence:(double)confidence timestamp:(int64_t)timestamp success:(BOOL)success stepDetector:(QpsStepDetector *)stepDetector duration:(int64_t)duration __attribute__((swift_name("init(keyValues:confidence:timestamp:success:stepDetector:duration:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type confidence:(double)confidence timestamp:(int64_t)timestamp keyValues:(QpsKeyValueData * _Nullable)keyValues success:(BOOL)success stepDetector:(QpsStepDetector *)stepDetector duration:(int64_t)duration speedData:(QpsSpeedData *)speedData __attribute__((swift_name("init(type:confidence:timestamp:keyValues:success:stepDetector:duration:speedData:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("StepDetector")))
@interface QpsStepDetector : QpsBase <QpsObserver>
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type sensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener> _Nullable)stepListener __attribute__((swift_name("init(type:sensorList:stepListener:)"))) __attribute__((objc_designated_initializer));
- (void)onEnter __attribute__((swift_name("onEnter()")));
- (void)onExit __attribute__((swift_name("onExit()")));
- (void)onSensorInitiated __attribute__((swift_name("onSensorInitiated()")));
- (void)onStepDetectedStep:(QpsStepData *)step __attribute__((swift_name("onStepDetected(step:)")));
- (void)setToLenient __attribute__((swift_name("setToLenient()")));
- (void)setToStrict __attribute__((swift_name("setToStrict()")));
- (void)stop __attribute__((swift_name("stop()")));
- (BOOL)weakEvaluateInData:(QpsStepData *)inData __attribute__((swift_name("weakEvaluate(inData:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuxiliaryStochasticSwingStepDetector")))
@interface QpsAuxiliaryStochasticSwingStepDetector : QpsStepDetector
- (instancetype)initWithSensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener>)stepListener onIllegalBehaviour:(void (^)(void))onIllegalBehaviour __attribute__((swift_name("init(sensorList:stepListener:onIllegalBehaviour:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type sensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener> _Nullable)stepListener __attribute__((swift_name("init(type:sensorList:stepListener:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)onEnter __attribute__((swift_name("onEnter()")));
- (void)setToLenient __attribute__((swift_name("setToLenient()")));
- (void)setToStrict __attribute__((swift_name("setToStrict()")));
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
- (BOOL)weakEvaluateInData:(QpsStepData *)step __attribute__((swift_name("weakEvaluate(inData:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuxiliaryStochasticSwingStepDetector.Companion")))
@interface QpsAuxiliaryStochasticSwingStepDetectorCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FrequencySwingStepDetector")))
@interface QpsFrequencySwingStepDetector : QpsStepDetector
- (instancetype)initWithSensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener>)stepListener __attribute__((swift_name("init(sensorList:stepListener:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type sensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener> _Nullable)stepListener __attribute__((swift_name("init(type:sensorList:stepListener:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)setToLenient __attribute__((swift_name("setToLenient()")));
- (void)setToStrict __attribute__((swift_name("setToStrict()")));
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
- (BOOL)weakEvaluateInData:(QpsStepData *)inData __attribute__((swift_name("weakEvaluate(inData:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FrequencySwingStepDetector.Companion")))
@interface QpsFrequencySwingStepDetectorCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FrequencySwingStepDetector.Frequency")))
@interface QpsFrequencySwingStepDetectorFrequency : QpsBase
- (instancetype)initWith:(QpsFrequencySwingStepDetector *)receiver __attribute__((swift_name("init(_:)"))) __attribute__((objc_designated_initializer));
- (void)addMaxDataValue:(float)value __attribute__((swift_name("addMaxData(value:)")));
- (void)addMinData __attribute__((swift_name("addMinData()")));
- (void)clearStepDataCache __attribute__((swift_name("clearStepDataCache()")));
- (NSArray<QpsStepData *> *)getStepDataCache __attribute__((swift_name("getStepDataCache()")));
- (void)tryValueValue:(float)value __attribute__((swift_name("tryValue(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RBFSVMRegularStepDetector")))
@interface QpsRBFSVMRegularStepDetector : QpsStepDetector
- (instancetype)initWithSensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener>)stepListener svm:(QpsSVM *)svm __attribute__((swift_name("init(sensorList:stepListener:svm:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type sensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener> _Nullable)stepListener __attribute__((swift_name("init(type:sensorList:stepListener:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)setToLenient __attribute__((swift_name("setToLenient()")));
- (void)setToStrict __attribute__((swift_name("setToStrict()")));
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
- (BOOL)weakEvaluateInData:(QpsStepData *)inData __attribute__((swift_name("weakEvaluate(inData:)")));
@property (readonly) QpsSVM *svm __attribute__((swift_name("svm")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RBFSVMRegularStepDetector.Companion")))
@interface QpsRBFSVMRegularStepDetectorCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RBFSVMSwingStepDetector")))
@interface QpsRBFSVMSwingStepDetector : QpsStepDetector
- (instancetype)initWithSensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener>)stepListener svm:(QpsSVM *)svm __attribute__((swift_name("init(sensorList:stepListener:svm:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type sensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener> _Nullable)stepListener __attribute__((swift_name("init(type:sensorList:stepListener:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)setToLenient __attribute__((swift_name("setToLenient()")));
- (void)setToStrict __attribute__((swift_name("setToStrict()")));
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
- (BOOL)weakEvaluateInData:(QpsStepData *)inData __attribute__((swift_name("weakEvaluate(inData:)")));
@property (readonly) QpsSVM *svm __attribute__((swift_name("svm")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RBFSVMSwingStepDetector.Companion")))
@interface QpsRBFSVMSwingStepDetectorCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SpackStepDetector")))
@interface QpsSpackStepDetector : QpsStepDetector
- (instancetype)initWithSensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener>)stepListener svm:(QpsSVM *)svm weakEllipseEvaluator:(QpsEllipseEvaluator *)weakEllipseEvaluator lenientEllipseEvaluator:(QpsEllipseEvaluator *)lenientEllipseEvaluator lowCutoff:(QpsDouble * _Nullable)lowCutoff highCutoff:(QpsDouble * _Nullable)highCutoff ghettoOffset:(int32_t)ghettoOffset __attribute__((swift_name("init(sensorList:stepListener:svm:weakEllipseEvaluator:lenientEllipseEvaluator:lowCutoff:highCutoff:ghettoOffset:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type sensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener> _Nullable)stepListener __attribute__((swift_name("init(type:sensorList:stepListener:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)setToLenient __attribute__((swift_name("setToLenient()")));
- (void)setToStrict __attribute__((swift_name("setToStrict()")));
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
- (BOOL)weakEvaluateInData:(QpsStepData *)inData __attribute__((swift_name("weakEvaluate(inData:)")));
@property (readonly) QpsSVM *svm __attribute__((swift_name("svm")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SpackStepDetector.Companion")))
@interface QpsSpackStepDetectorCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StartStep")))
@interface QpsStartStep : QpsBase
- (instancetype)initWithLen:(int32_t)len __attribute__((swift_name("init(len:)"))) __attribute__((objc_designated_initializer));
- (BOOL)addStepElement:(double)element __attribute__((swift_name("addStep(element:)")));
- (double)getFrequency __attribute__((swift_name("getFrequency()")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (int32_t)size __attribute__((swift_name("size()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSMutableArray<QpsDouble *> *x __attribute__((swift_name("x")));
@property NSMutableArray<QpsDouble *> *y __attribute__((swift_name("y")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StochasticRegularStepDetector")))
@interface QpsStochasticRegularStepDetector : QpsStepDetector
- (instancetype)initWithSensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener>)stepListener __attribute__((swift_name("init(sensorList:stepListener:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type sensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener> _Nullable)stepListener __attribute__((swift_name("init(type:sensorList:stepListener:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)setToLenient __attribute__((swift_name("setToLenient()")));
- (void)setToStrict __attribute__((swift_name("setToStrict()")));
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
- (BOOL)weakEvaluateInData:(QpsStepData *)inData __attribute__((swift_name("weakEvaluate(inData:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StochasticRegularStepDetector.Companion")))
@interface QpsStochasticRegularStepDetectorCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StochasticSwingStepDetector")))
@interface QpsStochasticSwingStepDetector : QpsStepDetector
- (instancetype)initWithSensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener>)stepListener __attribute__((swift_name("init(sensorList:stepListener:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type sensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener> _Nullable)stepListener __attribute__((swift_name("init(type:sensorList:stepListener:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)setToLenient __attribute__((swift_name("setToLenient()")));
- (void)setToStrict __attribute__((swift_name("setToStrict()")));
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
- (BOOL)weakEvaluateInData:(QpsStepData *)step __attribute__((swift_name("weakEvaluate(inData:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StochasticSwingStepDetector.Companion")))
@interface QpsStochasticSwingStepDetectorCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SwingStepDetector")))
@interface QpsSwingStepDetector : QpsStepDetector
- (instancetype)initWithSensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener>)stepListener __attribute__((swift_name("init(sensorList:stepListener:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type sensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener> _Nullable)stepListener __attribute__((swift_name("init(type:sensorList:stepListener:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (BOOL)isCurveStepStep:(NSArray<QpsFloat *> *)step __attribute__((swift_name("isCurveStep(step:)")));
- (void)setToLenient __attribute__((swift_name("setToLenient()")));
- (void)setToStrict __attribute__((swift_name("setToStrict()")));
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
- (BOOL)weakEvaluateInData:(QpsStepData *)inData __attribute__((swift_name("weakEvaluate(inData:)")));
@property int32_t historyLength __attribute__((swift_name("historyLength")));
@property int32_t timesPrecheck __attribute__((swift_name("timesPrecheck")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SwingStepDetector.Companion")))
@interface QpsSwingStepDetectorCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TrolleySVMStepDetector")))
@interface QpsTrolleySVMStepDetector : QpsStepDetector
- (instancetype)initWithSensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener>)stepListener svm:(QpsSVM *)svm __attribute__((swift_name("init(sensorList:stepListener:svm:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithType:(QpsIQPSDeviceOrientation *)type sensorList:(NSArray<QpsRawSensor *> *)sensorList stepListener:(id<QpsIStepListener> _Nullable)stepListener __attribute__((swift_name("init(type:sensorList:stepListener:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)setToLenient __attribute__((swift_name("setToLenient()")));
- (void)setToStrict __attribute__((swift_name("setToStrict()")));
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
- (BOOL)weakEvaluateInData:(QpsStepData *)inData __attribute__((swift_name("weakEvaluate(inData:)")));
@property (readonly) QpsSVM *svm __attribute__((swift_name("svm")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TrolleySVMStepDetector.Companion")))
@interface QpsTrolleySVMStepDetectorCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((swift_name("StepState")))
@interface QpsStepState : QpsBase <QpsIStepListener>
- (instancetype)initWithStepListener:(id<QpsIStepListener> _Nullable)stepListener stepDetectorList:(NSArray<QpsStepDetector *> *)stepDetectorList __attribute__((swift_name("init(stepListener:stepDetectorList:)"))) __attribute__((objc_designated_initializer));
- (void)onEnter __attribute__((swift_name("onEnter()")));
- (void)onExit __attribute__((swift_name("onExit()")));
- (void)onStepDetectedStep:(QpsStepData *)step __attribute__((swift_name("onStepDetected(step:)")));
- (void)setToLenient __attribute__((swift_name("setToLenient()")));
- (void)setToStrict __attribute__((swift_name("setToStrict()")));
- (void)stop __attribute__((swift_name("stop()")));
- (void)update __attribute__((swift_name("update()")));
@property id<QpsIStepListener> _Nullable stepListener __attribute__((swift_name("stepListener")));
@property (readonly) QpsStepStateStepStateType *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RegularState")))
@interface QpsRegularState : QpsStepState
- (instancetype)initWithStepListener:(id<QpsIStepListener>)stepListener stepDetectorList:(NSArray<QpsStepDetector *> *)stepDetectorList __attribute__((swift_name("init(stepListener:stepDetectorList:)"))) __attribute__((objc_designated_initializer));
- (void)onSensorsInitiated __attribute__((swift_name("onSensorsInitiated()")));
- (void)onStepDetectedStep:(QpsStepData *)step __attribute__((swift_name("onStepDetected(step:)")));
@property (readonly) QpsStepStateStepStateType *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RegularState.Companion")))
@interface QpsRegularStateCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StepState.Companion")))
@interface QpsStepStateCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StepState.StepStateType")))
@interface QpsStepStateStepStateType : QpsKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) QpsStepStateStepStateType *regular __attribute__((swift_name("regular")));
@property (class, readonly) QpsStepStateStepStateType *swing __attribute__((swift_name("swing")));
@property (class, readonly) QpsStepStateStepStateType *trolley __attribute__((swift_name("trolley")));
- (int32_t)compareToOther:(QpsStepStateStepStateType *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SwingState")))
@interface QpsSwingState : QpsStepState
- (instancetype)initWithStepListener:(id<QpsIStepListener>)stepListener stepDetectorList:(NSArray<QpsStepDetector *> *)stepDetectorList __attribute__((swift_name("init(stepListener:stepDetectorList:)"))) __attribute__((objc_designated_initializer));
- (void)onEnter __attribute__((swift_name("onEnter()")));
- (void)onSensorsInitiated __attribute__((swift_name("onSensorsInitiated()")));
@property (readonly) QpsStepStateStepStateType *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SwingState.Companion")))
@interface QpsSwingStateCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TrolleyState")))
@interface QpsTrolleyState : QpsStepState
- (instancetype)initWithStepListener:(id<QpsIStepListener>)stepListener stepDetectorList:(NSArray<QpsStepDetector *> *)stepDetectorList __attribute__((swift_name("init(stepListener:stepDetectorList:)"))) __attribute__((objc_designated_initializer));
- (void)onSensorsInitiated __attribute__((swift_name("onSensorsInitiated()")));
@property (readonly) QpsStepStateStepStateType *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TrolleyState.Companion")))
@interface QpsTrolleyStateCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@end;

__attribute__((swift_name("Allpass")))
@interface QpsAllpass : QpsBase
- (instancetype)initWithOrder:(int32_t)order __attribute__((swift_name("init(order:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithA:(QpsPolynomial *)A __attribute__((swift_name("init(A:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithK:(QpsKotlinDoubleArray *)k __attribute__((swift_name("init(k:)"))) __attribute__((objc_designated_initializer));
- (void)constructRationalRepresentation __attribute__((swift_name("constructRationalRepresentation()")));
- (QpsComplex *)evaluateOmega:(double)omega __attribute__((swift_name("evaluate(omega:)")));
- (float)filterX:(float)x __attribute__((swift_name("filter(x:)")));
- (void)filterX_:(QpsKotlinFloatArray *)x __attribute__((swift_name("filter(x_:)")));
- (double)groupDelayOmega:(double)Omega __attribute__((swift_name("groupDelay(Omega:)")));
- (void)initialize __attribute__((swift_name("initialize()")));
- (QpsRational *)rationalRepresentation __attribute__((swift_name("rationalRepresentation()")));
@property QpsRational * _Nullable T __attribute__((swift_name("T")));
@property QpsKotlinDoubleArray *k __attribute__((swift_name("k")));
@property int32_t order __attribute__((swift_name("order")));
@property QpsKotlinDoubleArray *state __attribute__((swift_name("state")));
@end;

__attribute__((swift_name("AnalogPrototype")))
@interface QpsAnalogPrototype : QpsBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)addSectionR:(QpsRational *)R __attribute__((swift_name("addSection(R:)")));
- (void)computeTransferFunction __attribute__((swift_name("computeTransferFunction()")));
- (QpsComplex *)evaluateOmega:(double)omega __attribute__((swift_name("evaluate(omega:)")));
- (QpsRational *)getSectionIndex:(int32_t)index __attribute__((swift_name("getSection(index:)")));
- (double)groupDelayOmega:(double)omega __attribute__((swift_name("groupDelay(omega:)")));
- (QpsAnalogPrototype *)lptobpOmega1:(double)omega1 omega2:(double)omega2 __attribute__((swift_name("lptobp(omega1:omega2:)")));
- (QpsAnalogPrototype *)lptohpOmega0:(double)omega0 __attribute__((swift_name("lptohp(omega0:)")));
- (QpsAnalogPrototype *)lptolpOmega0:(double)omega0 __attribute__((swift_name("lptolp(omega0:)")));
- (int32_t)nSections __attribute__((swift_name("nSections()")));
@property QpsRational * _Nullable T __attribute__((swift_name("T")));
@property NSMutableArray<QpsRational *> *sections __attribute__((swift_name("sections")));
@property (readonly) QpsRational *transferFunction __attribute__((swift_name("transferFunction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AnalogButterworth")))
@interface QpsAnalogButterworth : QpsAnalogPrototype
- (instancetype)initWithOrder:(int32_t)order __attribute__((swift_name("init(order:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AnalogChebyshevI")))
@interface QpsAnalogChebyshevI : QpsAnalogPrototype
- (instancetype)initWithOrder:(int32_t)order epsilon:(double)epsilon __attribute__((swift_name("init(order:epsilon:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AnalogChebyshevII")))
@interface QpsAnalogChebyshevII : QpsAnalogPrototype
- (instancetype)initWithOrder:(int32_t)order epsilon:(double)epsilon __attribute__((swift_name("init(order:epsilon:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AnalogPrototype.Companion")))
@interface QpsAnalogPrototypeCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@end;

__attribute__((swift_name("IIRFilter")))
@interface QpsIIRFilter : QpsBase
- (instancetype)initWithBaseFilter:(QpsAnalogPrototype *)baseFilter type:(QpsPassbandType * _Nullable)type f1:(double)f1 f2:(double)f2 delta:(double)delta __attribute__((swift_name("init(baseFilter:type:f1:f2:delta:)"))) __attribute__((objc_designated_initializer));
- (QpsComplex *)evaluateOmega:(double)Omega __attribute__((swift_name("evaluate(Omega:)")));
- (float)filterX:(float)x __attribute__((swift_name("filter(x:)")));
- (void)filterX_:(QpsKotlinFloatArray *)x __attribute__((swift_name("filter(x_:)")));
- (void)filterX:(QpsKotlinFloatArray *)x y:(QpsKotlinFloatArray *)y __attribute__((swift_name("filter(x:y:)")));
- (double)groupDelayOmega:(double)Omega __attribute__((swift_name("groupDelay(Omega:)")));
- (void)initialize __attribute__((swift_name("initialize()")));
@property QpsRational *T __attribute__((swift_name("T")));
@property NSMutableArray<QpsSecondOrderSection *> *sections __attribute__((swift_name("sections")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Butterworth")))
@interface QpsButterworth : QpsIIRFilter
- (instancetype)initWithOrder:(int32_t)order type:(QpsPassbandType * _Nullable)type f1:(double)f1 f2:(double)f2 delta:(double)delta __attribute__((swift_name("init(order:type:f1:f2:delta:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithBaseFilter:(QpsAnalogPrototype *)baseFilter type:(QpsPassbandType * _Nullable)type f1:(double)f1 f2:(double)f2 delta:(double)delta __attribute__((swift_name("init(baseFilter:type:f1:f2:delta:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ChebyshevI")))
@interface QpsChebyshevI : QpsIIRFilter
- (instancetype)initWithOrder:(int32_t)order epsilon:(double)epsilon type:(QpsPassbandType * _Nullable)type f1:(double)f1 f2:(double)f2 delta:(double)delta __attribute__((swift_name("init(order:epsilon:type:f1:f2:delta:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithBaseFilter:(QpsAnalogPrototype *)baseFilter type:(QpsPassbandType * _Nullable)type f1:(double)f1 f2:(double)f2 delta:(double)delta __attribute__((swift_name("init(baseFilter:type:f1:f2:delta:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ChebyshevII")))
@interface QpsChebyshevII : QpsIIRFilter
- (instancetype)initWithOrder:(int32_t)order epsilon:(double)epsilon type:(QpsPassbandType * _Nullable)type f1:(double)f1 f2:(double)f2 delta:(double)delta __attribute__((swift_name("init(order:epsilon:type:f1:f2:delta:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithBaseFilter:(QpsAnalogPrototype *)baseFilter type:(QpsPassbandType * _Nullable)type f1:(double)f1 f2:(double)f2 delta:(double)delta __attribute__((swift_name("init(baseFilter:type:f1:f2:delta:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Complex")))
@interface QpsComplex : QpsBase
- (instancetype)initWithReal:(double)real imag:(double)imag __attribute__((swift_name("init(real:imag:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithReal:(double)real __attribute__((swift_name("init(real:)"))) __attribute__((objc_designated_initializer));
- (void)divideEqualsA:(double)a __attribute__((swift_name("divideEquals(a:)")));
- (void)divideEqualsC:(QpsComplex *)c __attribute__((swift_name("divideEquals(c:)")));
- (double)imag __attribute__((swift_name("imag()")));
- (QpsComplex *)minusA:(double)a __attribute__((swift_name("minus(a:)")));
- (QpsComplex *)minusC:(QpsComplex *)c __attribute__((swift_name("minus(c:)")));
- (void)minusEqualsA:(double)a __attribute__((swift_name("minusEquals(a:)")));
- (void)minusEqualsC:(QpsComplex *)c __attribute__((swift_name("minusEquals(c:)")));
- (QpsComplex *)overA:(double)a __attribute__((swift_name("over(a:)")));
- (QpsComplex *)overC:(QpsComplex *)c __attribute__((swift_name("over(c:)")));
- (QpsComplex *)plusA:(double)a __attribute__((swift_name("plus(a:)")));
- (QpsComplex *)plusC:(QpsComplex *)c __attribute__((swift_name("plus(c:)")));
- (void)plusEqualsA:(double)a __attribute__((swift_name("plusEquals(a:)")));
- (void)plusEqualsC:(QpsComplex *)c __attribute__((swift_name("plusEquals(c:)")));
- (double)real __attribute__((swift_name("real()")));
- (QpsComplex *)timesA:(double)a __attribute__((swift_name("times(a:)")));
- (QpsComplex *)timesC:(QpsComplex *)c __attribute__((swift_name("times(c:)")));
- (void)timesEqualsA:(double)a __attribute__((swift_name("timesEquals(a:)")));
- (void)timesEqualsC:(QpsComplex *)c __attribute__((swift_name("timesEquals(c:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Complex.Companion")))
@interface QpsComplexCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (QpsComplex *)ComplexFromPolarR:(double)r phi:(double)phi __attribute__((swift_name("ComplexFromPolar(r:phi:)")));
- (QpsComplex *)addA:(double)a c:(QpsComplex *)c __attribute__((swift_name("add(a:c:)")));
- (QpsComplex *)addC:(QpsComplex *)c a:(double)a __attribute__((swift_name("add(c:a:)")));
- (QpsComplex *)addC1:(QpsComplex *)c1 c2:(QpsComplex *)c2 __attribute__((swift_name("add(c1:c2:)")));
- (double)cAbsC:(QpsComplex *)c __attribute__((swift_name("cAbs(c:)")));
- (double)cAngleC:(QpsComplex *)c __attribute__((swift_name("cAngle(c:)")));
- (QpsComplex *)cConjugateC:(QpsComplex *)c __attribute__((swift_name("cConjugate(c:)")));
- (QpsComplex *)cExpC:(QpsComplex *)c __attribute__((swift_name("cExp(c:)")));
- (QpsComplex *)cSqrtC:(QpsComplex *)c __attribute__((swift_name("cSqrt(c:)")));
- (QpsComplex *)divideA:(double)a c:(QpsComplex *)c __attribute__((swift_name("divide(a:c:)")));
- (QpsComplex *)divideC:(QpsComplex *)c a:(double)a __attribute__((swift_name("divide(c:a:)")));
- (QpsComplex *)divideC1:(QpsComplex *)c1 c2:(QpsComplex *)c2 __attribute__((swift_name("divide(c1:c2:)")));
- (QpsComplex *)multiplyA:(double)a c:(QpsComplex *)c __attribute__((swift_name("multiply(a:c:)")));
- (QpsComplex *)multiplyC:(QpsComplex *)c a:(double)a __attribute__((swift_name("multiply(c:a:)")));
- (QpsComplex *)multiplyC1:(QpsComplex *)c1 c2:(QpsComplex *)c2 __attribute__((swift_name("multiply(c1:c2:)")));
- (QpsComplex *)subtractA:(double)a c:(QpsComplex *)c __attribute__((swift_name("subtract(a:c:)")));
- (QpsComplex *)subtractC:(QpsComplex *)c a:(double)a __attribute__((swift_name("subtract(c:a:)")));
- (QpsComplex *)subtractC1:(QpsComplex *)c1 c2:(QpsComplex *)c2 __attribute__((swift_name("subtract(c1:c2:)")));
- (QpsComplex *)unaryMinusC:(QpsComplex *)c __attribute__((swift_name("unaryMinus(c:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataFilterAndroid")))
@interface QpsDataFilterAndroid : QpsBase
- (instancetype)initWithCutoff:(double)cutoff fs:(double)fs order:(int32_t)order medKernel:(int32_t)medKernel __attribute__((swift_name("init(cutoff:fs:order:medKernel:)"))) __attribute__((objc_designated_initializer));
- (float)filterX:(float)x __attribute__((swift_name("filter(x:)")));
@property (readonly) double cutoff __attribute__((swift_name("cutoff")));
@property (readonly) double fs __attribute__((swift_name("fs")));
@property (readonly) QpsButterworth *lpFilter __attribute__((swift_name("lpFilter")));
@property (readonly) QpsMedFilter *medFilter __attribute__((swift_name("medFilter")));
@property (readonly) int32_t medKernel __attribute__((swift_name("medKernel")));
@property (readonly) int32_t order __attribute__((swift_name("order")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LagrangePolynomial")))
@interface QpsLagrangePolynomial : QpsBase
- (instancetype)initWithX:(QpsKotlinDoubleArray *)x y:(QpsKotlinDoubleArray *)y __attribute__((swift_name("init(x:y:)"))) __attribute__((objc_designated_initializer));
- (QpsKotlinDoubleArray *)ChebyshevNodesA:(double)a b:(double)b n:(int32_t)n __attribute__((swift_name("ChebyshevNodes(a:b:n:)")));
- (double)evaluateXp:(double)xp __attribute__((swift_name("evaluate(xp:)")));
- (int32_t)order __attribute__((swift_name("order()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LagrangePolynomial.Companion")))
@interface QpsLagrangePolynomialCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (QpsKotlinDoubleArray *)BarycentricWeightsZ:(QpsKotlinDoubleArray *)z __attribute__((swift_name("BarycentricWeights(z:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PassbandType")))
@interface QpsPassbandType : QpsKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) QpsPassbandType *lowpass __attribute__((swift_name("lowpass")));
@property (class, readonly) QpsPassbandType *bandpass __attribute__((swift_name("bandpass")));
@property (class, readonly) QpsPassbandType *highpass __attribute__((swift_name("highpass")));
- (int32_t)compareToOther:(QpsPassbandType *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Polynomial")))
@interface QpsPolynomial : QpsBase
- (instancetype)initWithA:(QpsKotlinDoubleArray *)a __attribute__((swift_name("init(a:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithB:(QpsPolynomial *)B __attribute__((swift_name("init(B:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithOrder:(int32_t)order __attribute__((swift_name("init(order:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithC:(double)c __attribute__((swift_name("init(c:)"))) __attribute__((objc_designated_initializer));
- (QpsKotlinDoubleArray *)coefficients __attribute__((swift_name("coefficients()")));
- (QpsPolynomial *)derivative __attribute__((swift_name("derivative()")));
- (double)discreteTimeGroupDelayOmega:(double)Omega __attribute__((swift_name("discreteTimeGroupDelay(Omega:)")));
- (double)evaluateX:(double)x __attribute__((swift_name("evaluate(x:)")));
- (QpsComplex *)evaluateC:(QpsComplex * _Nullable)c __attribute__((swift_name("evaluate(c:)")));
- (double)groupDelayOmega:(double)omega __attribute__((swift_name("groupDelay(omega:)")));
- (QpsPolynomial *)minusC:(double)c __attribute__((swift_name("minus(c:)")));
- (QpsPolynomial *)minusB:(QpsPolynomial *)B __attribute__((swift_name("minus(B:)")));
- (void)minusEqualsC:(double)c __attribute__((swift_name("minusEquals(c:)")));
- (void)minusEqualsB:(QpsPolynomial *)B __attribute__((swift_name("minusEquals(B:)")));
- (int32_t)order __attribute__((swift_name("order()")));
- (QpsPolynomial *)overC:(double)c __attribute__((swift_name("over(c:)")));
- (QpsRational *)overB:(QpsPolynomial *)B __attribute__((swift_name("over(B:)")));
- (void)overEqualsC:(double)c __attribute__((swift_name("overEquals(c:)")));
- (QpsPolynomial *)plusC:(double)c __attribute__((swift_name("plus(c:)")));
- (QpsPolynomial *)plusB:(QpsPolynomial *)B __attribute__((swift_name("plus(B:)")));
- (void)plusEqualsC:(double)c __attribute__((swift_name("plusEquals(c:)")));
- (void)plusEqualsB:(QpsPolynomial *)B __attribute__((swift_name("plusEquals(B:)")));
- (QpsKotlinDoubleArray *)reflectionCoefficients __attribute__((swift_name("reflectionCoefficients()")));
- (QpsPolynomial *)timesC:(double)c __attribute__((swift_name("times(c:)")));
- (QpsPolynomial *)timesB:(QpsPolynomial *)B __attribute__((swift_name("times(B:)")));
- (void)timesEqualsC:(double)c __attribute__((swift_name("timesEquals(c:)")));
- (void)timesEqualsB:(QpsPolynomial *)B __attribute__((swift_name("timesEquals(B:)")));
- (void)trim __attribute__((swift_name("trim()")));
@property QpsKotlinDoubleArray *a __attribute__((swift_name("a")));
@property (getter=order_) int32_t order __attribute__((swift_name("order")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Rational")))
@interface QpsRational : QpsBase
- (instancetype)initWithNum:(QpsKotlinDoubleArray *)num denom:(QpsKotlinDoubleArray *)denom __attribute__((swift_name("init(num:denom:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithN:(QpsPolynomial *)N D:(QpsPolynomial *)D __attribute__((swift_name("init(N:D:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithR:(QpsRational *)R __attribute__((swift_name("init(R:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithC:(double)c __attribute__((swift_name("init(c:)"))) __attribute__((objc_designated_initializer));
- (double)canonicalForm __attribute__((swift_name("canonicalForm()")));
- (QpsPolynomial *)denominator __attribute__((swift_name("denominator()")));
- (double)discreteTimeGroupDelayOmega:(double)Omega __attribute__((swift_name("discreteTimeGroupDelay(Omega:)")));
- (double)evaluateX:(double)x __attribute__((swift_name("evaluate(x:)")));
- (QpsComplex *)evaluateC:(QpsComplex * _Nullable)c __attribute__((swift_name("evaluate(c:)")));
- (double)groupDelayOmega:(double)omega __attribute__((swift_name("groupDelay(omega:)")));
- (QpsRational *)mapS:(QpsRational *)S __attribute__((swift_name("map(S:)")));
- (QpsPolynomial *)numerator __attribute__((swift_name("numerator()")));
- (QpsKotlinIntArray *)order __attribute__((swift_name("order()")));
- (double)residuePole:(double)pole __attribute__((swift_name("residue(pole:)")));
- (QpsComplex *)residuePole_:(QpsComplex * _Nullable)pole __attribute__((swift_name("residue(pole_:)")));
- (void)timesEqualsA:(double)A __attribute__((swift_name("timesEquals(A:)")));
- (void)timesEqualsP:(QpsPolynomial *)P __attribute__((swift_name("timesEquals(P:)")));
- (void)timesEqualsR:(QpsRational *)R __attribute__((swift_name("timesEquals(R:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SecondOrderSection")))
@interface QpsSecondOrderSection : QpsBase
- (instancetype)initWithB0:(double)b0 b1:(double)b1 b2:(double)b2 a1:(double)a1 a2:(double)a2 __attribute__((swift_name("init(b0:b1:b2:a1:a2:)"))) __attribute__((objc_designated_initializer));
- (float)filterX:(float)x __attribute__((swift_name("filter(x:)")));
- (void)filterX:(QpsKotlinFloatArray *)x y:(QpsKotlinFloatArray *)y __attribute__((swift_name("filter(x:y:)")));
- (void)initialize __attribute__((swift_name("initialize()")));
@property double a1 __attribute__((swift_name("a1")));
@property double a2 __attribute__((swift_name("a2")));
@property double b0 __attribute__((swift_name("b0")));
@property double b1 __attribute__((swift_name("b1")));
@property double b2 __attribute__((swift_name("b2")));
@property double s1 __attribute__((swift_name("s1")));
@property double s2 __attribute__((swift_name("s2")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ThiranAllpass")))
@interface QpsThiranAllpass : QpsAllpass
- (instancetype)initWithN:(int32_t)N D:(double)D __attribute__((swift_name("init(N:D:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithOrder:(int32_t)order __attribute__((swift_name("init(order:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithA:(QpsPolynomial *)A __attribute__((swift_name("init(A:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithK:(QpsKotlinDoubleArray *)k __attribute__((swift_name("init(k:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MapCoordinate")))
@interface QpsMapCoordinate : QpsBase
- (instancetype)initWithValidPosition:(BOOL)validPosition offset:(double)offset __attribute__((swift_name("init(validPosition:offset:)"))) __attribute__((objc_designated_initializer));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (double)component2 __attribute__((swift_name("component2()")));
- (QpsMapCoordinate *)doCopyValidPosition:(BOOL)validPosition offset:(double)offset __attribute__((swift_name("doCopy(validPosition:offset:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) double offset __attribute__((swift_name("offset")));
@property (readonly) BOOL validPosition __attribute__((swift_name("validPosition")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MapData")))
@interface QpsMapData : QpsBase
- (instancetype)initWithMapInformation:(id<QpsIQPSMapInformation>)mapInformation debug:(BOOL)debug __attribute__((swift_name("init(mapInformation:debug:)"))) __attribute__((objc_designated_initializer));
- (QpsMapCoordinate *)getPoint:(QpsPointF *)point __attribute__((swift_name("get(point:)")));
- (BOOL)isValidPointPoint:(QpsPointF *)point __attribute__((swift_name("isValidPoint(point:)")));
- (double)offsetZoneRadiansPoint:(QpsPointF *)point __attribute__((swift_name("offsetZoneRadians(point:)")));
@property void * _Nullable mapFenceData __attribute__((swift_name("mapFenceData")));
@property void * _Nullable offsetZoneData __attribute__((swift_name("offsetZoneData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DebugCluster")))
@interface QpsDebugCluster : QpsBase
- (instancetype)initWithPositions:(NSMutableArray<QpsTimedDebugPositions *> *)positions clusterCenter:(NSMutableArray<QpsTimedDebugPositions *> *)clusterCenter precisionRadius:(NSMutableArray<QpsTimedDebugRadius *> *)precisionRadius id:(int32_t)id __attribute__((swift_name("init(positions:clusterCenter:precisionRadius:id:)"))) __attribute__((objc_designated_initializer));
- (NSMutableArray<QpsTimedDebugPositions *> *)component1 __attribute__((swift_name("component1()")));
- (NSMutableArray<QpsTimedDebugPositions *> *)component2 __attribute__((swift_name("component2()")));
- (NSMutableArray<QpsTimedDebugRadius *> *)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (QpsDebugCluster *)doCopyPositions:(NSMutableArray<QpsTimedDebugPositions *> *)positions clusterCenter:(NSMutableArray<QpsTimedDebugPositions *> *)clusterCenter precisionRadius:(NSMutableArray<QpsTimedDebugRadius *> *)precisionRadius id:(int32_t)id __attribute__((swift_name("doCopy(positions:clusterCenter:precisionRadius:id:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSMutableArray<QpsTimedDebugPositions *> *clusterCenter __attribute__((swift_name("clusterCenter")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) NSMutableArray<QpsTimedDebugPositions *> *positions __attribute__((swift_name("positions")));
@property (readonly) NSMutableArray<QpsTimedDebugRadius *> *precisionRadius __attribute__((swift_name("precisionRadius")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DebugCluster.Companion")))
@interface QpsDebugClusterCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DebugClusterAngle")))
@interface QpsDebugClusterAngle : QpsBase
- (instancetype)initWithCenter:(QpsPointF * _Nullable)center angle:(double)angle offset:(double)offset __attribute__((swift_name("init(center:angle:offset:)"))) __attribute__((objc_designated_initializer));
- (QpsPointF * _Nullable)component1 __attribute__((swift_name("component1()")));
- (double)component2 __attribute__((swift_name("component2()")));
- (double)component3 __attribute__((swift_name("component3()")));
- (QpsDebugClusterAngle *)doCopyCenter:(QpsPointF * _Nullable)center angle:(double)angle offset:(double)offset __attribute__((swift_name("doCopy(center:angle:offset:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) double angle __attribute__((swift_name("angle")));
@property (readonly) QpsPointF * _Nullable center __attribute__((swift_name("center")));
@property (readonly) double offset __attribute__((swift_name("offset")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DebugClusterAngle.Companion")))
@interface QpsDebugClusterAngleCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DebugPosition")))
@interface QpsDebugPosition : QpsBase
- (instancetype)initWithPosition:(QpsPointF *)position age:(int32_t)age speed:(double)speed precisionRadius:(QpsDouble * _Nullable)precisionRadius __attribute__((swift_name("init(position:age:speed:precisionRadius:)"))) __attribute__((objc_designated_initializer));
- (QpsPointF *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (double)component3 __attribute__((swift_name("component3()")));
- (QpsDouble * _Nullable)component4 __attribute__((swift_name("component4()")));
- (QpsDebugPosition *)doCopyPosition:(QpsPointF *)position age:(int32_t)age speed:(double)speed precisionRadius:(QpsDouble * _Nullable)precisionRadius __attribute__((swift_name("doCopy(position:age:speed:precisionRadius:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t age __attribute__((swift_name("age")));
@property (readonly) QpsPointF *position __attribute__((swift_name("position")));
@property (readonly) QpsDouble * _Nullable precisionRadius __attribute__((swift_name("precisionRadius")));
@property (readonly) double speed __attribute__((swift_name("speed")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DebugPosition.Companion")))
@interface QpsDebugPositionCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DebugPrinter")))
@interface QpsDebugPrinter : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)debugPrinter __attribute__((swift_name("init()")));
- (void)printDebugTAG:(NSString *)TAG output:(float)output shouldPrint:(BOOL)shouldPrint __attribute__((swift_name("printDebug(TAG:output:shouldPrint:)")));
- (void)printDebugTAG:(NSString *)TAG output:(NSString *)output shouldPrint_:(BOOL)shouldPrint __attribute__((swift_name("printDebug(TAG:output:shouldPrint_:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DebugState")))
@interface QpsDebugState : QpsBase
- (instancetype)initWithState:(NSString *)state __attribute__((swift_name("init(state:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (QpsDebugState *)doCopyState:(NSString *)state __attribute__((swift_name("doCopy(state:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *state __attribute__((swift_name("state")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DebugState.Companion")))
@interface QpsDebugStateCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedDebugClusterAngles")))
@interface QpsTimedDebugClusterAngles : QpsBase
- (instancetype)initWithPositions:(NSArray<QpsDebugClusterAngle *> *)positions timestamp:(int64_t)timestamp __attribute__((swift_name("init(positions:timestamp:)"))) __attribute__((objc_designated_initializer));
- (NSArray<QpsDebugClusterAngle *> *)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (QpsTimedDebugClusterAngles *)doCopyPositions:(NSArray<QpsDebugClusterAngle *> *)positions timestamp:(int64_t)timestamp __attribute__((swift_name("doCopy(positions:timestamp:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<QpsDebugClusterAngle *> *positions __attribute__((swift_name("positions")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedDebugClusterAngles.Companion")))
@interface QpsTimedDebugClusterAnglesCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedDebugClusters")))
@interface QpsTimedDebugClusters : QpsBase
- (instancetype)initWithClusters:(NSMutableArray<QpsDebugCluster *> *)clusters __attribute__((swift_name("init(clusters:)"))) __attribute__((objc_designated_initializer));
- (NSMutableArray<QpsDebugCluster *> *)component1 __attribute__((swift_name("component1()")));
- (QpsTimedDebugClusters *)doCopyClusters:(NSMutableArray<QpsDebugCluster *> *)clusters __attribute__((swift_name("doCopy(clusters:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSMutableArray<QpsDebugCluster *> *clusters __attribute__((swift_name("clusters")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedDebugClusters.Companion")))
@interface QpsTimedDebugClustersCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedDebugMessages")))
@interface QpsTimedDebugMessages : QpsBase
- (instancetype)initWithMessages:(NSArray<NSString *> *)messages timestamp:(int64_t)timestamp __attribute__((swift_name("init(messages:timestamp:)"))) __attribute__((objc_designated_initializer));
- (NSArray<NSString *> *)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (QpsTimedDebugMessages *)doCopyMessages:(NSArray<NSString *> *)messages timestamp:(int64_t)timestamp __attribute__((swift_name("doCopy(messages:timestamp:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<NSString *> *messages __attribute__((swift_name("messages")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedDebugMessages.Companion")))
@interface QpsTimedDebugMessagesCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedDebugPosition")))
@interface QpsTimedDebugPosition : QpsBase
- (instancetype)initWithPosition:(QpsPointF * _Nullable)position timestamp:(int64_t)timestamp __attribute__((swift_name("init(position:timestamp:)"))) __attribute__((objc_designated_initializer));
- (QpsPointF * _Nullable)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (QpsTimedDebugPosition *)doCopyPosition:(QpsPointF * _Nullable)position timestamp:(int64_t)timestamp __attribute__((swift_name("doCopy(position:timestamp:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) QpsPointF * _Nullable position __attribute__((swift_name("position")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedDebugPosition.Companion")))
@interface QpsTimedDebugPositionCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedDebugPositions")))
@interface QpsTimedDebugPositions : QpsBase
- (instancetype)initWithPositions:(NSArray<QpsDebugPosition *> *)positions timestamp:(int64_t)timestamp __attribute__((swift_name("init(positions:timestamp:)"))) __attribute__((objc_designated_initializer));
- (NSArray<QpsDebugPosition *> *)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (QpsTimedDebugPositions *)doCopyPositions:(NSArray<QpsDebugPosition *> *)positions timestamp:(int64_t)timestamp __attribute__((swift_name("doCopy(positions:timestamp:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<QpsDebugPosition *> *positions __attribute__((swift_name("positions")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedDebugPositions.Companion")))
@interface QpsTimedDebugPositionsCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedDebugRadius")))
@interface QpsTimedDebugRadius : QpsBase
- (instancetype)initWithPositions:(double)positions timestamp:(int64_t)timestamp __attribute__((swift_name("init(positions:timestamp:)"))) __attribute__((objc_designated_initializer));
- (double)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (QpsTimedDebugRadius *)doCopyPositions:(double)positions timestamp:(int64_t)timestamp __attribute__((swift_name("doCopy(positions:timestamp:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) double positions __attribute__((swift_name("positions")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedDebugRadius.Companion")))
@interface QpsTimedDebugRadiusCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedDebugStates")))
@interface QpsTimedDebugStates : QpsBase
- (instancetype)initWithStates:(NSArray<QpsDebugState *> *)states timestamp:(int64_t)timestamp __attribute__((swift_name("init(states:timestamp:)"))) __attribute__((objc_designated_initializer));
- (NSArray<QpsDebugState *> *)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (QpsTimedDebugStates *)doCopyStates:(NSArray<QpsDebugState *> *)states timestamp:(int64_t)timestamp __attribute__((swift_name("doCopy(states:timestamp:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<QpsDebugState *> *states __attribute__((swift_name("states")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedDebugStates.Companion")))
@interface QpsTimedDebugStatesCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("Observable")))
@interface QpsObservable : QpsBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)addObserverObserver:(id<QpsObserver> _Nullable)observer __attribute__((swift_name("addObserver(observer:)")));
- (void)clearChanged __attribute__((swift_name("clearChanged()")));
- (void)deleteObserverObserver:(id<QpsObserver>)observer __attribute__((swift_name("deleteObserver(observer:)")));
- (void)deleteObservers __attribute__((swift_name("deleteObservers()")));
- (BOOL)hasChanged __attribute__((swift_name("hasChanged()")));
- (void)notifyObserversObj:(id)obj __attribute__((swift_name("notifyObservers(obj:)")));
- (void)setChanged __attribute__((swift_name("setChanged()")));
@end;

__attribute__((swift_name("RawSensor")))
@interface QpsRawSensor : QpsObservable
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSString *)getVendorName __attribute__((swift_name("getVendorName()")));
- (void)notifyChangedValues:(QpsKotlinFloatArray *)values __attribute__((swift_name("notifyChanged(values:)")));
- (void)start __attribute__((swift_name("start()")));
- (void)stop __attribute__((swift_name("stop()")));
@property (readonly) int64_t currentTime __attribute__((swift_name("currentTime")));
@property (readonly) QpsRawSensorDataSensorDataType *sensorDataType __attribute__((swift_name("sensorDataType")));
@end;

__attribute__((swift_name("ReplaySensor")))
@interface QpsReplaySensor : QpsRawSensor <QpsObserver>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)start __attribute__((swift_name("start()")));
- (void)stop __attribute__((swift_name("stop()")));
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
@property int64_t currentTime __attribute__((swift_name("currentTime")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayAcceleration")))
@interface QpsReplayAcceleration : QpsReplaySensor
- (instancetype)initWithDataCoordinator:(QpsReplayDataCoordinator *)dataCoordinator vendor:(NSString * _Nullable)vendor __attribute__((swift_name("init(dataCoordinator:vendor:)"))) __attribute__((objc_designated_initializer));
- (NSString *)getVendorName __attribute__((swift_name("getVendorName()")));
@property (readonly) QpsRawSensorDataSensorDataType *sensorDataType __attribute__((swift_name("sensorDataType")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayDataCoordinator")))
@interface QpsReplayDataCoordinator : QpsObservable
- (instancetype)initWithDataHandler:(id<QpsIReplayDataHandler>)dataHandler usesReplayDataClass:(BOOL)usesReplayDataClass onComplete:(void (^)(void))onComplete __attribute__((swift_name("init(dataHandler:usesReplayDataClass:onComplete:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)start __attribute__((swift_name("start()")));
- (void)stop __attribute__((swift_name("stop()")));
@property int64_t currentTime __attribute__((swift_name("currentTime")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayGameRotation")))
@interface QpsReplayGameRotation : QpsReplaySensor
- (instancetype)initWithDataCoordinator:(QpsReplayDataCoordinator *)dataCoordinator vendor:(NSString * _Nullable)vendor __attribute__((swift_name("init(dataCoordinator:vendor:)"))) __attribute__((objc_designated_initializer));
- (NSString *)getVendorName __attribute__((swift_name("getVendorName()")));
@property (readonly) QpsRawSensorDataSensorDataType *sensorDataType __attribute__((swift_name("sensorDataType")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayGeoMagneticSensor")))
@interface QpsReplayGeoMagneticSensor : QpsReplaySensor
- (instancetype)initWithDataCoordinator:(QpsReplayDataCoordinator *)dataCoordinator vendor:(NSString * _Nullable)vendor __attribute__((swift_name("init(dataCoordinator:vendor:)"))) __attribute__((objc_designated_initializer));
- (NSString *)getVendorName __attribute__((swift_name("getVendorName()")));
@property (readonly) QpsRawSensorDataSensorDataType *sensorDataType __attribute__((swift_name("sensorDataType")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayGravity")))
@interface QpsReplayGravity : QpsReplaySensor
- (instancetype)initWithDataCoordinator:(QpsReplayDataCoordinator *)dataCoordinator vendor:(NSString * _Nullable)vendor __attribute__((swift_name("init(dataCoordinator:vendor:)"))) __attribute__((objc_designated_initializer));
- (NSString *)getVendorName __attribute__((swift_name("getVendorName()")));
@property (readonly) QpsRawSensorDataSensorDataType *sensorDataType __attribute__((swift_name("sensorDataType")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplaySensor.Companion")))
@interface QpsReplaySensorCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CompassRotationSensor")))
@interface QpsCompassRotationSensor : QpsObservable <QpsObserver>
- (instancetype)initWithGeoMagneticRotation:(QpsRawSensor *)geoMagneticRotation observer:(id<QpsObserver> _Nullable)observer __attribute__((swift_name("init(geoMagneticRotation:observer:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)stop __attribute__((swift_name("stop()")));
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
@property id<QpsObserver> _Nullable observer __attribute__((swift_name("observer")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RotationSensor")))
@interface QpsRotationSensor : QpsObservable <QpsObserver>
- (instancetype)initWithRotation:(QpsRawSensor *)rotation gravity:(QpsRawSensor *)gravity observer:(id<QpsObserver> _Nullable)observer __attribute__((swift_name("init(rotation:gravity:observer:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)stop __attribute__((swift_name("stop()")));
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RotationSensor.Companion")))
@interface QpsRotationSensorCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SpeedSensor")))
@interface QpsSpeedSensor : QpsObservable <QpsIStepListener>
- (instancetype)initWithAccelerationSensor:(QpsRawSensor *)accelerationSensor gravitySensor:(QpsRawSensor *)gravitySensor rotationSensor:(QpsRawSensor *)rotationSensor speedBundleObserver:(id<QpsObserver> _Nullable)speedBundleObserver userSettings:(id<QpsIQPSUserSettings>)userSettings onIllegalBehaviour:(void (^)(void))onIllegalBehaviour onSensorsInitiated:(void (^)(void))onSensorsInitiated __attribute__((swift_name("init(accelerationSensor:gravitySensor:rotationSensor:speedBundleObserver:userSettings:onIllegalBehaviour:onSensorsInitiated:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)onSensorsInitiated __attribute__((swift_name("onSensorsInitiated()")));
- (void)onStepDetectedStep:(QpsStepData *)step __attribute__((swift_name("onStepDetected(step:)")));
- (void)stop __attribute__((swift_name("stop()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SpeedSensor.Companion")))
@interface QpsSpeedSensorCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@end;

__attribute__((swift_name("IReplayDataHandler")))
@protocol QpsIReplayDataHandler
@required
- (void)addSensorDataData:(QpsRawSensorData *)data __attribute__((swift_name("addSensorData(data:)")));
- (void)addTickDataNavBundle:(QpsQPSNavBundle *)navBundle currentTime:(int64_t)currentTime azimuth:(double)azimuth magnitude:(double)magnitude qPositions:(NSArray<QpsQPosition *> *)qPositions __attribute__((swift_name("addTickData(navBundle:currentTime:azimuth:magnitude:qPositions:)")));
- (NSArray<QpsRawSensorData *> *)getAllSensorDataDataTypes:(NSArray<QpsRawSensorDataSensorDataType *> *)dataTypes __attribute__((swift_name("getAllSensorData(dataTypes:)")));
- (NSArray<QpsRawSensorData *> *)getAllSensorDataNewDataTypes:(NSArray<QpsRawSensorDataSensorDataType *> *)dataTypes __attribute__((swift_name("getAllSensorDataNew(dataTypes:)")));
- (NSArray<QpsRawSensorData *> *)getSensorDataDataType:(QpsRawSensorDataSensorDataType *)dataType __attribute__((swift_name("getSensorData(dataType:)")));
- (void)startRecording __attribute__((swift_name("startRecording()")));
- (void)stopRecording __attribute__((swift_name("stopRecording()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IReplayDataHandlerCompanion")))
@interface QpsIReplayDataHandlerCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IReplayDataHandlerIReplayConfig")))
@interface QpsIReplayDataHandlerIReplayConfig : QpsBase
- (instancetype)initWithTestNumber:(NSString * _Nullable)testNumber printMissingFileException:(BOOL)printMissingFileException __attribute__((swift_name("init(testNumber:printMissingFileException:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (QpsIReplayDataHandlerIReplayConfig *)doCopyTestNumber:(NSString * _Nullable)testNumber printMissingFileException:(BOOL)printMissingFileException __attribute__((swift_name("doCopy(testNumber:printMissingFileException:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property BOOL printMissingFileException __attribute__((swift_name("printMissingFileException")));
@property (readonly) NSString * _Nullable testNumber __attribute__((swift_name("testNumber")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayDataHandler")))
@interface QpsReplayDataHandler : QpsBase <QpsIReplayDataHandler>
- (instancetype)initWithInteractor:(id<QpsIQPSReplayInteractor>)interactor config:(QpsIReplayDataHandlerIReplayConfig *)config systemType:(QpsIQPSSystemType *)systemType userSettings:(id<QpsIQPSUserSettings> _Nullable)userSettings qpsValues:(QpsQPSValues *)qpsValues mapInformation:(id<QpsIQPSMapInformation> _Nullable)mapInformation versionOfVPS:(NSString *)versionOfVPS __attribute__((swift_name("init(interactor:config:systemType:userSettings:qpsValues:mapInformation:versionOfVPS:)"))) __attribute__((objc_designated_initializer));
- (void)addSensorDataData:(QpsRawSensorData *)data __attribute__((swift_name("addSensorData(data:)")));
- (void)addTickDataNavBundle:(QpsQPSNavBundle *)navBundle currentTime:(int64_t)currentTime azimuth:(double)azimuth magnitude:(double)magnitude qPositions:(NSArray<QpsQPosition *> *)qPositions __attribute__((swift_name("addTickData(navBundle:currentTime:azimuth:magnitude:qPositions:)")));
- (NSArray<QpsRawSensorData *> *)getAllSensorDataDataTypes:(NSArray<QpsRawSensorDataSensorDataType *> *)dataTypes __attribute__((swift_name("getAllSensorData(dataTypes:)")));
- (NSArray<QpsRawSensorData *> *)getAllSensorDataNewDataTypes:(NSArray<QpsRawSensorDataSensorDataType *> *)dataTypes __attribute__((swift_name("getAllSensorDataNew(dataTypes:)")));
- (NSArray<QpsRawSensorData *> *)getSensorDataDataType:(QpsRawSensorDataSensorDataType *)dataType __attribute__((swift_name("getSensorData(dataType:)")));
- (void)startRecording __attribute__((swift_name("startRecording()")));
- (void)stopRecording __attribute__((swift_name("stopRecording()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SensorRecorder")))
@interface QpsSensorRecorder : QpsBase <QpsObserver>
- (instancetype)initWithSensors:(NSSet<id> *)sensors recordDataHelper:(id<QpsIReplayDataHandler>)recordDataHelper __attribute__((swift_name("init(sensors:recordDataHelper:)"))) __attribute__((objc_designated_initializer));
- (void)updateObservable:(QpsObservable *)observable arg:(id)arg __attribute__((swift_name("update(observable:arg:)")));
@end;

__attribute__((swift_name("MoveVector")))
@interface QpsMoveVector : QpsBase
- (instancetype)initWithAzimuth:(float)azimuth magnitude:(double)magnitude confidence:(double)confidence __attribute__((swift_name("init(azimuth:magnitude:confidence:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithX:(double)x y:(double)y confidence:(double)confidence __attribute__((swift_name("init(x:y:confidence:)"))) __attribute__((objc_designated_initializer));
- (void)addOffsetOffset:(float)offset __attribute__((swift_name("addOffset(offset:)")));
- (QpsMoveVector *)doCopy __attribute__((swift_name("doCopy()")));
- (QpsMoveVector *)divDivisor:(double)divisor __attribute__((swift_name("div(divisor:)")));
- (double)dotVector:(QpsMoveVector *)vector __attribute__((swift_name("dot(vector:)")));
- (BOOL)equalsVector:(QpsMoveVector *)vector __attribute__((swift_name("equals(vector:)")));
- (QpsMoveVector *)getClosestDirectionPossibleDirections:(NSMutableArray<QpsMoveVector *> *)possibleDirections __attribute__((swift_name("getClosestDirection(possibleDirections:)")));
- (QpsMoveVector *)gramSchmidtVector:(QpsMoveVector *)vector __attribute__((swift_name("gramSchmidt(vector:)")));
- (BOOL)isNonZero __attribute__((swift_name("isNonZero()")));
- (BOOL)isZero __attribute__((swift_name("isZero()")));
- (QpsMoveVector *)minusVector:(QpsMoveVector *)vector __attribute__((swift_name("minus(vector:)")));
- (QpsMoveVector *)normed __attribute__((swift_name("normed()")));
- (BOOL)parallelVector:(QpsMoveVector *)vector __attribute__((swift_name("parallel(vector:)")));
- (QpsMoveVector *)plusVector:(QpsMoveVector *)vector __attribute__((swift_name("plus(vector:)")));
- (QpsMoveVector *)rotateAngle:(double)angle degrees:(BOOL)degrees __attribute__((swift_name("rotate(angle:degrees:)")));
- (QpsMoveVector *)timesFactor:(double)factor __attribute__((swift_name("times(factor:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property float azimuth __attribute__((swift_name("azimuth")));
@property (readonly) double confidence __attribute__((swift_name("confidence")));
@property double magnitude __attribute__((swift_name("magnitude")));
@property (readonly) double x __attribute__((swift_name("x")));
@property (readonly) double y __attribute__((swift_name("y")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MovementStateTick")))
@interface QpsMovementStateTick : QpsBase
- (instancetype)initWithVector:(QpsMoveVector *)vector currentTime:(int64_t)currentTime duration:(int64_t)duration clusterPositions:(NSMutableArray<QpsQPosition *> *)clusterPositions __attribute__((swift_name("init(vector:currentTime:duration:clusterPositions:)"))) __attribute__((objc_designated_initializer));
- (QpsMoveVector *)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (int64_t)component3 __attribute__((swift_name("component3()")));
- (NSMutableArray<QpsQPosition *> *)component4 __attribute__((swift_name("component4()")));
- (QpsMovementStateTick *)doCopyVector:(QpsMoveVector *)vector currentTime:(int64_t)currentTime duration:(int64_t)duration clusterPositions:(NSMutableArray<QpsQPosition *> *)clusterPositions __attribute__((swift_name("doCopy(vector:currentTime:duration:clusterPositions:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSMutableArray<QpsQPosition *> *clusterPositions __attribute__((swift_name("clusterPositions")));
@property int64_t currentTime __attribute__((swift_name("currentTime")));
@property (readonly) int64_t duration __attribute__((swift_name("duration")));
@property QpsMoveVector *vector __attribute__((swift_name("vector")));
@end;

__attribute__((swift_name("PointF")))
@interface QpsPointF : QpsBase
- (instancetype)initWithPoint:(QpsPointF *)point vector:(QpsMoveVector *)vector __attribute__((swift_name("init(point:vector:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithX:(double)x y:(double)y __attribute__((swift_name("init(x:y:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithX:(float)x y_:(float)y __attribute__((swift_name("init(x:y_:)"))) __attribute__((objc_designated_initializer));
- (QpsPointF *)doCopy __attribute__((swift_name("doCopy()")));
- (double)distanceToPosition:(QpsPointF *)position __attribute__((swift_name("distanceTo(position:)")));
- (BOOL)isInValid __attribute__((swift_name("isInValid()")));
- (BOOL)isPointWithinDistancePosition:(QpsPointF *)position distance:(double)distance __attribute__((swift_name("isPointWithinDistance(position:distance:)")));
- (BOOL)isValid __attribute__((swift_name("isValid()")));
- (QpsPointF *)minusPoint:(QpsPointF *)point __attribute__((swift_name("minus(point:)")));
- (void)setX:(float)x y:(float)y __attribute__((swift_name("set(x:y:)")));
- (QpsQPosition *)toQPos __attribute__((swift_name("toQPos()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property float x __attribute__((swift_name("x")));
@property float y __attribute__((swift_name("y")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PointF.Companion")))
@interface QpsPointFCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PointWithOffset")))
@interface QpsPointWithOffset : QpsBase
- (instancetype)initWithPosition:(QpsPointF *)position offset:(QpsPointF *)offset __attribute__((swift_name("init(position:offset:)"))) __attribute__((objc_designated_initializer));
- (QpsPointF *)component1 __attribute__((swift_name("component1()")));
- (QpsPointF *)component2 __attribute__((swift_name("component2()")));
- (QpsPointWithOffset *)doCopyPosition:(QpsPointF *)position offset:(QpsPointF *)offset __attribute__((swift_name("doCopy(position:offset:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) QpsPointF *offset __attribute__((swift_name("offset")));
@property (readonly) QpsPointF *position __attribute__((swift_name("position")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QPosition")))
@interface QpsQPosition : QpsPointF
- (instancetype)initWithX:(float)x y:(float)y birthTime:(int64_t)birthTime __attribute__((swift_name("init(x:y:birthTime:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithX:(float)x y:(float)y birthTime:(int64_t)birthTime angleOffset:(double)angleOffset initialDirection:(double)initialDirection __attribute__((swift_name("init(x:y:birthTime:angleOffset:initialDirection:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPoint:(QpsPointF *)point vector:(QpsMoveVector *)vector __attribute__((swift_name("init(point:vector:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithX:(double)x y:(double)y __attribute__((swift_name("init(x:y:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithX:(float)x y_:(float)y __attribute__((swift_name("init(x:y_:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (double)distanceToPointPoint:(QpsPointF *)point __attribute__((swift_name("distanceToPoint(point:)")));
- (QpsPointF *)getLastValidPositionIter:(int32_t)iter __attribute__((swift_name("getLastValidPosition(iter:)")));
- (void)moveVector:(QpsMoveVector *)vector centerVector:(QpsMoveVector * _Nullable)centerVector radius:(double)radius closestFixedDirection:(QpsMoveVector *)closestFixedDirection __attribute__((swift_name("move(vector:centerVector:radius:closestFixedDirection:)")));
- (QpsPointF *)toPointF __attribute__((swift_name("toPointF()")));
- (NSString *)description __attribute__((swift_name("description()")));
- (void)updateSpeedPidSpeedMultiplier:(double)pidSpeedMultiplier __attribute__((swift_name("updateSpeed(pidSpeedMultiplier:)")));
- (void)updateSpeedMapZoneOffset:(double)offset __attribute__((swift_name("updateSpeedMapZone(offset:)")));
@property int32_t age __attribute__((swift_name("age")));
@property (readonly) double angleOffset __attribute__((swift_name("angleOffset")));
@property int32_t clusterId __attribute__((swift_name("clusterId")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property double pidSpeedMultiplier __attribute__((swift_name("pidSpeedMultiplier")));
@property NSMutableArray<QpsMoveVector *> *possibleDirections __attribute__((swift_name("possibleDirections")));
@property QpsMutableDictionary<QpsFloat *, QpsDouble *> *possibleDirectionsSpeedMap __attribute__((swift_name("possibleDirectionsSpeedMap")));
@property double speedMultiplier __attribute__((swift_name("speedMultiplier")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QPosition.Companion")))
@interface QpsQPositionCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *TAG __attribute__((swift_name("TAG")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RawSensorData")))
@interface QpsRawSensorData : QpsBase
- (instancetype)initWithValues:(QpsKotlinFloatArray *)values sensorDataType:(QpsRawSensorDataSensorDataType *)sensorDataType timestamp:(int64_t)timestamp __attribute__((swift_name("init(values:sensorDataType:timestamp:)"))) __attribute__((objc_designated_initializer));
- (QpsKotlinFloatArray *)component1 __attribute__((swift_name("component1()")));
- (QpsRawSensorDataSensorDataType *)component2 __attribute__((swift_name("component2()")));
- (int64_t)component3 __attribute__((swift_name("component3()")));
- (QpsRawSensorData *)doCopyValues:(QpsKotlinFloatArray *)values sensorDataType:(QpsRawSensorDataSensorDataType *)sensorDataType timestamp:(int64_t)timestamp __attribute__((swift_name("doCopy(values:sensorDataType:timestamp:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) QpsRawSensorDataSensorDataType *sensorDataType __attribute__((swift_name("sensorDataType")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@property (readonly) QpsKotlinFloatArray *values __attribute__((swift_name("values")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RawSensorData.Companion")))
@interface QpsRawSensorDataCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RawSensorData.SensorDataType")))
@interface QpsRawSensorDataSensorDataType : QpsKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) QpsRawSensorDataSensorDataType *acceleration __attribute__((swift_name("acceleration")));
@property (class, readonly) QpsRawSensorDataSensorDataType *gravity __attribute__((swift_name("gravity")));
@property (class, readonly) QpsRawSensorDataSensorDataType *rotation __attribute__((swift_name("rotation")));
@property (class, readonly) QpsRawSensorDataSensorDataType *ambientLight __attribute__((swift_name("ambientLight")));
@property (class, readonly) QpsRawSensorDataSensorDataType *proximity __attribute__((swift_name("proximity")));
@property (class, readonly) QpsRawSensorDataSensorDataType *geomagnetic __attribute__((swift_name("geomagnetic")));
@property (class, readonly) QpsRawSensorDataSensorDataType *accelerationUncalibrated __attribute__((swift_name("accelerationUncalibrated")));
@property (class, readonly) QpsRawSensorDataSensorDataType *gyroscopeUncalibrated __attribute__((swift_name("gyroscopeUncalibrated")));
- (int32_t)compareToOther:(QpsRawSensorDataSensorDataType *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReferenceAngle")))
@interface QpsReferenceAngle : QpsBase
- (instancetype)initWithAngle:(double)angle state:(QpsReferenceAngleState *)state __attribute__((swift_name("init(angle:state:)"))) __attribute__((objc_designated_initializer));
- (double)component1 __attribute__((swift_name("component1()")));
- (QpsReferenceAngleState *)component2 __attribute__((swift_name("component2()")));
- (QpsReferenceAngle *)doCopyAngle:(double)angle state:(QpsReferenceAngleState *)state __attribute__((swift_name("doCopy(angle:state:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) double angle __attribute__((swift_name("angle")));
@property (readonly) BOOL badAngle __attribute__((swift_name("badAngle")));
@property (readonly) QpsReferenceAngleState *state __attribute__((swift_name("state")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReferenceAngle.State")))
@interface QpsReferenceAngleState : QpsKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) QpsReferenceAngleState *estimatedSet __attribute__((swift_name("estimatedSet")));
@property (class, readonly) QpsReferenceAngleState *set __attribute__((swift_name("set")));
@property (class, readonly) QpsReferenceAngleState *unset __attribute__((swift_name("unset")));
- (int32_t)compareToOther:(QpsReferenceAngleState *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayData")))
@interface QpsReplayData : QpsBase
- (instancetype)initWithQpsValues:(NSString *)qpsValues versionOfVPS:(NSString *)versionOfVPS systemType:(NSString *)systemType ecsuValue:(QpsDouble * _Nullable)ecsuValue __attribute__((swift_name("init(qpsValues:versionOfVPS:systemType:ecsuValue:)"))) __attribute__((objc_designated_initializer));
- (void)addSensorDataData:(QpsRawSensorData *)data __attribute__((swift_name("addSensorData(data:)")));
- (void)addTickDataNavBundle:(QpsQPSNavBundle *)navBundle currentTime:(int64_t)currentTime azimuth:(double)azimuth magnitude:(double)magnitude qPositions:(NSArray<QpsQPosition *> *)qPositions __attribute__((swift_name("addTickData(navBundle:currentTime:azimuth:magnitude:qPositions:)")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (QpsDouble * _Nullable)component4 __attribute__((swift_name("component4()")));
- (QpsReplayData *)doCopyQpsValues:(NSString *)qpsValues versionOfVPS:(NSString *)versionOfVPS systemType:(NSString *)systemType ecsuValue:(QpsDouble * _Nullable)ecsuValue __attribute__((swift_name("doCopy(qpsValues:versionOfVPS:systemType:ecsuValue:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) QpsDouble * _Nullable ecsuValue __attribute__((swift_name("ecsuValue")));
@property (readonly) NSString *qpsValues __attribute__((swift_name("qpsValues")));
@property (readonly) NSString *systemType __attribute__((swift_name("systemType")));
@property (readonly) NSString *versionOfVPS __attribute__((swift_name("versionOfVPS")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayData.Companion")))
@interface QpsReplayDataCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayData.InteractorAction")))
@interface QpsReplayDataInteractorAction : QpsBase
- (instancetype)initWithTimestamp:(int64_t)timestamp type:(QpsReplayDataInteractorActionType *)type __attribute__((swift_name("init(timestamp:type:)"))) __attribute__((objc_designated_initializer));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (QpsReplayDataInteractorActionType *)component2 __attribute__((swift_name("component2()")));
- (QpsReplayDataInteractorAction *)doCopyTimestamp:(int64_t)timestamp type:(QpsReplayDataInteractorActionType *)type __attribute__((swift_name("doCopy(timestamp:type:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@property (readonly) QpsReplayDataInteractorActionType *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayData.InteractorActionCompanion")))
@interface QpsReplayDataInteractorActionCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayData.InteractorActionType")))
@interface QpsReplayDataInteractorActionType : QpsKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) QpsReplayDataInteractorActionType *syncposition __attribute__((swift_name("syncposition")));
- (int32_t)compareToOther:(QpsReplayDataInteractorActionType *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayData.Tick")))
@interface QpsReplayDataTick : QpsBase
- (instancetype)initWithCurrentTime:(int64_t)currentTime stableDirection:(QpsKotlinPair *)stableDirection centerPoint:(QpsKotlinPair * _Nullable)centerPoint radius:(double)radius qPosition:(NSArray<QpsKotlinPair *> *)qPosition __attribute__((swift_name("init(currentTime:stableDirection:centerPoint:radius:qPosition:)"))) __attribute__((objc_designated_initializer));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (QpsKotlinPair *)component2 __attribute__((swift_name("component2()")));
- (QpsKotlinPair * _Nullable)component3 __attribute__((swift_name("component3()")));
- (double)component4 __attribute__((swift_name("component4()")));
- (NSArray<QpsKotlinPair *> *)component5 __attribute__((swift_name("component5()")));
- (QpsReplayDataTick *)doCopyCurrentTime:(int64_t)currentTime stableDirection:(QpsKotlinPair *)stableDirection centerPoint:(QpsKotlinPair * _Nullable)centerPoint radius:(double)radius qPosition:(NSArray<QpsKotlinPair *> *)qPosition __attribute__((swift_name("doCopy(currentTime:stableDirection:centerPoint:radius:qPosition:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) QpsKotlinPair * _Nullable centerPoint __attribute__((swift_name("centerPoint")));
@property (readonly) int64_t currentTime __attribute__((swift_name("currentTime")));
@property (readonly) NSArray<QpsKotlinPair *> *qPosition __attribute__((swift_name("qPosition")));
@property (readonly) double radius __attribute__((swift_name("radius")));
@property (readonly) QpsKotlinPair *stableDirection __attribute__((swift_name("stableDirection")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayData.TickCompanion")))
@interface QpsReplayDataTickCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayPosition")))
@interface QpsReplayPosition : QpsBase
- (instancetype)initWithX:(float)x y:(float)y __attribute__((swift_name("init(x:y:)"))) __attribute__((objc_designated_initializer));
- (float)component1 __attribute__((swift_name("component1()")));
- (float)component2 __attribute__((swift_name("component2()")));
- (QpsReplayPosition *)doCopyX:(float)x y:(float)y __attribute__((swift_name("doCopy(x:y:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) float x __attribute__((swift_name("x")));
@property (readonly) float y __attribute__((swift_name("y")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReplayPosition.Companion")))
@interface QpsReplayPositionCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SpeedData")))
@interface QpsSpeedData : QpsBase
- (instancetype)initWithSpeed:(float)speed displacement:(float)displacement valley:(float)valley hasGoodSpeed:(BOOL)hasGoodSpeed __attribute__((swift_name("init(speed:displacement:valley:hasGoodSpeed:)"))) __attribute__((objc_designated_initializer));
- (float)component1 __attribute__((swift_name("component1()")));
- (float)component2 __attribute__((swift_name("component2()")));
- (float)component3 __attribute__((swift_name("component3()")));
- (BOOL)component4 __attribute__((swift_name("component4()")));
- (QpsSpeedData *)doCopySpeed:(float)speed displacement:(float)displacement valley:(float)valley hasGoodSpeed:(BOOL)hasGoodSpeed __attribute__((swift_name("doCopy(speed:displacement:valley:hasGoodSpeed:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) float displacement __attribute__((swift_name("displacement")));
@property (readonly) BOOL hasGoodSpeed __attribute__((swift_name("hasGoodSpeed")));
@property (readonly) float speed __attribute__((swift_name("speed")));
@property (readonly) float valley __attribute__((swift_name("valley")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedMoveVector")))
@interface QpsTimedMoveVector : QpsMoveVector
- (instancetype)initWithMoveVector:(QpsMoveVector *)moveVector duration:(double)duration timestamp:(int64_t)timestamp __attribute__((swift_name("init(moveVector:duration:timestamp:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithAzimuth:(float)azimuth magnitude:(double)magnitude duration:(double)duration timestamp:(int64_t)timestamp confidence:(double)confidence __attribute__((swift_name("init(azimuth:magnitude:duration:timestamp:confidence:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithAzimuth:(float)azimuth magnitude:(double)magnitude confidence:(double)confidence __attribute__((swift_name("init(azimuth:magnitude:confidence:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithX:(double)x y:(double)y confidence:(double)confidence __attribute__((swift_name("init(x:y:confidence:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (QpsTimedMoveVector *)divDivisor:(double)divisor __attribute__((swift_name("div(divisor:)")));
- (QpsTimedMoveVector *)minusVector_:(QpsTimedMoveVector *)vector __attribute__((swift_name("minus(vector_:)")));
- (QpsTimedMoveVector *)plusVector_:(QpsTimedMoveVector *)vector __attribute__((swift_name("plus(vector_:)")));
- (QpsTimedMoveVector *)timesFactor:(double)factor __attribute__((swift_name("times(factor:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) double duration __attribute__((swift_name("duration")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedReplayPositions")))
@interface QpsTimedReplayPositions : QpsBase
- (instancetype)initWithTimestamp:(int64_t)timestamp positions:(NSArray<QpsReplayPosition *> *)positions __attribute__((swift_name("init(timestamp:positions:)"))) __attribute__((objc_designated_initializer));
- (int64_t)component1 __attribute__((swift_name("component1()")));
- (NSArray<QpsReplayPosition *> *)component2 __attribute__((swift_name("component2()")));
- (QpsTimedReplayPositions *)doCopyTimestamp:(int64_t)timestamp positions:(NSArray<QpsReplayPosition *> *)positions __attribute__((swift_name("doCopy(timestamp:positions:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<QpsReplayPosition *> *positions __attribute__((swift_name("positions")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimedReplayPositions.Companion")))
@interface QpsTimedReplayPositionsCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("SensorBundle")))
@interface QpsSensorBundle : QpsBase
@property (readonly) double confidence __attribute__((swift_name("confidence")));
@property (readonly) int64_t timeStamp __attribute__((swift_name("timeStamp")));
@property (readonly) QpsSensorBundleBundleType *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GeoMagneticRotationBundle")))
@interface QpsGeoMagneticRotationBundle : QpsSensorBundle
- (instancetype)initWithAzimuth:(QpsDouble * _Nullable)azimuth pitch:(QpsDouble * _Nullable)pitch roll:(QpsDouble * _Nullable)roll timeStamp:(int64_t)timeStamp confidence:(double)confidence __attribute__((swift_name("init(azimuth:pitch:roll:timeStamp:confidence:)"))) __attribute__((objc_designated_initializer));
@property (readonly) QpsDouble * _Nullable azimuth __attribute__((swift_name("azimuth")));
@property (readonly) QpsDouble * _Nullable pitch __attribute__((swift_name("pitch")));
@property (readonly) QpsDouble * _Nullable roll __attribute__((swift_name("roll")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RotationBundle")))
@interface QpsRotationBundle : QpsSensorBundle
- (instancetype)initWithRotation:(float)rotation turn:(QpsRotationBundleTurn *)turn timeStamp:(int64_t)timeStamp confidence:(double)confidence __attribute__((swift_name("init(rotation:turn:timeStamp:confidence:)"))) __attribute__((objc_designated_initializer));
@property (readonly) float rotation __attribute__((swift_name("rotation")));
@property (readonly) QpsRotationBundleTurn *turn __attribute__((swift_name("turn")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RotationBundle.Turn")))
@interface QpsRotationBundleTurn : QpsKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) QpsRotationBundleTurn *left __attribute__((swift_name("left")));
@property (class, readonly) QpsRotationBundleTurn *none __attribute__((swift_name("none")));
@property (class, readonly) QpsRotationBundleTurn *right __attribute__((swift_name("right")));
- (int32_t)compareToOther:(QpsRotationBundleTurn *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SensorBundle.BundleType")))
@interface QpsSensorBundleBundleType : QpsKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) QpsSensorBundleBundleType *speed __attribute__((swift_name("speed")));
@property (class, readonly) QpsSensorBundleBundleType *fusionRotationSensor __attribute__((swift_name("fusionRotationSensor")));
@property (class, readonly) QpsSensorBundleBundleType *geomagneticRotationSensor __attribute__((swift_name("geomagneticRotationSensor")));
- (int32_t)compareToOther:(QpsSensorBundleBundleType *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SpeedBundle")))
@interface QpsSpeedBundle : QpsSensorBundle
- (instancetype)initWithSpeed:(float)speed stepData:(QpsStepData *)stepData __attribute__((swift_name("init(speed:stepData:)"))) __attribute__((objc_designated_initializer));
@property (readonly) float speed __attribute__((swift_name("speed")));
@property (readonly) QpsStepData *stepData __attribute__((swift_name("stepData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DirectionBundle")))
@interface QpsDirectionBundle : QpsBase
- (instancetype)initWithDirection:(double)direction __attribute__((swift_name("init(direction:)"))) __attribute__((objc_designated_initializer));
- (double)component1 __attribute__((swift_name("component1()")));
- (QpsDirectionBundle *)doCopyDirection:(double)direction __attribute__((swift_name("doCopy(direction:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) double direction __attribute__((swift_name("direction")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PrecisionCircle")))
@interface QpsPrecisionCircle : QpsBase
- (instancetype)initWithCenter:(QpsPointF *)center radius:(double)radius __attribute__((swift_name("init(center:radius:)"))) __attribute__((objc_designated_initializer));
- (QpsPointF *)component1 __attribute__((swift_name("component1()")));
- (double)component2 __attribute__((swift_name("component2()")));
- (QpsPrecisionCircle *)doCopyCenter:(QpsPointF *)center radius:(double)radius __attribute__((swift_name("doCopy(center:radius:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) QpsPointF *center __attribute__((swift_name("center")));
@property (readonly) double radius __attribute__((swift_name("radius")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QPSNavBundle")))
@interface QpsQPSNavBundle : QpsBase
- (instancetype)initWithPosition:(QpsPointF * _Nullable)position timestamp:(int64_t)timestamp debugPositions:(NSArray<QpsDebugPosition *> *)debugPositions precisionCircle:(QpsPrecisionCircle *)precisionCircle confidence:(double)confidence __attribute__((swift_name("init(position:timestamp:debugPositions:precisionCircle:confidence:)"))) __attribute__((objc_designated_initializer));
- (QpsPointF * _Nullable)component1 __attribute__((swift_name("component1()")));
- (int64_t)component2 __attribute__((swift_name("component2()")));
- (NSArray<QpsDebugPosition *> *)component3 __attribute__((swift_name("component3()")));
- (QpsPrecisionCircle *)component4 __attribute__((swift_name("component4()")));
- (double)component5 __attribute__((swift_name("component5()")));
- (QpsQPSNavBundle *)doCopyPosition:(QpsPointF * _Nullable)position timestamp:(int64_t)timestamp debugPositions:(NSArray<QpsDebugPosition *> *)debugPositions precisionCircle:(QpsPrecisionCircle *)precisionCircle confidence:(double)confidence __attribute__((swift_name("doCopy(position:timestamp:debugPositions:precisionCircle:confidence:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) double confidence __attribute__((swift_name("confidence")));
@property (readonly) NSArray<QpsDebugPosition *> *debugPositions __attribute__((swift_name("debugPositions")));
@property (readonly) QpsPointF * _Nullable position __attribute__((swift_name("position")));
@property (readonly) QpsPrecisionCircle *precisionCircle __attribute__((swift_name("precisionCircle")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@end;

__attribute__((swift_name("MLModel")))
@interface QpsMLModel : QpsBase
- (instancetype)initWithTransformers:(NSMutableArray<QpsVectorTransformer *> *)transformers __attribute__((swift_name("init(transformers:)"))) __attribute__((objc_designated_initializer));
- (BOOL)pipelineData:(QpsKotlinArray *)data __attribute__((swift_name("pipeline(data:)")));
- (BOOL)predictData:(QpsKotlinArray *)data __attribute__((swift_name("predict(data:)")));
@property NSMutableArray<QpsVectorTransformer *> *transformers __attribute__((swift_name("transformers")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EllipseEvaluator")))
@interface QpsEllipseEvaluator : QpsMLModel
- (instancetype)initWithCenterX:(float)centerX centerY:(float)centerY alpha:(float)alpha radiusA:(float)radiusA radiusB:(float)radiusB scaler:(QpsStandardScaler *)scaler PCA:(QpsPCATransformer *)PCA __attribute__((swift_name("init(centerX:centerY:alpha:radiusA:radiusB:scaler:PCA:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithTransformers:(NSMutableArray<QpsVectorTransformer *> *)transformers __attribute__((swift_name("init(transformers:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (BOOL)predictData:(QpsKotlinArray *)data __attribute__((swift_name("predict(data:)")));
@property (readonly) float alpha __attribute__((swift_name("alpha")));
@property (readonly) float centerX __attribute__((swift_name("centerX")));
@property (readonly) float centerY __attribute__((swift_name("centerY")));
@property (readonly) float radiusA __attribute__((swift_name("radiusA")));
@property (readonly) float radiusB __attribute__((swift_name("radiusB")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FirstPCAEvaluator")))
@interface QpsFirstPCAEvaluator : QpsMLModel
- (instancetype)initWithX0:(float)x0 scaler:(QpsStandardScaler *)scaler PCA:(QpsPCATransformer *)PCA __attribute__((swift_name("init(x0:scaler:PCA:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithTransformers:(NSMutableArray<QpsVectorTransformer *> *)transformers __attribute__((swift_name("init(transformers:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (BOOL)predictData:(QpsKotlinArray *)data __attribute__((swift_name("predict(data:)")));
@property (readonly) float x0 __attribute__((swift_name("x0")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("HyperbolaEvaluator")))
@interface QpsHyperbolaEvaluator : QpsMLModel
- (instancetype)initWithScaler:(QpsStandardScaler *)scaler PCA:(QpsPCATransformer *)PCA __attribute__((swift_name("init(scaler:PCA:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithTransformers:(NSMutableArray<QpsVectorTransformer *> *)transformers __attribute__((swift_name("init(transformers:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (BOOL)predictData:(QpsKotlinArray *)data __attribute__((swift_name("predict(data:)")));
@end;

__attribute__((swift_name("SVM")))
@interface QpsSVM : QpsMLModel
- (instancetype)initWithTransformers:(NSMutableArray<QpsVectorTransformer *> *)transformers weights:(NSMutableArray<QpsFloat *> *)weights intercept:(float)intercept supportVectors:(NSMutableArray<NSMutableArray<QpsFloat *> *> *)supportVectors kernel:(QpsFloat *(^)(QpsKotlinArray *, NSMutableArray<QpsFloat *> *))kernel __attribute__((swift_name("init(transformers:weights:intercept:supportVectors:kernel:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithTransformers:(NSMutableArray<QpsVectorTransformer *> *)transformers __attribute__((swift_name("init(transformers:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (BOOL)predictData:(QpsKotlinArray *)x __attribute__((swift_name("predict(data:)")));
@property float intercept __attribute__((swift_name("intercept")));
@property (readonly) QpsFloat *(^kernel)(QpsKotlinArray *, NSMutableArray<QpsFloat *> *) __attribute__((swift_name("kernel")));
@property (readonly) NSMutableArray<NSMutableArray<QpsFloat *> *> *supportVectors __attribute__((swift_name("supportVectors")));
@property NSMutableArray<QpsFloat *> *weights __attribute__((swift_name("weights")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LinearSVM")))
@interface QpsLinearSVM : QpsSVM
- (instancetype)initWithWeights:(NSMutableArray<QpsFloat *> *)weights intercept:(float)intercept transformers:(NSMutableArray<QpsVectorTransformer *> *)transformers __attribute__((swift_name("init(weights:intercept:transformers:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithTransformers:(NSMutableArray<QpsVectorTransformer *> *)transformers weights:(NSMutableArray<QpsFloat *> *)weights intercept:(float)intercept supportVectors:(NSMutableArray<NSMutableArray<QpsFloat *> *> *)supportVectors kernel:(QpsFloat *(^)(QpsKotlinArray *, NSMutableArray<QpsFloat *> *))kernel __attribute__((swift_name("init(transformers:weights:intercept:supportVectors:kernel:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (BOOL)predictData:(QpsKotlinArray *)x __attribute__((swift_name("predict(data:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LinearSVMSwingStepDetectorValues")))
@interface QpsLinearSVMSwingStepDetectorValues : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)linearSVMSwingStepDetectorValues __attribute__((swift_name("init()")));
@property (readonly) NSMutableArray<NSMutableArray<QpsFloat *> *> *PCA_components_hugo_04_dec_2020 __attribute__((swift_name("PCA_components_hugo_04_dec_2020")));
@property (readonly) NSMutableArray<QpsFloat *> *c0_hugo_04_dec_2020 __attribute__((swift_name("c0_hugo_04_dec_2020")));
@property (readonly) NSMutableArray<QpsFloat *> *c1_hugo_04_dec_2020 __attribute__((swift_name("c1_hugo_04_dec_2020")));
@property (readonly) NSMutableArray<QpsFloat *> *c2_hugo_04_dec_2020 __attribute__((swift_name("c2_hugo_04_dec_2020")));
@property (readonly) NSMutableArray<QpsFloat *> *c3_hugo_04_dec_2020 __attribute__((swift_name("c3_hugo_04_dec_2020")));
@property (readonly) NSMutableArray<QpsFloat *> *coef_hugo_04_dec_2020 __attribute__((swift_name("coef_hugo_04_dec_2020")));
@property (readonly) float intercept_hugo_04_dec_2020 __attribute__((swift_name("intercept_hugo_04_dec_2020")));
@property (readonly) NSMutableArray<QpsFloat *> *means_hugo_04_dec_2020 __attribute__((swift_name("means_hugo_04_dec_2020")));
@property (readonly) NSMutableArray<QpsFloat *> *scales_hugo_04_dec_2020 __attribute__((swift_name("scales_hugo_04_dec_2020")));
@end;

__attribute__((swift_name("VectorTransformer")))
@interface QpsVectorTransformer : QpsBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (QpsKotlinArray *)transformData:(QpsKotlinArray *)data __attribute__((swift_name("transform(data:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PCATransformer")))
@interface QpsPCATransformer : QpsVectorTransformer
- (instancetype)initWithComponents:(NSMutableArray<NSMutableArray<QpsFloat *> *> *)components __attribute__((swift_name("init(components:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (QpsKotlinArray *)transformData:(QpsKotlinArray *)data __attribute__((swift_name("transform(data:)")));
@property (readonly) NSMutableArray<NSMutableArray<QpsFloat *> *> *components __attribute__((swift_name("components")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RegularRBFSVMsupport")))
@interface QpsRegularRBFSVMsupport : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)regularRBFSVMsupport __attribute__((swift_name("init()")));
@property (readonly) NSMutableArray<NSMutableArray<QpsFloat *> *> *PCA_components_spack_rbf_v2 __attribute__((swift_name("PCA_components_spack_rbf_v2")));
@property (readonly) float PCA_variance_spack_rbf __attribute__((swift_name("PCA_variance_spack_rbf")));
@property (readonly) NSMutableArray<NSMutableArray<QpsFloat *> *> *all_sv_spack_rbf_v2 __attribute__((swift_name("all_sv_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *c0_spack_rbf_v2 __attribute__((swift_name("c0_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *c1_spack_rbf_v2 __attribute__((swift_name("c1_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *c2_spack_rbf_v2 __attribute__((swift_name("c2_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *c3_spack_rbf_v2 __attribute__((swift_name("c3_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *c4_spack_rbf_v2 __attribute__((swift_name("c4_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *c5_spack_rbf_v2 __attribute__((swift_name("c5_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *c6_spack_rbf_v2 __attribute__((swift_name("c6_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *c7_spack_rbf_v2 __attribute__((swift_name("c7_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *dual_coef_spack_rbf_v2 __attribute__((swift_name("dual_coef_spack_rbf_v2")));
@property (readonly) float intercept_spack_rbf_v2 __attribute__((swift_name("intercept_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *means_spack_rbf_v2 __attribute__((swift_name("means_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsInt *> *removed_vals_spack_rbf_v2 __attribute__((swift_name("removed_vals_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *scales_spack_rbf_v2 __attribute__((swift_name("scales_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv0_spack_rbf_v2 __attribute__((swift_name("sv0_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv100_spack_rbf_v2 __attribute__((swift_name("sv100_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv101_spack_rbf_v2 __attribute__((swift_name("sv101_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv102_spack_rbf_v2 __attribute__((swift_name("sv102_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv103_spack_rbf_v2 __attribute__((swift_name("sv103_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv104_spack_rbf_v2 __attribute__((swift_name("sv104_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv105_spack_rbf_v2 __attribute__((swift_name("sv105_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv106_spack_rbf_v2 __attribute__((swift_name("sv106_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv107_spack_rbf_v2 __attribute__((swift_name("sv107_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv108_spack_rbf_v2 __attribute__((swift_name("sv108_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv109_spack_rbf_v2 __attribute__((swift_name("sv109_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv10_spack_rbf_v2 __attribute__((swift_name("sv10_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv110_spack_rbf_v2 __attribute__((swift_name("sv110_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv111_spack_rbf_v2 __attribute__((swift_name("sv111_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv112_spack_rbf_v2 __attribute__((swift_name("sv112_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv113_spack_rbf_v2 __attribute__((swift_name("sv113_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv114_spack_rbf_v2 __attribute__((swift_name("sv114_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv115_spack_rbf_v2 __attribute__((swift_name("sv115_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv116_spack_rbf_v2 __attribute__((swift_name("sv116_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv117_spack_rbf_v2 __attribute__((swift_name("sv117_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv118_spack_rbf_v2 __attribute__((swift_name("sv118_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv119_spack_rbf_v2 __attribute__((swift_name("sv119_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv11_spack_rbf_v2 __attribute__((swift_name("sv11_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv120_spack_rbf_v2 __attribute__((swift_name("sv120_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv121_spack_rbf_v2 __attribute__((swift_name("sv121_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv122_spack_rbf_v2 __attribute__((swift_name("sv122_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv123_spack_rbf_v2 __attribute__((swift_name("sv123_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv124_spack_rbf_v2 __attribute__((swift_name("sv124_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv125_spack_rbf_v2 __attribute__((swift_name("sv125_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv126_spack_rbf_v2 __attribute__((swift_name("sv126_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv127_spack_rbf_v2 __attribute__((swift_name("sv127_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv128_spack_rbf_v2 __attribute__((swift_name("sv128_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv129_spack_rbf_v2 __attribute__((swift_name("sv129_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv12_spack_rbf_v2 __attribute__((swift_name("sv12_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv130_spack_rbf_v2 __attribute__((swift_name("sv130_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv131_spack_rbf_v2 __attribute__((swift_name("sv131_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv132_spack_rbf_v2 __attribute__((swift_name("sv132_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv133_spack_rbf_v2 __attribute__((swift_name("sv133_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv134_spack_rbf_v2 __attribute__((swift_name("sv134_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv13_spack_rbf_v2 __attribute__((swift_name("sv13_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv14_spack_rbf_v2 __attribute__((swift_name("sv14_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv15_spack_rbf_v2 __attribute__((swift_name("sv15_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv16_spack_rbf_v2 __attribute__((swift_name("sv16_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv17_spack_rbf_v2 __attribute__((swift_name("sv17_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv18_spack_rbf_v2 __attribute__((swift_name("sv18_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv19_spack_rbf_v2 __attribute__((swift_name("sv19_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv1_spack_rbf_v2 __attribute__((swift_name("sv1_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv20_spack_rbf_v2 __attribute__((swift_name("sv20_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv21_spack_rbf_v2 __attribute__((swift_name("sv21_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv22_spack_rbf_v2 __attribute__((swift_name("sv22_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv23_spack_rbf_v2 __attribute__((swift_name("sv23_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv24_spack_rbf_v2 __attribute__((swift_name("sv24_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv25_spack_rbf_v2 __attribute__((swift_name("sv25_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv26_spack_rbf_v2 __attribute__((swift_name("sv26_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv27_spack_rbf_v2 __attribute__((swift_name("sv27_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv28_spack_rbf_v2 __attribute__((swift_name("sv28_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv29_spack_rbf_v2 __attribute__((swift_name("sv29_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv2_spack_rbf_v2 __attribute__((swift_name("sv2_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv30_spack_rbf_v2 __attribute__((swift_name("sv30_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv31_spack_rbf_v2 __attribute__((swift_name("sv31_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv32_spack_rbf_v2 __attribute__((swift_name("sv32_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv33_spack_rbf_v2 __attribute__((swift_name("sv33_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv34_spack_rbf_v2 __attribute__((swift_name("sv34_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv35_spack_rbf_v2 __attribute__((swift_name("sv35_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv36_spack_rbf_v2 __attribute__((swift_name("sv36_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv37_spack_rbf_v2 __attribute__((swift_name("sv37_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv38_spack_rbf_v2 __attribute__((swift_name("sv38_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv39_spack_rbf_v2 __attribute__((swift_name("sv39_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv3_spack_rbf_v2 __attribute__((swift_name("sv3_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv40_spack_rbf_v2 __attribute__((swift_name("sv40_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv41_spack_rbf_v2 __attribute__((swift_name("sv41_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv42_spack_rbf_v2 __attribute__((swift_name("sv42_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv43_spack_rbf_v2 __attribute__((swift_name("sv43_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv44_spack_rbf_v2 __attribute__((swift_name("sv44_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv45_spack_rbf_v2 __attribute__((swift_name("sv45_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv46_spack_rbf_v2 __attribute__((swift_name("sv46_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv47_spack_rbf_v2 __attribute__((swift_name("sv47_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv48_spack_rbf_v2 __attribute__((swift_name("sv48_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv49_spack_rbf_v2 __attribute__((swift_name("sv49_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv4_spack_rbf_v2 __attribute__((swift_name("sv4_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv50_spack_rbf_v2 __attribute__((swift_name("sv50_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv51_spack_rbf_v2 __attribute__((swift_name("sv51_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv52_spack_rbf_v2 __attribute__((swift_name("sv52_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv53_spack_rbf_v2 __attribute__((swift_name("sv53_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv54_spack_rbf_v2 __attribute__((swift_name("sv54_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv55_spack_rbf_v2 __attribute__((swift_name("sv55_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv56_spack_rbf_v2 __attribute__((swift_name("sv56_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv57_spack_rbf_v2 __attribute__((swift_name("sv57_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv58_spack_rbf_v2 __attribute__((swift_name("sv58_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv59_spack_rbf_v2 __attribute__((swift_name("sv59_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv5_spack_rbf_v2 __attribute__((swift_name("sv5_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv60_spack_rbf_v2 __attribute__((swift_name("sv60_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv61_spack_rbf_v2 __attribute__((swift_name("sv61_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv62_spack_rbf_v2 __attribute__((swift_name("sv62_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv63_spack_rbf_v2 __attribute__((swift_name("sv63_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv64_spack_rbf_v2 __attribute__((swift_name("sv64_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv65_spack_rbf_v2 __attribute__((swift_name("sv65_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv66_spack_rbf_v2 __attribute__((swift_name("sv66_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv67_spack_rbf_v2 __attribute__((swift_name("sv67_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv68_spack_rbf_v2 __attribute__((swift_name("sv68_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv69_spack_rbf_v2 __attribute__((swift_name("sv69_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv6_spack_rbf_v2 __attribute__((swift_name("sv6_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv70_spack_rbf_v2 __attribute__((swift_name("sv70_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv71_spack_rbf_v2 __attribute__((swift_name("sv71_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv72_spack_rbf_v2 __attribute__((swift_name("sv72_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv73_spack_rbf_v2 __attribute__((swift_name("sv73_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv74_spack_rbf_v2 __attribute__((swift_name("sv74_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv75_spack_rbf_v2 __attribute__((swift_name("sv75_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv76_spack_rbf_v2 __attribute__((swift_name("sv76_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv77_spack_rbf_v2 __attribute__((swift_name("sv77_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv78_spack_rbf_v2 __attribute__((swift_name("sv78_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv79_spack_rbf_v2 __attribute__((swift_name("sv79_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv7_spack_rbf_v2 __attribute__((swift_name("sv7_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv80_spack_rbf_v2 __attribute__((swift_name("sv80_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv81_spack_rbf_v2 __attribute__((swift_name("sv81_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv82_spack_rbf_v2 __attribute__((swift_name("sv82_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv83_spack_rbf_v2 __attribute__((swift_name("sv83_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv84_spack_rbf_v2 __attribute__((swift_name("sv84_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv85_spack_rbf_v2 __attribute__((swift_name("sv85_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv86_spack_rbf_v2 __attribute__((swift_name("sv86_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv87_spack_rbf_v2 __attribute__((swift_name("sv87_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv88_spack_rbf_v2 __attribute__((swift_name("sv88_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv89_spack_rbf_v2 __attribute__((swift_name("sv89_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv8_spack_rbf_v2 __attribute__((swift_name("sv8_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv90_spack_rbf_v2 __attribute__((swift_name("sv90_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv91_spack_rbf_v2 __attribute__((swift_name("sv91_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv92_spack_rbf_v2 __attribute__((swift_name("sv92_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv93_spack_rbf_v2 __attribute__((swift_name("sv93_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv94_spack_rbf_v2 __attribute__((swift_name("sv94_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv95_spack_rbf_v2 __attribute__((swift_name("sv95_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv96_spack_rbf_v2 __attribute__((swift_name("sv96_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv97_spack_rbf_v2 __attribute__((swift_name("sv97_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv98_spack_rbf_v2 __attribute__((swift_name("sv98_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv99_spack_rbf_v2 __attribute__((swift_name("sv99_spack_rbf_v2")));
@property (readonly) NSMutableArray<QpsFloat *> *sv9_spack_rbf_v2 __attribute__((swift_name("sv9_spack_rbf_v2")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SVMHelpers")))
@interface QpsSVMHelpers : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)sVMHelpers __attribute__((swift_name("init()")));
- (float)rbfKernelX:(QpsKotlinArray *)x y:(NSMutableArray<QpsFloat *> *)y variance:(float)variance __attribute__((swift_name("rbfKernel(x:y:variance:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SpackRBFSVMsupport")))
@interface QpsSpackRBFSVMsupport : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)spackRBFSVMsupport __attribute__((swift_name("init()")));
@property (readonly) NSMutableArray<NSMutableArray<QpsFloat *> *> *PCA_components_spack_swing_12_okt_rbf __attribute__((swift_name("PCA_components_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<NSMutableArray<QpsFloat *> *> *PCA_components_spack_swing_rbf __attribute__((swift_name("PCA_components_spack_swing_rbf")));
@property (readonly) NSMutableArray<NSMutableArray<QpsFloat *> *> *PCA_components_weak_eval_swing __attribute__((swift_name("PCA_components_weak_eval_swing")));
@property (readonly) NSMutableArray<NSMutableArray<QpsFloat *> *> *PCA_components_weak_eval_swing_12_okt __attribute__((swift_name("PCA_components_weak_eval_swing_12_okt")));
@property (readonly) float PCA_variance_spack_swing_12_okt_rbf __attribute__((swift_name("PCA_variance_spack_swing_12_okt_rbf")));
@property (readonly) float PCA_variance_spack_swing_rbf __attribute__((swift_name("PCA_variance_spack_swing_rbf")));
@property (readonly) NSMutableArray<NSMutableArray<QpsFloat *> *> *all_sv_spack_swing_12_okt_rbf __attribute__((swift_name("all_sv_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<NSMutableArray<QpsFloat *> *> *all_sv_spack_swing_rbf __attribute__((swift_name("all_sv_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c0_spack_swing_12_okt_rbf __attribute__((swift_name("c0_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c0_spack_swing_rbf __attribute__((swift_name("c0_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c0_weak_eval_swing __attribute__((swift_name("c0_weak_eval_swing")));
@property (readonly) NSMutableArray<QpsFloat *> *c0_weak_eval_swing_12_okt __attribute__((swift_name("c0_weak_eval_swing_12_okt")));
@property (readonly) NSMutableArray<QpsFloat *> *c1_spack_swing_12_okt_rbf __attribute__((swift_name("c1_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c1_spack_swing_rbf __attribute__((swift_name("c1_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c1_weak_eval_swing __attribute__((swift_name("c1_weak_eval_swing")));
@property (readonly) NSMutableArray<QpsFloat *> *c1_weak_eval_swing_12_okt __attribute__((swift_name("c1_weak_eval_swing_12_okt")));
@property (readonly) NSMutableArray<QpsFloat *> *c2_spack_swing_12_okt_rbf __attribute__((swift_name("c2_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c2_spack_swing_rbf __attribute__((swift_name("c2_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c3_spack_swing_12_okt_rbf __attribute__((swift_name("c3_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c3_spack_swing_rbf __attribute__((swift_name("c3_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c4_spack_swing_12_okt_rbf __attribute__((swift_name("c4_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c4_spack_swing_rbf __attribute__((swift_name("c4_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c5_spack_swing_12_okt_rbf __attribute__((swift_name("c5_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c5_spack_swing_rbf __attribute__((swift_name("c5_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c6_spack_swing_12_okt_rbf __attribute__((swift_name("c6_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c6_spack_swing_rbf __attribute__((swift_name("c6_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c7_spack_swing_12_okt_rbf __attribute__((swift_name("c7_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *c7_spack_swing_rbf __attribute__((swift_name("c7_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *dual_coef_spack_swing_12_okt_rbf __attribute__((swift_name("dual_coef_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *dual_coef_spack_swing_rbf __attribute__((swift_name("dual_coef_spack_swing_rbf")));
@property (readonly) float intercept_spack_swing_12_okt_rbf __attribute__((swift_name("intercept_spack_swing_12_okt_rbf")));
@property (readonly) float intercept_spack_swing_rbf __attribute__((swift_name("intercept_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *means_spack_swing_12_okt_rbf __attribute__((swift_name("means_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *means_spack_swing_rbf __attribute__((swift_name("means_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *scales_spack_swing_12_okt_rbf __attribute__((swift_name("scales_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *scales_spack_swing_rbf __attribute__((swift_name("scales_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv0_spack_swing_12_okt_rbf __attribute__((swift_name("sv0_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv0_spack_swing_rbf __attribute__((swift_name("sv0_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv100_spack_swing_12_okt_rbf __attribute__((swift_name("sv100_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv100_spack_swing_rbf __attribute__((swift_name("sv100_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv101_spack_swing_12_okt_rbf __attribute__((swift_name("sv101_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv101_spack_swing_rbf __attribute__((swift_name("sv101_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv102_spack_swing_12_okt_rbf __attribute__((swift_name("sv102_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv102_spack_swing_rbf __attribute__((swift_name("sv102_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv103_spack_swing_12_okt_rbf __attribute__((swift_name("sv103_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv103_spack_swing_rbf __attribute__((swift_name("sv103_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv104_spack_swing_12_okt_rbf __attribute__((swift_name("sv104_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv104_spack_swing_rbf __attribute__((swift_name("sv104_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv105_spack_swing_12_okt_rbf __attribute__((swift_name("sv105_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv105_spack_swing_rbf __attribute__((swift_name("sv105_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv106_spack_swing_12_okt_rbf __attribute__((swift_name("sv106_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv106_spack_swing_rbf __attribute__((swift_name("sv106_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv107_spack_swing_12_okt_rbf __attribute__((swift_name("sv107_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv107_spack_swing_rbf __attribute__((swift_name("sv107_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv108_spack_swing_12_okt_rbf __attribute__((swift_name("sv108_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv108_spack_swing_rbf __attribute__((swift_name("sv108_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv109_spack_swing_12_okt_rbf __attribute__((swift_name("sv109_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv10_spack_swing_12_okt_rbf __attribute__((swift_name("sv10_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv10_spack_swing_rbf __attribute__((swift_name("sv10_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv110_spack_swing_12_okt_rbf __attribute__((swift_name("sv110_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv111_spack_swing_12_okt_rbf __attribute__((swift_name("sv111_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv112_spack_swing_12_okt_rbf __attribute__((swift_name("sv112_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv113_spack_swing_12_okt_rbf __attribute__((swift_name("sv113_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv114_spack_swing_12_okt_rbf __attribute__((swift_name("sv114_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv115_spack_swing_12_okt_rbf __attribute__((swift_name("sv115_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv116_spack_swing_12_okt_rbf __attribute__((swift_name("sv116_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv117_spack_swing_12_okt_rbf __attribute__((swift_name("sv117_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv118_spack_swing_12_okt_rbf __attribute__((swift_name("sv118_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv11_spack_swing_12_okt_rbf __attribute__((swift_name("sv11_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv11_spack_swing_rbf __attribute__((swift_name("sv11_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv12_spack_swing_12_okt_rbf __attribute__((swift_name("sv12_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv12_spack_swing_rbf __attribute__((swift_name("sv12_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv13_spack_swing_12_okt_rbf __attribute__((swift_name("sv13_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv13_spack_swing_rbf __attribute__((swift_name("sv13_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv14_spack_swing_12_okt_rbf __attribute__((swift_name("sv14_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv14_spack_swing_rbf __attribute__((swift_name("sv14_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv15_spack_swing_12_okt_rbf __attribute__((swift_name("sv15_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv15_spack_swing_rbf __attribute__((swift_name("sv15_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv16_spack_swing_12_okt_rbf __attribute__((swift_name("sv16_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv16_spack_swing_rbf __attribute__((swift_name("sv16_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv17_spack_swing_12_okt_rbf __attribute__((swift_name("sv17_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv17_spack_swing_rbf __attribute__((swift_name("sv17_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv18_spack_swing_12_okt_rbf __attribute__((swift_name("sv18_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv18_spack_swing_rbf __attribute__((swift_name("sv18_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv19_spack_swing_12_okt_rbf __attribute__((swift_name("sv19_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv19_spack_swing_rbf __attribute__((swift_name("sv19_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv1_spack_swing_12_okt_rbf __attribute__((swift_name("sv1_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv1_spack_swing_rbf __attribute__((swift_name("sv1_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv20_spack_swing_12_okt_rbf __attribute__((swift_name("sv20_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv20_spack_swing_rbf __attribute__((swift_name("sv20_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv21_spack_swing_12_okt_rbf __attribute__((swift_name("sv21_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv21_spack_swing_rbf __attribute__((swift_name("sv21_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv22_spack_swing_12_okt_rbf __attribute__((swift_name("sv22_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv22_spack_swing_rbf __attribute__((swift_name("sv22_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv23_spack_swing_12_okt_rbf __attribute__((swift_name("sv23_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv23_spack_swing_rbf __attribute__((swift_name("sv23_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv24_spack_swing_12_okt_rbf __attribute__((swift_name("sv24_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv24_spack_swing_rbf __attribute__((swift_name("sv24_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv25_spack_swing_12_okt_rbf __attribute__((swift_name("sv25_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv25_spack_swing_rbf __attribute__((swift_name("sv25_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv26_spack_swing_12_okt_rbf __attribute__((swift_name("sv26_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv26_spack_swing_rbf __attribute__((swift_name("sv26_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv27_spack_swing_12_okt_rbf __attribute__((swift_name("sv27_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv27_spack_swing_rbf __attribute__((swift_name("sv27_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv28_spack_swing_12_okt_rbf __attribute__((swift_name("sv28_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv28_spack_swing_rbf __attribute__((swift_name("sv28_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv29_spack_swing_12_okt_rbf __attribute__((swift_name("sv29_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv29_spack_swing_rbf __attribute__((swift_name("sv29_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv2_spack_swing_12_okt_rbf __attribute__((swift_name("sv2_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv2_spack_swing_rbf __attribute__((swift_name("sv2_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv30_spack_swing_12_okt_rbf __attribute__((swift_name("sv30_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv30_spack_swing_rbf __attribute__((swift_name("sv30_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv31_spack_swing_12_okt_rbf __attribute__((swift_name("sv31_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv31_spack_swing_rbf __attribute__((swift_name("sv31_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv32_spack_swing_12_okt_rbf __attribute__((swift_name("sv32_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv32_spack_swing_rbf __attribute__((swift_name("sv32_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv33_spack_swing_12_okt_rbf __attribute__((swift_name("sv33_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv33_spack_swing_rbf __attribute__((swift_name("sv33_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv34_spack_swing_12_okt_rbf __attribute__((swift_name("sv34_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv34_spack_swing_rbf __attribute__((swift_name("sv34_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv35_spack_swing_12_okt_rbf __attribute__((swift_name("sv35_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv35_spack_swing_rbf __attribute__((swift_name("sv35_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv36_spack_swing_12_okt_rbf __attribute__((swift_name("sv36_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv36_spack_swing_rbf __attribute__((swift_name("sv36_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv37_spack_swing_12_okt_rbf __attribute__((swift_name("sv37_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv37_spack_swing_rbf __attribute__((swift_name("sv37_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv38_spack_swing_12_okt_rbf __attribute__((swift_name("sv38_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv38_spack_swing_rbf __attribute__((swift_name("sv38_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv39_spack_swing_12_okt_rbf __attribute__((swift_name("sv39_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv39_spack_swing_rbf __attribute__((swift_name("sv39_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv3_spack_swing_12_okt_rbf __attribute__((swift_name("sv3_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv3_spack_swing_rbf __attribute__((swift_name("sv3_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv40_spack_swing_12_okt_rbf __attribute__((swift_name("sv40_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv40_spack_swing_rbf __attribute__((swift_name("sv40_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv41_spack_swing_12_okt_rbf __attribute__((swift_name("sv41_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv41_spack_swing_rbf __attribute__((swift_name("sv41_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv42_spack_swing_12_okt_rbf __attribute__((swift_name("sv42_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv42_spack_swing_rbf __attribute__((swift_name("sv42_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv43_spack_swing_12_okt_rbf __attribute__((swift_name("sv43_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv43_spack_swing_rbf __attribute__((swift_name("sv43_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv44_spack_swing_12_okt_rbf __attribute__((swift_name("sv44_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv44_spack_swing_rbf __attribute__((swift_name("sv44_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv45_spack_swing_12_okt_rbf __attribute__((swift_name("sv45_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv45_spack_swing_rbf __attribute__((swift_name("sv45_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv46_spack_swing_12_okt_rbf __attribute__((swift_name("sv46_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv46_spack_swing_rbf __attribute__((swift_name("sv46_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv47_spack_swing_12_okt_rbf __attribute__((swift_name("sv47_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv47_spack_swing_rbf __attribute__((swift_name("sv47_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv48_spack_swing_12_okt_rbf __attribute__((swift_name("sv48_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv48_spack_swing_rbf __attribute__((swift_name("sv48_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv49_spack_swing_12_okt_rbf __attribute__((swift_name("sv49_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv49_spack_swing_rbf __attribute__((swift_name("sv49_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv4_spack_swing_12_okt_rbf __attribute__((swift_name("sv4_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv4_spack_swing_rbf __attribute__((swift_name("sv4_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv50_spack_swing_12_okt_rbf __attribute__((swift_name("sv50_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv50_spack_swing_rbf __attribute__((swift_name("sv50_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv51_spack_swing_12_okt_rbf __attribute__((swift_name("sv51_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv51_spack_swing_rbf __attribute__((swift_name("sv51_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv52_spack_swing_12_okt_rbf __attribute__((swift_name("sv52_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv52_spack_swing_rbf __attribute__((swift_name("sv52_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv53_spack_swing_12_okt_rbf __attribute__((swift_name("sv53_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv53_spack_swing_rbf __attribute__((swift_name("sv53_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv54_spack_swing_12_okt_rbf __attribute__((swift_name("sv54_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv54_spack_swing_rbf __attribute__((swift_name("sv54_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv55_spack_swing_12_okt_rbf __attribute__((swift_name("sv55_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv55_spack_swing_rbf __attribute__((swift_name("sv55_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv56_spack_swing_12_okt_rbf __attribute__((swift_name("sv56_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv56_spack_swing_rbf __attribute__((swift_name("sv56_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv57_spack_swing_12_okt_rbf __attribute__((swift_name("sv57_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv57_spack_swing_rbf __attribute__((swift_name("sv57_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv58_spack_swing_12_okt_rbf __attribute__((swift_name("sv58_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv58_spack_swing_rbf __attribute__((swift_name("sv58_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv59_spack_swing_12_okt_rbf __attribute__((swift_name("sv59_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv59_spack_swing_rbf __attribute__((swift_name("sv59_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv5_spack_swing_12_okt_rbf __attribute__((swift_name("sv5_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv5_spack_swing_rbf __attribute__((swift_name("sv5_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv60_spack_swing_12_okt_rbf __attribute__((swift_name("sv60_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv60_spack_swing_rbf __attribute__((swift_name("sv60_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv61_spack_swing_12_okt_rbf __attribute__((swift_name("sv61_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv61_spack_swing_rbf __attribute__((swift_name("sv61_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv62_spack_swing_12_okt_rbf __attribute__((swift_name("sv62_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv62_spack_swing_rbf __attribute__((swift_name("sv62_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv63_spack_swing_12_okt_rbf __attribute__((swift_name("sv63_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv63_spack_swing_rbf __attribute__((swift_name("sv63_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv64_spack_swing_12_okt_rbf __attribute__((swift_name("sv64_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv64_spack_swing_rbf __attribute__((swift_name("sv64_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv65_spack_swing_12_okt_rbf __attribute__((swift_name("sv65_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv65_spack_swing_rbf __attribute__((swift_name("sv65_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv66_spack_swing_12_okt_rbf __attribute__((swift_name("sv66_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv66_spack_swing_rbf __attribute__((swift_name("sv66_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv67_spack_swing_12_okt_rbf __attribute__((swift_name("sv67_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv67_spack_swing_rbf __attribute__((swift_name("sv67_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv68_spack_swing_12_okt_rbf __attribute__((swift_name("sv68_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv68_spack_swing_rbf __attribute__((swift_name("sv68_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv69_spack_swing_12_okt_rbf __attribute__((swift_name("sv69_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv69_spack_swing_rbf __attribute__((swift_name("sv69_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv6_spack_swing_12_okt_rbf __attribute__((swift_name("sv6_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv6_spack_swing_rbf __attribute__((swift_name("sv6_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv70_spack_swing_12_okt_rbf __attribute__((swift_name("sv70_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv70_spack_swing_rbf __attribute__((swift_name("sv70_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv71_spack_swing_12_okt_rbf __attribute__((swift_name("sv71_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv71_spack_swing_rbf __attribute__((swift_name("sv71_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv72_spack_swing_12_okt_rbf __attribute__((swift_name("sv72_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv72_spack_swing_rbf __attribute__((swift_name("sv72_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv73_spack_swing_12_okt_rbf __attribute__((swift_name("sv73_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv73_spack_swing_rbf __attribute__((swift_name("sv73_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv74_spack_swing_12_okt_rbf __attribute__((swift_name("sv74_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv74_spack_swing_rbf __attribute__((swift_name("sv74_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv75_spack_swing_12_okt_rbf __attribute__((swift_name("sv75_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv75_spack_swing_rbf __attribute__((swift_name("sv75_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv76_spack_swing_12_okt_rbf __attribute__((swift_name("sv76_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv76_spack_swing_rbf __attribute__((swift_name("sv76_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv77_spack_swing_12_okt_rbf __attribute__((swift_name("sv77_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv77_spack_swing_rbf __attribute__((swift_name("sv77_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv78_spack_swing_12_okt_rbf __attribute__((swift_name("sv78_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv78_spack_swing_rbf __attribute__((swift_name("sv78_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv79_spack_swing_12_okt_rbf __attribute__((swift_name("sv79_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv79_spack_swing_rbf __attribute__((swift_name("sv79_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv7_spack_swing_12_okt_rbf __attribute__((swift_name("sv7_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv7_spack_swing_rbf __attribute__((swift_name("sv7_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv80_spack_swing_12_okt_rbf __attribute__((swift_name("sv80_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv80_spack_swing_rbf __attribute__((swift_name("sv80_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv81_spack_swing_12_okt_rbf __attribute__((swift_name("sv81_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv81_spack_swing_rbf __attribute__((swift_name("sv81_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv82_spack_swing_12_okt_rbf __attribute__((swift_name("sv82_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv82_spack_swing_rbf __attribute__((swift_name("sv82_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv83_spack_swing_12_okt_rbf __attribute__((swift_name("sv83_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv83_spack_swing_rbf __attribute__((swift_name("sv83_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv84_spack_swing_12_okt_rbf __attribute__((swift_name("sv84_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv84_spack_swing_rbf __attribute__((swift_name("sv84_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv85_spack_swing_12_okt_rbf __attribute__((swift_name("sv85_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv85_spack_swing_rbf __attribute__((swift_name("sv85_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv86_spack_swing_12_okt_rbf __attribute__((swift_name("sv86_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv86_spack_swing_rbf __attribute__((swift_name("sv86_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv87_spack_swing_12_okt_rbf __attribute__((swift_name("sv87_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv87_spack_swing_rbf __attribute__((swift_name("sv87_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv88_spack_swing_12_okt_rbf __attribute__((swift_name("sv88_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv88_spack_swing_rbf __attribute__((swift_name("sv88_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv89_spack_swing_12_okt_rbf __attribute__((swift_name("sv89_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv89_spack_swing_rbf __attribute__((swift_name("sv89_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv8_spack_swing_12_okt_rbf __attribute__((swift_name("sv8_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv8_spack_swing_rbf __attribute__((swift_name("sv8_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv90_spack_swing_12_okt_rbf __attribute__((swift_name("sv90_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv90_spack_swing_rbf __attribute__((swift_name("sv90_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv91_spack_swing_12_okt_rbf __attribute__((swift_name("sv91_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv91_spack_swing_rbf __attribute__((swift_name("sv91_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv92_spack_swing_12_okt_rbf __attribute__((swift_name("sv92_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv92_spack_swing_rbf __attribute__((swift_name("sv92_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv93_spack_swing_12_okt_rbf __attribute__((swift_name("sv93_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv93_spack_swing_rbf __attribute__((swift_name("sv93_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv94_spack_swing_12_okt_rbf __attribute__((swift_name("sv94_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv94_spack_swing_rbf __attribute__((swift_name("sv94_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv95_spack_swing_12_okt_rbf __attribute__((swift_name("sv95_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv95_spack_swing_rbf __attribute__((swift_name("sv95_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv96_spack_swing_12_okt_rbf __attribute__((swift_name("sv96_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv96_spack_swing_rbf __attribute__((swift_name("sv96_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv97_spack_swing_12_okt_rbf __attribute__((swift_name("sv97_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv97_spack_swing_rbf __attribute__((swift_name("sv97_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv98_spack_swing_12_okt_rbf __attribute__((swift_name("sv98_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv98_spack_swing_rbf __attribute__((swift_name("sv98_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv99_spack_swing_12_okt_rbf __attribute__((swift_name("sv99_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv99_spack_swing_rbf __attribute__((swift_name("sv99_spack_swing_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv9_spack_swing_12_okt_rbf __attribute__((swift_name("sv9_spack_swing_12_okt_rbf")));
@property (readonly) NSMutableArray<QpsFloat *> *sv9_spack_swing_rbf __attribute__((swift_name("sv9_spack_swing_rbf")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StandardScaler")))
@interface QpsStandardScaler : QpsVectorTransformer
- (instancetype)initWithScales:(NSMutableArray<QpsFloat *> *)scales means:(NSMutableArray<QpsFloat *> *)means __attribute__((swift_name("init(scales:means:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (QpsKotlinArray *)transformData:(QpsKotlinArray *)data __attribute__((swift_name("transform(data:)")));
@property (readonly) NSMutableArray<QpsFloat *> *means __attribute__((swift_name("means")));
@property (readonly) NSMutableArray<QpsFloat *> *scales __attribute__((swift_name("scales")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WeakEvaluatorParameters")))
@interface QpsWeakEvaluatorParameters : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)weakEvaluatorParameters __attribute__((swift_name("init()")));
@property (readonly) NSMutableArray<NSMutableArray<QpsFloat *> *> *PCA_components_weak_eval __attribute__((swift_name("PCA_components_weak_eval")));
@property (readonly) NSMutableArray<QpsFloat *> *c0_weak_eval __attribute__((swift_name("c0_weak_eval")));
@property (readonly) NSMutableArray<QpsFloat *> *c1_weak_eval __attribute__((swift_name("c1_weak_eval")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ContinuousMoveVectorQueue")))
@interface QpsContinuousMoveVectorQueue : QpsBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)addVectorList:(NSMutableArray<QpsTimedMoveVector *> *)vectorList __attribute__((swift_name("add(vectorList:)")));
- (void)addVector:(QpsTimedMoveVector *)vector __attribute__((swift_name("add(vector:)")));
- (void)clear __attribute__((swift_name("clear()")));
- (QpsMoveVector *)getByMillisRatio:(double)ratio __attribute__((swift_name("getByMillis(ratio:)")));
- (QpsMoveVector *)getByMillisTimeMillis:(int32_t)timeMillis __attribute__((swift_name("getByMillis(timeMillis:)")));
- (QpsMoveVector *)getOne __attribute__((swift_name("getOne()")));
- (QpsMoveVector *)getTwo __attribute__((swift_name("getTwo()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) double duration __attribute__((swift_name("duration")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ContinuousMoveVectorQueue.Companion")))
@interface QpsContinuousMoveVectorQueueCompanion : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) BOOL PRINT_MISC __attribute__((swift_name("PRINT_MISC")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("QuaternionUtils")))
@interface QpsQuaternionUtils : QpsBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (QpsKotlinFloatArray *)multiplyQuaternionQ:(QpsKotlinFloatArray *)q p:(QpsKotlinFloatArray *)p __attribute__((swift_name("multiplyQuaternion(q:p:)")));
- (QpsKotlinFloatArray *)quaternionExtractPitchQ:(QpsKotlinFloatArray *)q __attribute__((swift_name("quaternionExtractPitch(q:)")));
- (QpsKotlinFloatArray *)quaternionExtractYawQ:(QpsKotlinFloatArray *)q __attribute__((swift_name("quaternionExtractYaw(q:)")));
- (QpsKotlinFloatArray *)quaternionInverseQ:(QpsKotlinFloatArray *)q __attribute__((swift_name("quaternionInverse(q:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StepDataProvider")))
@interface QpsStepDataProvider : QpsBase <QpsIQPSReplayInteractor>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSString * _Nullable)getDataIdentifier:(NSString *)identifier printMissingFileException:(BOOL)printMissingFileException __attribute__((swift_name("getData(identifier:printMissingFileException:)")));
- (void)postDataData:(NSString *)data identifier:(NSString *)identifier __attribute__((swift_name("postData(data:identifier:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("VectorUtils")))
@interface QpsVectorUtils : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)vectorUtils __attribute__((swift_name("init()")));
- (float)angleBetweenVectorsV1:(QpsKotlinFloatArray *)v1 v2:(QpsKotlinFloatArray *)v2 __attribute__((swift_name("angleBetweenVectors(v1:v2:)")));
- (float)calculateDifferenceRatioBetweenVectorsDifferenceVector:(QpsKotlinFloatArray * _Nullable)differenceVector movementSumVector:(QpsKotlinFloatArray *)movementSumVector __attribute__((swift_name("calculateDifferenceRatioBetweenVectors(differenceVector:movementSumVector:)")));
- (QpsKotlinFloatArray *)createFloatVectorFromMoveVectorVector:(QpsMoveVector *)vector __attribute__((swift_name("createFloatVectorFromMoveVector(vector:)")));
- (QpsKotlinFloatArray * _Nullable)createVectorFromPointsPointA:(QpsPointF * _Nullable)pointA pointB:(QpsPointF * _Nullable)pointB __attribute__((swift_name("createVectorFromPoints(pointA:pointB:)")));
- (QpsKotlinFloatArray *)crossProductV1:(QpsKotlinFloatArray *)v1 v2:(QpsKotlinFloatArray *)v2 normed:(BOOL)normed __attribute__((swift_name("crossProduct(v1:v2:normed:)")));
- (double)degreesToRadiansAngDeg:(double)angDeg __attribute__((swift_name("degreesToRadians(angDeg:)")));
- (float)degreesToRadiansAngDeg_:(float)angDeg __attribute__((swift_name("degreesToRadians(angDeg_:)")));
- (float)distanceBetweenPointsP1:(QpsPointF *)p1 p2:(QpsPointF *)p2 __attribute__((swift_name("distanceBetweenPoints(p1:p2:)")));
- (float)dotProductV1:(QpsKotlinFloatArray *)v1 v2:(QpsKotlinFloatArray *)v2 __attribute__((swift_name("dotProduct(v1:v2:)")));
- (QpsKotlinFloatArray *)getProjectionOnPlaneVector:(QpsKotlinFloatArray *)vector normal:(QpsKotlinFloatArray *)normal __attribute__((swift_name("getProjectionOnPlane(vector:normal:)")));
- (void)getRotationMatrixFromVectorR:(QpsKotlinFloatArray *)R rotationVector:(QpsKotlinFloatArray *)rotationVector __attribute__((swift_name("getRotationMatrixFromVector(R:rotationVector:)")));
- (QpsKotlinFloatArray *)gramSchmidtV1:(QpsKotlinFloatArray *)v1 v2:(QpsKotlinFloatArray *)v2 __attribute__((swift_name("gramSchmidt(v1:v2:)")));
- (NSMutableArray<QpsKotlinFloatArray *> *)integrateVector:(NSMutableArray<QpsKotlinFloatArray *> *)vector timeStamps:(QpsKotlinArray *)timeStamps __attribute__((swift_name("integrate(vector:timeStamps:)")));
- (NSMutableArray<QpsFloat *> *)integrateVector:(NSArray<QpsFloat *> *)vector timeStamps_:(NSArray<QpsLong *> *)timeStamps __attribute__((swift_name("integrate(vector:timeStamps_:)")));
- (QpsKotlinArray *)matrixFromLongArrayArray:(QpsKotlinFloatArray *)array __attribute__((swift_name("matrixFromLongArray(array:)")));
- (QpsKotlinFloatArray *)minusV1:(QpsKotlinFloatArray *)v1 v2:(QpsKotlinFloatArray *)v2 __attribute__((swift_name("minus(v1:v2:)")));
- (QpsKotlinFloatArray *)multiplyMV2Matrix:(QpsKotlinArray *)matrix vector:(QpsKotlinFloatArray *)vector __attribute__((swift_name("multiplyMV2(matrix:vector:)")));
- (QpsKotlinFloatArray *)multiplyWithScalarVector:(QpsKotlinFloatArray *)vector scalar:(float)scalar __attribute__((swift_name("multiplyWithScalar(vector:scalar:)")));
- (QpsKotlinFloatArray *)normalizeVectorV:(QpsKotlinFloatArray *)v __attribute__((swift_name("normalizeVector(v:)")));
- (double)radiansToDegreesAngRad:(double)angRad __attribute__((swift_name("radiansToDegrees(angRad:)")));
- (float)radiansToDegreesAngRad_:(float)angRad __attribute__((swift_name("radiansToDegrees(angRad_:)")));
- (QpsKotlinFloatArray *)vectorAbsVector:(QpsKotlinFloatArray *)vector __attribute__((swift_name("vectorAbs(vector:)")));
- (QpsKotlinFloatArray *)vectorAdditionVector1:(QpsKotlinFloatArray *)vector1 vector2:(QpsKotlinFloatArray *)vector2 __attribute__((swift_name("vectorAddition(vector1:vector2:)")));
- (float)vectorMagnitudeV:(QpsKotlinFloatArray *)v __attribute__((swift_name("vectorMagnitude(v:)")));
- (float)vectorMagnitudeV_:(NSMutableArray<QpsFloat *> *)v __attribute__((swift_name("vectorMagnitude(v_:)")));
- (NSMutableArray<QpsFloat *> *)vectorSubtractionVector1:(QpsKotlinArray *)vector1 vector2:(NSMutableArray<QpsFloat *> *)vector2 __attribute__((swift_name("vectorSubtraction(vector1:vector2:)")));
- (QpsKotlinFloatArray *)vectorSubtractionVector1:(QpsKotlinFloatArray *)vector1 vector2_:(QpsKotlinFloatArray *)vector2 __attribute__((swift_name("vectorSubtraction(vector1:vector2_:)")));
- (NSMutableArray<QpsFloat *> *)vectorSubtractionVector1:(NSMutableArray<QpsFloat *> *)vector1 vector2__:(NSMutableArray<QpsFloat *> *)vector2 __attribute__((swift_name("vectorSubtraction(vector1:vector2__:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OffsetHelper")))
@interface QpsOffsetHelper : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)offsetHelper __attribute__((swift_name("init()")));
- (NSMutableArray<QpsMoveVector *> *)generateDirectionVectorsOffset:(double)offset __attribute__((swift_name("generateDirectionVectors(offset:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Spack")))
@interface QpsSpack : QpsBase
- (instancetype)initWithRaw_acc:(QpsKotlinArray *)raw_acc raw_grav:(QpsKotlinArray *)raw_grav raw_rot:(QpsKotlinArray *)raw_rot gravity_projected_data:(QpsKotlinArray *)gravity_projected_data planar_data:(QpsKotlinArray *)planar_data timestamps:(QpsKotlinArray *)timestamps __attribute__((swift_name("init(raw_acc:raw_grav:raw_rot:gravity_projected_data:planar_data:timestamps:)"))) __attribute__((objc_designated_initializer));
@property (readonly) QpsKotlinArray *gravity_projected_data __attribute__((swift_name("gravity_projected_data")));
@property (readonly) QpsKotlinArray *planar_data __attribute__((swift_name("planar_data")));
@property (readonly) QpsKotlinArray *raw_acc __attribute__((swift_name("raw_acc")));
@property (readonly) QpsKotlinArray *raw_grav __attribute__((swift_name("raw_grav")));
@property (readonly) QpsKotlinArray *raw_rot __attribute__((swift_name("raw_rot")));
@property (readonly) QpsKotlinArray *timestamps __attribute__((swift_name("timestamps")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SpackFeaturiser")))
@interface QpsSpackFeaturiser : QpsBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)spackFeaturiser __attribute__((swift_name("init()")));
- (QpsKotlinArray *)continuous_directionDirection:(QpsKotlinArray *)direction __attribute__((swift_name("continuous_direction(direction:)")));
- (QpsKotlinArray *)featuriseRegularSpackSpack:(QpsSpack *)spack freq:(double)freq __attribute__((swift_name("featuriseRegularSpack(spack:freq:)")));
- (QpsKotlinArray *)featuriseSwingSpackSpack:(QpsSpack *)spack freq:(double)freq __attribute__((swift_name("featuriseSwingSpack(spack:freq:)")));
- (QpsKotlinFloatArray *)getDisplacementSpack:(QpsSpack *)spack __attribute__((swift_name("getDisplacement(spack:)")));
- (float)getStraightComponentAcc:(QpsKotlinArray *)acc rot:(QpsKotlinArray *)rot __attribute__((swift_name("getStraightComponent(acc:rot:)")));
- (QpsKotlinArray *)get_directionRotation:(QpsKotlinArray *)rotation __attribute__((swift_name("get_direction(rotation:)")));
- (QpsKotlinDoubleArray *)leastSquarePolynomialCoefficientsX:(QpsKotlinDoubleArray *)x y:(QpsKotlinDoubleArray *)y freq:(double)freq d:(int32_t)d __attribute__((swift_name("leastSquarePolynomialCoefficients(x:y:freq:d:)")));
- (QpsKotlinDoubleArray *)leastSquareSineCoefficientsX:(QpsKotlinDoubleArray *)x y:(QpsKotlinDoubleArray *)y freq:(double)freq __attribute__((swift_name("leastSquareSineCoefficients(x:y:freq:)")));
- (void)printRegularSpackFeaturesSpack:(QpsSpack *)spack freq:(double)freq __attribute__((swift_name("printRegularSpackFeatures(spack:freq:)")));
- (void)printSwingSpackFeaturesSpack:(QpsSpack *)spack freq:(double)freq __attribute__((swift_name("printSwingSpackFeatures(spack:freq:)")));
- (NSMutableArray<QpsKotlinFloatArray *> *)rotateToStraightVectors:(NSMutableArray<QpsKotlinFloatArray *> *)vectors angles:(QpsKotlinArray *)angles __attribute__((swift_name("rotateToStraight(vectors:angles:)")));
@end;

__attribute__((swift_name("HistoryHandler")))
@interface QpsHistoryHandler : QpsBase
- (instancetype)initWithMaxSize:(int32_t)maxSize fillValue:(id _Nullable)fillValue fillTime:(QpsLong * _Nullable)fillTime __attribute__((swift_name("init(maxSize:fillValue:fillTime:)"))) __attribute__((objc_designated_initializer));
- (void)addDataData:(id _Nullable)data currentTime:(int64_t)currentTime __attribute__((swift_name("addData(data:currentTime:)")));
- (void)clear __attribute__((swift_name("clear()")));
- (id _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id _Nullable)getDelayedValueTime:(int64_t)time __attribute__((swift_name("getDelayedValue(time:)")));
- (NSArray<id> *)getHistory __attribute__((swift_name("getHistory()")));
- (id _Nullable)getLatest __attribute__((swift_name("getLatest()")));
- (NSArray<QpsLong *> *)getTimestampHistory __attribute__((swift_name("getTimestampHistory()")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
@property (readonly) NSMutableArray<QpsHistoryHandlerTimedData *> *innerHistory __attribute__((swift_name("innerHistory")));
@property (readonly) int32_t lastIndex __attribute__((swift_name("lastIndex")));
@property (readonly) int32_t maxSize __attribute__((swift_name("maxSize")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@property (readonly) NSArray<QpsHistoryHandlerTimedData *> *timedHistory __attribute__((swift_name("timedHistory")));
@end;

__attribute__((swift_name("HistoryHandler.TimedData")))
@interface QpsHistoryHandlerTimedData : QpsBase
- (instancetype)initWith:(QpsHistoryHandler *)receiver data:(id _Nullable)data timestamp:(int64_t)timestamp __attribute__((swift_name("init(_:data:timestamp:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id _Nullable data __attribute__((swift_name("data")));
@property int64_t timestamp __attribute__((swift_name("timestamp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MedFilter")))
@interface QpsMedFilter : QpsBase
- (instancetype)initWithKernel:(int32_t)kernel __attribute__((swift_name("init(kernel:)"))) __attribute__((objc_designated_initializer));
- (float)filterX:(float)x __attribute__((swift_name("filter(x:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MoveVectorKt")))
@interface QpsMoveVectorKt : QpsBase
+ (QpsMoveVector *)sum:(id)receiver __attribute__((swift_name("sum(_:)")));
+ (double)totalMagnitude:(id)receiver __attribute__((swift_name("totalMagnitude(_:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("VectorUtilsKt")))
@interface QpsVectorUtilsKt : QpsBase
+ (float)getRotatedYAxisAngleOnPlaneRotationVector:(QpsKotlinFloatArray *)rotationVector __attribute__((swift_name("getRotatedYAxisAngleOnPlane(rotationVector:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UtilsKt")))
@interface QpsUtilsKt : QpsBase
+ (int64_t)getSystemTimeInMillis __attribute__((swift_name("getSystemTimeInMillis()")));
+ (int64_t)getSystemTimeInNanos __attribute__((swift_name("getSystemTimeInNanos()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinDoubleArray")))
@interface QpsKotlinDoubleArray : QpsBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(QpsDouble *(^)(QpsInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (double)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (QpsKotlinDoubleIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(double)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinFloatArray")))
@interface QpsKotlinFloatArray : QpsBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(QpsFloat *(^)(QpsInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (float)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (QpsKotlinFloatIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(float)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinIntArray")))
@interface QpsKotlinIntArray : QpsBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(QpsInt *(^)(QpsInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (int32_t)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (QpsKotlinIntIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(int32_t)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerializationStrategy")))
@protocol QpsKotlinx_serialization_runtimeSerializationStrategy
@required
- (void)serializeEncoder:(id<QpsKotlinx_serialization_runtimeEncoder>)encoder value:(id _Nullable)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<QpsKotlinx_serialization_runtimeSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeDeserializationStrategy")))
@protocol QpsKotlinx_serialization_runtimeDeserializationStrategy
@required
- (id _Nullable)deserializeDecoder:(id<QpsKotlinx_serialization_runtimeDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (id _Nullable)patchDecoder:(id<QpsKotlinx_serialization_runtimeDecoder>)decoder old:(id _Nullable)old __attribute__((swift_name("patch(decoder:old:)")));
@property (readonly) id<QpsKotlinx_serialization_runtimeSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeKSerializer")))
@protocol QpsKotlinx_serialization_runtimeKSerializer <QpsKotlinx_serialization_runtimeSerializationStrategy, QpsKotlinx_serialization_runtimeDeserializationStrategy>
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinPair")))
@interface QpsKotlinPair : QpsBase
- (instancetype)initWithFirst:(id _Nullable)first second:(id _Nullable)second __attribute__((swift_name("init(first:second:)"))) __attribute__((objc_designated_initializer));
- (id _Nullable)component1 __attribute__((swift_name("component1()")));
- (id _Nullable)component2 __attribute__((swift_name("component2()")));
- (QpsKotlinPair *)doCopyFirst:(id _Nullable)first second:(id _Nullable)second __attribute__((swift_name("doCopy(first:second:)")));
- (BOOL)equalsOther:(id _Nullable)other __attribute__((swift_name("equals(other:)")));
- (int32_t)hashCode __attribute__((swift_name("hashCode()")));
- (NSString *)toString __attribute__((swift_name("toString()")));
@property (readonly) id _Nullable first __attribute__((swift_name("first")));
@property (readonly) id _Nullable second __attribute__((swift_name("second")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface QpsKotlinArray : QpsBase
+ (instancetype)arrayWithSize:(int32_t)size init:(id _Nullable (^)(QpsInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (id _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<QpsKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(id _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("KotlinIterator")))
@protocol QpsKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end;

__attribute__((swift_name("KotlinDoubleIterator")))
@interface QpsKotlinDoubleIterator : QpsBase <QpsKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (QpsDouble *)next __attribute__((swift_name("next()")));
- (double)nextDouble __attribute__((swift_name("nextDouble()")));
@end;

__attribute__((swift_name("KotlinFloatIterator")))
@interface QpsKotlinFloatIterator : QpsBase <QpsKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (QpsFloat *)next __attribute__((swift_name("next()")));
- (float)nextFloat __attribute__((swift_name("nextFloat()")));
@end;

__attribute__((swift_name("KotlinIntIterator")))
@interface QpsKotlinIntIterator : QpsBase <QpsKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (QpsInt *)next __attribute__((swift_name("next()")));
- (int32_t)nextInt __attribute__((swift_name("nextInt()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeEncoder")))
@protocol QpsKotlinx_serialization_runtimeEncoder
@required
- (id<QpsKotlinx_serialization_runtimeCompositeEncoder>)beginCollectionDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor collectionSize:(int32_t)collectionSize typeSerializers:(QpsKotlinArray *)typeSerializers __attribute__((swift_name("beginCollection(descriptor:collectionSize:typeSerializers:)")));
- (id<QpsKotlinx_serialization_runtimeCompositeEncoder>)beginStructureDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor typeSerializers:(QpsKotlinArray *)typeSerializers __attribute__((swift_name("beginStructure(descriptor:typeSerializers:)")));
- (void)encodeBooleanValue:(BOOL)value __attribute__((swift_name("encodeBoolean(value:)")));
- (void)encodeByteValue:(int8_t)value __attribute__((swift_name("encodeByte(value:)")));
- (void)encodeCharValue:(unichar)value __attribute__((swift_name("encodeChar(value:)")));
- (void)encodeDoubleValue:(double)value __attribute__((swift_name("encodeDouble(value:)")));
- (void)encodeEnumEnumDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)enumDescriptor index:(int32_t)index __attribute__((swift_name("encodeEnum(enumDescriptor:index:)")));
- (void)encodeFloatValue:(float)value __attribute__((swift_name("encodeFloat(value:)")));
- (void)encodeIntValue:(int32_t)value __attribute__((swift_name("encodeInt(value:)")));
- (void)encodeLongValue:(int64_t)value __attribute__((swift_name("encodeLong(value:)")));
- (void)encodeNotNullMark __attribute__((swift_name("encodeNotNullMark()")));
- (void)encodeNull __attribute__((swift_name("encodeNull()")));
- (void)encodeNullableSerializableValueSerializer:(id<QpsKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableValue(serializer:value:)")));
- (void)encodeSerializableValueSerializer:(id<QpsKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableValue(serializer:value:)")));
- (void)encodeShortValue:(int16_t)value __attribute__((swift_name("encodeShort(value:)")));
- (void)encodeStringValue:(NSString *)value __attribute__((swift_name("encodeString(value:)")));
- (void)encodeUnit __attribute__((swift_name("encodeUnit()")));
@property (readonly) id<QpsKotlinx_serialization_runtimeSerialModule> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerialDescriptor")))
@protocol QpsKotlinx_serialization_runtimeSerialDescriptor
@required
- (NSArray<id<QpsKotlinAnnotation>> *)getElementAnnotationsIndex:(int32_t)index __attribute__((swift_name("getElementAnnotations(index:)")));
- (id<QpsKotlinx_serialization_runtimeSerialDescriptor>)getElementDescriptorIndex:(int32_t)index __attribute__((swift_name("getElementDescriptor(index:)")));
- (int32_t)getElementIndexName:(NSString *)name __attribute__((swift_name("getElementIndex(name:)")));
- (NSString *)getElementNameIndex:(int32_t)index __attribute__((swift_name("getElementName(index:)")));
- (NSArray<id<QpsKotlinAnnotation>> *)getEntityAnnotations __attribute__((swift_name("getEntityAnnotations()"))) __attribute__((deprecated("Deprecated in the favour of 'annotations' property")));
- (BOOL)isElementOptionalIndex:(int32_t)index __attribute__((swift_name("isElementOptional(index:)")));
@property (readonly) NSArray<id<QpsKotlinAnnotation>> *annotations __attribute__((swift_name("annotations")));
@property (readonly) int32_t elementsCount __attribute__((swift_name("elementsCount")));
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));
@property (readonly) QpsKotlinx_serialization_runtimeSerialKind *kind __attribute__((swift_name("kind")));
@property (readonly) NSString *name __attribute__((swift_name("name"))) __attribute__((unavailable("name property deprecated in the favour of serialName")));
@property (readonly) NSString *serialName __attribute__((swift_name("serialName")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeDecoder")))
@protocol QpsKotlinx_serialization_runtimeDecoder
@required
- (id<QpsKotlinx_serialization_runtimeCompositeDecoder>)beginStructureDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor typeParams:(QpsKotlinArray *)typeParams __attribute__((swift_name("beginStructure(descriptor:typeParams:)")));
- (BOOL)decodeBoolean __attribute__((swift_name("decodeBoolean()")));
- (int8_t)decodeByte __attribute__((swift_name("decodeByte()")));
- (unichar)decodeChar __attribute__((swift_name("decodeChar()")));
- (double)decodeDouble __attribute__((swift_name("decodeDouble()")));
- (int32_t)decodeEnumEnumDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)enumDescriptor __attribute__((swift_name("decodeEnum(enumDescriptor:)")));
- (float)decodeFloat __attribute__((swift_name("decodeFloat()")));
- (int32_t)decodeInt __attribute__((swift_name("decodeInt()")));
- (int64_t)decodeLong __attribute__((swift_name("decodeLong()")));
- (BOOL)decodeNotNullMark __attribute__((swift_name("decodeNotNullMark()")));
- (QpsKotlinNothing * _Nullable)decodeNull __attribute__((swift_name("decodeNull()")));
- (id _Nullable)decodeNullableSerializableValueDeserializer:(id<QpsKotlinx_serialization_runtimeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableValue(deserializer:)")));
- (id _Nullable)decodeSerializableValueDeserializer:(id<QpsKotlinx_serialization_runtimeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableValue(deserializer:)")));
- (int16_t)decodeShort __attribute__((swift_name("decodeShort()")));
- (NSString *)decodeString __attribute__((swift_name("decodeString()")));
- (void)decodeUnit __attribute__((swift_name("decodeUnit()")));
- (id _Nullable)updateNullableSerializableValueDeserializer:(id<QpsKotlinx_serialization_runtimeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateNullableSerializableValue(deserializer:old:)")));
- (id _Nullable)updateSerializableValueDeserializer:(id<QpsKotlinx_serialization_runtimeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateSerializableValue(deserializer:old:)")));
@property (readonly) id<QpsKotlinx_serialization_runtimeSerialModule> context __attribute__((swift_name("context")));
@property (readonly) QpsKotlinx_serialization_runtimeUpdateMode *updateMode __attribute__((swift_name("updateMode")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeCompositeEncoder")))
@protocol QpsKotlinx_serialization_runtimeCompositeEncoder
@required
- (void)encodeBooleanElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(BOOL)value __attribute__((swift_name("encodeBooleanElement(descriptor:index:value:)")));
- (void)encodeByteElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(int8_t)value __attribute__((swift_name("encodeByteElement(descriptor:index:value:)")));
- (void)encodeCharElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(unichar)value __attribute__((swift_name("encodeCharElement(descriptor:index:value:)")));
- (void)encodeDoubleElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(double)value __attribute__((swift_name("encodeDoubleElement(descriptor:index:value:)")));
- (void)encodeFloatElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(float)value __attribute__((swift_name("encodeFloatElement(descriptor:index:value:)")));
- (void)encodeIntElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(int32_t)value __attribute__((swift_name("encodeIntElement(descriptor:index:value:)")));
- (void)encodeLongElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(int64_t)value __attribute__((swift_name("encodeLongElement(descriptor:index:value:)")));
- (void)encodeNonSerializableElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(id)value __attribute__((swift_name("encodeNonSerializableElement(descriptor:index:value:)"))) __attribute__((unavailable("This method is deprecated for removal. Please remove it from your implementation and delegate to default method instead")));
- (void)encodeNullableSerializableElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<QpsKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeSerializableElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<QpsKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeShortElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(int16_t)value __attribute__((swift_name("encodeShortElement(descriptor:index:value:)")));
- (void)encodeStringElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(NSString *)value __attribute__((swift_name("encodeStringElement(descriptor:index:value:)")));
- (void)encodeUnitElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("encodeUnitElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
- (BOOL)shouldEncodeElementDefaultDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("shouldEncodeElementDefault(descriptor:index:)")));
@property (readonly) id<QpsKotlinx_serialization_runtimeSerialModule> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerialModule")))
@protocol QpsKotlinx_serialization_runtimeSerialModule
@required
- (void)dumpToCollector:(id<QpsKotlinx_serialization_runtimeSerialModuleCollector>)collector __attribute__((swift_name("dumpTo(collector:)")));
- (id<QpsKotlinx_serialization_runtimeKSerializer> _Nullable)getContextualKclass:(id<QpsKotlinKClass>)kclass __attribute__((swift_name("getContextual(kclass:)")));
- (id<QpsKotlinx_serialization_runtimeKSerializer> _Nullable)getPolymorphicBaseClass:(id<QpsKotlinKClass>)baseClass value:(id)value __attribute__((swift_name("getPolymorphic(baseClass:value:)")));
- (id<QpsKotlinx_serialization_runtimeKSerializer> _Nullable)getPolymorphicBaseClass:(id<QpsKotlinKClass>)baseClass serializedClassName:(NSString *)serializedClassName __attribute__((swift_name("getPolymorphic(baseClass:serializedClassName:)")));
@end;

__attribute__((swift_name("KotlinAnnotation")))
@protocol QpsKotlinAnnotation
@required
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerialKind")))
@interface QpsKotlinx_serialization_runtimeSerialKind : QpsBase
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeCompositeDecoder")))
@protocol QpsKotlinx_serialization_runtimeCompositeDecoder
@required
- (BOOL)decodeBooleanElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeBooleanElement(descriptor:index:)")));
- (int8_t)decodeByteElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeByteElement(descriptor:index:)")));
- (unichar)decodeCharElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeCharElement(descriptor:index:)")));
- (int32_t)decodeCollectionSizeDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor __attribute__((swift_name("decodeCollectionSize(descriptor:)")));
- (double)decodeDoubleElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeDoubleElement(descriptor:index:)")));
- (int32_t)decodeElementIndexDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor __attribute__((swift_name("decodeElementIndex(descriptor:)")));
- (float)decodeFloatElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeFloatElement(descriptor:index:)")));
- (int32_t)decodeIntElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeIntElement(descriptor:index:)")));
- (int64_t)decodeLongElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeLongElement(descriptor:index:)")));
- (id _Nullable)decodeNullableSerializableElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<QpsKotlinx_serialization_runtimeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableElement(descriptor:index:deserializer:)")));
- (BOOL)decodeSequentially __attribute__((swift_name("decodeSequentially()")));
- (id _Nullable)decodeSerializableElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<QpsKotlinx_serialization_runtimeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableElement(descriptor:index:deserializer:)")));
- (int16_t)decodeShortElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeShortElement(descriptor:index:)")));
- (NSString *)decodeStringElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeStringElement(descriptor:index:)")));
- (void)decodeUnitElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeUnitElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
- (id _Nullable)updateNullableSerializableElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<QpsKotlinx_serialization_runtimeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateNullableSerializableElement(descriptor:index:deserializer:old:)")));
- (id _Nullable)updateSerializableElementDescriptor:(id<QpsKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<QpsKotlinx_serialization_runtimeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateSerializableElement(descriptor:index:deserializer:old:)")));
@property (readonly) id<QpsKotlinx_serialization_runtimeSerialModule> context __attribute__((swift_name("context")));
@property (readonly) QpsKotlinx_serialization_runtimeUpdateMode *updateMode __attribute__((swift_name("updateMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface QpsKotlinNothing : QpsBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_runtimeUpdateMode")))
@interface QpsKotlinx_serialization_runtimeUpdateMode : QpsKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) QpsKotlinx_serialization_runtimeUpdateMode *banned __attribute__((swift_name("banned")));
@property (class, readonly) QpsKotlinx_serialization_runtimeUpdateMode *overwrite __attribute__((swift_name("overwrite")));
@property (class, readonly) QpsKotlinx_serialization_runtimeUpdateMode *update __attribute__((swift_name("update")));
- (int32_t)compareToOther:(QpsKotlinx_serialization_runtimeUpdateMode *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerialModuleCollector")))
@protocol QpsKotlinx_serialization_runtimeSerialModuleCollector
@required
- (void)contextualKClass:(id<QpsKotlinKClass>)kClass serializer:(id<QpsKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("contextual(kClass:serializer:)")));
- (void)polymorphicBaseClass:(id<QpsKotlinKClass>)baseClass actualClass:(id<QpsKotlinKClass>)actualClass actualSerializer:(id<QpsKotlinx_serialization_runtimeKSerializer>)actualSerializer __attribute__((swift_name("polymorphic(baseClass:actualClass:actualSerializer:)")));
@end;

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol QpsKotlinKDeclarationContainer
@required
@end;

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol QpsKotlinKAnnotatedElement
@required
@end;

__attribute__((swift_name("KotlinKClassifier")))
@protocol QpsKotlinKClassifier
@required
@end;

__attribute__((swift_name("KotlinKClass")))
@protocol QpsKotlinKClass <QpsKotlinKDeclarationContainer, QpsKotlinKAnnotatedElement, QpsKotlinKClassifier>
@required
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end;

#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
